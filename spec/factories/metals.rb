# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :metal do
    abbreviation 'Abcd'
    name 'Metal name'
    description 'My description'
    association :basemetal, factory: :basemetal
    factor 22
    association :language_id, factory: :language
    purity 1.5
    sort 1
  end
end
