
FactoryGirl.define do

  factory :user do
    account
    authorize
    first_name 'Billy'
    last_name 'Smith'
    report_level 2
    phone_cell '2142569875'
    cellcarrier
    locked true
    sequence(:email) { |n| "123#{n}@123.com" }
    password 'kkjii5W@ii'
    role #FK

  end

end