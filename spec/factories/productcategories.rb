# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :productcategory do
    description "MyString"
    language_id 1
    name "MyString"
    sort 1
  end
end
