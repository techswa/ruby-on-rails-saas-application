# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ratingsystem do
    abbreviation "MyString"
    description "MyString"
    sort ""
  end
end
