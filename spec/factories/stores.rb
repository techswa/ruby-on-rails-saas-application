

FactoryGirl.define do

  factory :store do |f|
    f.account #FK
    f.license '123456'
    #f.sequence(:license)  { |n| "12345#{n}" }
    f.anniversary {Date.today}
    f.sequence(:name) { |n|  "Billys Gold World#{n}" }
    f.address_1 '123 S. Elm'
    f.address_2 'Ste 432'
    f.city 'Dallas'
    f.state #FK
    f.zipcode '75287'
    f.phone_cell '214-256-9875'
    f.phone_office '972-698-5324'
    f.phone_office_ext '201'
    f.location #FK
    f.sequence(:key)  { |n| "A12548#{n}" }
  end

end