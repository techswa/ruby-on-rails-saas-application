# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :authorize do
    store_id 1
    user_id 1
    active false
    payment_date "2014-02-19"
    paymethod_id 1
    payment_note "MyString"
    expiration_date "2014-02-19"
  end
end
