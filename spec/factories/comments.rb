# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comment do
    content 'MyText'
    sticky false
    commentable_id 1
    commentable_type 'account'
  end
end
