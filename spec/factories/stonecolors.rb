# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :stonecolor do
    ratingsystem_id 1
    abbreviation "MyString"
    description "MyString"
    language_id 1
    method "MyString"
    name "MyString"
    sort 1
  end
end
