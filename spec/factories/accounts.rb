# Read about factories at https://github.com/thoughtbot/factory_girl

  FactoryGirl.define do
      #@rand = rand(999999).to_s.center(6, rand(9).to_s)
    factory :account do
      number 123456 #@rand
      name 'My Gold Buyers'
      first_name 'Bob'
      last_name 'Smith'
      title 'President'
      email 'bobsmith@mygoldbuyer.com'
      #sequence(:email) { |n| "bobsmith#{n}@mygoldbuyer.com" }
      email_confirmation 'bobsmith@mygoldbuyer.com'
      #sequence(:email_confirmation) { |n| "bobsmith#{n}@mygoldbuyer.com" }
      phone_office '123456790'
      phone_office_ext '001'
      phone_cell '2345678901'
      address_1 '1234 S. Elm St.'
      address_2 'Ste 213'
      city 'Dallas'
      state
      zipcode '75284'
      anniversary {Date.today}
      standing #FK
      code 'secret code'
    end

  end


