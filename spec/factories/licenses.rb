# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :license do
    abbreviation "MyString"
    description "MyString"
  end
end
