# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :stone do
    name "MyString"
    abbreviation "MyString"
    description "MyString"
    sort 1
  end
end
