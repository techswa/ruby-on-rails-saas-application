# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product do
    description "MyString"
    language_id 1
    name "MyString"
    productcateory_id 1
    sort 1
  end
end
