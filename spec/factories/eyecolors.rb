# Read about factories at https://github.com/thoughtbot/factory_girl

  FactoryGirl.define do

    factory :eyecolor do |f|
      f.abbreviation { 'Blu' }
      f.description { 'Eye color of Blue' }
    end

  end


  #trait :eyecolor_punctuation_chars do
  #  sms '@'
  #end

  #trait :sms_alpha_chars do
  #  sms 'A'
  #end
  #
  #trait :sms_numeric_short do
  #  sms '123'
  #end
  #
  #trait :sms_numeric_long do
  #  sms '1234567890123'
  #end


