# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do


  FactoryGirl.define do
    factory :basemetal do |f|
      f.name { generate(:name) }
      f.abbreviation { generate(:abbreviation) }
    end
  end

  trait :abbreviation_1_char do
    abbreviation '1'
  end

end
