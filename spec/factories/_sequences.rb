
FactoryGirl.define do

  sequence :name do |n|
    "Name#{n}"
  end

  sequence :abbreviation do |n|
    "Abbreviation#{n}"
  end

  sequence :description do |n|
    "Descritpion#{n}"
  end

  trait :contains_punctuation_chars do
    '@'
  end

end