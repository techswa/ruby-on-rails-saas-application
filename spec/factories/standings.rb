# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :standing do
    name 'Good'
    description 'Account is current'
    sort 1
  end
end
