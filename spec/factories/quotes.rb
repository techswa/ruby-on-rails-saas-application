# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :quote do
    account #FK
    user_ref 'Jim Smith'
    ag_spot 10
    ag_payout 9
    au_spot 5
    au_payout 4
    pt_spot 11
    pt_payout 8
  end
end
