FactoryGirl.define do

  FactoryGirl.define do
    factory :cellcarrier do
      name 'Name'
      sms '1234567890'
    end
  end

  trait :sms_punctuation_chars do
    sms '@'
  end

  trait :sms_alpha_chars do
    sms 'A'
  end

  trait :sms_numeric_short do
    sms '123'
  end

  trait :sms_numeric_long do
    sms '1234567890123'
  end

end