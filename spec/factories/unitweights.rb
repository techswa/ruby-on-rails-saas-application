# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :unitweight do
    abbreviation "MyString"
    description "MyString"
    sort 1
    factor 1.5
  end
end
