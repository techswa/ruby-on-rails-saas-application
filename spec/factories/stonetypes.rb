# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :stonetype do
    name "MyString"
    description "MyString"
    language_id 1
    sort 1
  end
end
