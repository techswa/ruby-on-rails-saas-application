# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :stonesize do
    abbreviation "MyString"
    description "MyString"
    language_id 1
    sort 1
  end
end
