require 'spec_helper'

describe AccountsController do

  #Test factory
  describe 'Factory' do
    context 'valid factory' do
      it 'is operational' do
        FactoryGirl.create(:account).should be_valid
      end
    end

  end


  # This should return the minimal set of attributes required to create a valid
  # Account. As you add validations to Account, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) { { "account_id" => "1" } }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # AuthorizesController. Be sure to keep this updated too.
  let(:valid_session) { {} }


  describe 'GET #index' do
    it 'populates an array of accounts' do
      account = FactoryGirl(:account)
      get :index
      assigns(:account).should eq([account])
    end

  end

  #describe "GET index" do
  #  it "assigns all accounts as @accounts" do
  #    #@accounts = FactoryGirl.create_list(:account,3)
  #    @accounts = FactoryGirl.create(:account)
  #    #@accounts.count == 3
  #    #puts "First account: #{@accounts[0].inspect}"
  #    puts "Accounts: #{@account.inspect}"
  #    get :index
  #    expect(response).to render_template :index
  #
  #  end
  #end

  #describe "GET show" do
  #  it "assigns the requested account as @account" do
  #    account = Account.create! valid_attributes
  #    get :show, {:id => account.to_param}, valid_session
  #    assigns(:account).should eq(account)
  #  end
  #end
  #
  #describe "GET new" do
  #  it "assigns a new account as @account" do
  #    get :new, {}, valid_session
  #    assigns(:account).should be_a_new(Account)
  #  end
  #end
  #
  #describe "GET edit" do
  #  it "assigns the requested account as @account" do
  #    account = Account.create! valid_attributes
  #    get :edit, {:id => account.to_param}, valid_session
  #    assigns(:account).should eq(account)
  #  end
  #end
  #
  #describe "POST create" do
  #  describe "with valid params" do
  #    it "creates a new Account" do
  #      expect {
  #        post :create, {:account => valid_attributes}, valid_session
  #      }.to change(Account, :count).by(1)
  #    end
  #
  #    it "assigns a newly created account as @account" do
  #      post :create, {:account => valid_attributes}, valid_session
  #      assigns(:account).should be_a(Account)
  #      assigns(:account).should be_persisted
  #    end
  #
  #    it "redirects to the created account" do
  #      post :create, {:account => valid_attributes}, valid_session
  #      response.should redirect_to(Account.last)
  #    end
  #  end
  #
  #  describe "with invalid params" do
  #    it "assigns a newly created but unsaved account as @account" do
  #      # Trigger the behavior that occurs when invalid params are submitted
  #      Account.any_instance.stub(:save).and_return(false)
  #      post :create, {:account => { "account_id" => "invalid value" }}, valid_session
  #      assigns(:account).should be_a_new(Account)
  #    end
  #
  #    it "re-renders the 'new' template" do
  #      # Trigger the behavior that occurs when invalid params are submitted
  #      Account.any_instance.stub(:save).and_return(false)
  #      post :create, {:account => { "account_id" => "invalid value" }}, valid_session
  #      response.should render_template("new")
  #    end
  #  end
  #end
  #
  #describe "PUT update" do
  #  describe "with valid params" do
  #    it "updates the requested account" do
  #      account = Account.create! valid_attributes
  #      # Assuming there are no other accounts in the database, this
  #      # specifies that the Account created on the previous line
  #      # receives the :update_attributes message with whatever params are
  #      # submitted in the request.
  #      Account.any_instance.should_receive(:update_attributes).with({ "account_id" => "1" })
  #      put :update, {:id => account.to_param, :account => { "account_id" => "1" }}, valid_session
  #    end
  #
  #    it "assigns the requested account as @account" do
  #      account = Account.create! valid_attributes
  #      put :update, {:id => account.to_param, :account => valid_attributes}, valid_session
  #      assigns(:account).should eq(account)
  #    end
  #
  #    it "redirects to the account" do
  #      account = Account.create! valid_attributes
  #      put :update, {:id => account.to_param, :account => valid_attributes}, valid_session
  #      response.should redirect_to(account)
  #    end
  #  end
  #
  #  describe "with invalid params" do
  #    it "assigns the account as @account" do
  #      account = Account.create! valid_attributes
  #      # Trigger the behavior that occurs when invalid params are submitted
  #      Account.any_instance.stub(:save).and_return(false)
  #      put :update, {:id => account.to_param, :account => { "account_id" => "invalid value" }}, valid_session
  #      assigns(:account).should eq(account)
  #    end
  #
  #    it "re-renders the 'edit' template" do
  #      account = Account.create! valid_attributes
  #      # Trigger the behavior that occurs when invalid params are submitted
  #      Account.any_instance.stub(:save).and_return(false)
  #      put :update, {:id => account.to_param, :account => { "account_id" => "invalid value" }}, valid_session
  #      response.should render_template("edit")
  #    end
  #  end
  #end
  #
  #describe "DELETE destroy" do
  #  it "destroys the requested account" do
  #    account = Account.create! valid_attributes
  #    expect {
  #      delete :destroy, {:id => account.to_param}, valid_session
  #    }.to change(Account, :count).by(-1)
  #  end
  #
  #  it "redirects to the accounts list" do
  #    account = Account.create! valid_attributes
  #    delete :destroy, {:id => account.to_param}, valid_session
  #    response.should redirect_to(authorizes_url)
  #  end
  #end
  
end
