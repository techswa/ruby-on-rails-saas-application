require 'spec_helper'

describe ReportsController do

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'sales'" do
    it "returns http success" do
      get 'sales'
      response.should be_success
    end
  end

end
