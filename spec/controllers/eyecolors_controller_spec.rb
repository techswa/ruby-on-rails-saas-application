require 'spec_helper'

## This should return the minimal set of attributes required to create a valid
## Eyecolor. As you add validations to Eyecolor, be sure to
## adjust the attributes here as well.
#let(:valid_attributes) { { 'abbreviation' => 'Blu', 'Description' => '' } }
#
## This should return the minimal set of values that should be in the session
## in order to pass any filters (e.g. authentication) defined in
## EyecolorsController. Be sure to keep this updated too.
#let(:valid_session) { {} }
#
#describe "GET index" do
#  it "assigns all eyecolors as @eyecolors" do
#    eyecolor = Eyecolor.create! valid_attributes
#    get :index, {}, valid_session
#    assigns(:eyecolors).should eq([eyecolor])
#  end
#end
#
#describe "GET show" do
#  it "assigns the requested eyecolor as @eyecolor" do
#    eyecolor = Eyecolor.create! valid_attributes
#    get :show, {:id => eyecolor.to_param}, valid_session
#    assigns(:eyecolor).should eq(eyecolor)
#  end
#end
#
#describe "GET new" do
#  it "assigns a new eyecolor as @eyecolor" do
#    get :new, {}, valid_session
#    assigns(:eyecolor).should be_a_new(Eyecolor)
#  end
#end
#
#describe "GET edit" do
#  it "assigns the requested eyecolor as @eyecolor" do
#    eyecolor = Eyecolor.create! valid_attributes
#    get :edit, {:id => eyecolor.to_param}, valid_session
#    assigns(:eyecolor).should eq(eyecolor)
#  end
#end
#
#describe "POST create" do
#  describe "with valid params" do
#    it "creates a new Eyecolor" do
#      expect {
#        post :create, {:eyecolor => valid_attributes}, valid_session
#      }.to change(Eyecolor, :count).by(1)
#    end
#
#    it "assigns a newly created eyecolor as @eyecolor" do
#      post :create, {:eyecolor => valid_attributes}, valid_session
#      assigns(:eyecolor).should be_a(Eyecolor)
#      assigns(:eyecolor).should be_persisted
#    end
#
#    it "redirects to the created eyecolor" do
#      post :create, {:eyecolor => valid_attributes}, valid_session
#      response.should redirect_to(Eyecolor.last)
#    end
#  end
#
#  describe "with invalid params" do
#    it "assigns a newly created but unsaved eyecolor as @eyecolor" do
#      # Trigger the behavior that occurs when invalid params are submitted
#      Eyecolor.any_instance.stub(:save).and_return(false)
#      post :create, {:eyecolor => { "abbreviation" => "invalid value" }}, valid_session
#      assigns(:eyecolor).should be_a_new(Eyecolor)
#    end
#
#    it "re-renders the 'new' template" do
#      # Trigger the behavior that occurs when invalid params are submitted
#      Eyecolor.any_instance.stub(:save).and_return(false)
#      post :create, {:eyecolor => { "abbreviation" => "invalid value" }}, valid_session
#      response.should render_template("new")
#    end
#  end
#end
#
#describe "PUT update" do
#  describe "with valid params" do
#    it "updates the requested eyecolor" do
#      eyecolor = Eyecolor.create! valid_attributes
#      # Assuming there are no other eyecolors in the database, this
#      # specifies that the Eyecolor created on the previous line
#      # receives the :update_attributes message with whatever params are
#      # submitted in the request.
#      Eyecolor.any_instance.should_receive(:update_attributes).with({ "abbreviation" => "MyString" })
#      put :update, {:id => eyecolor.to_param, :eyecolor => { "abbreviation" => "MyString" }}, valid_session
#    end
#
#    it "assigns the requested eyecolor as @eyecolor" do
#      eyecolor = Eyecolor.create! valid_attributes
#      put :update, {:id => eyecolor.to_param, :eyecolor => valid_attributes}, valid_session
#      assigns(:eyecolor).should eq(eyecolor)
#    end
#
#    it "redirects to the eyecolor" do
#      eyecolor = Eyecolor.create! valid_attributes
#      put :update, {:id => eyecolor.to_param, :eyecolor => valid_attributes}, valid_session
#      response.should redirect_to(eyecolor)
#    end
#  end
#
#  describe "with invalid params" do
#    it "assigns the eyecolor as @eyecolor" do
#      eyecolor = Eyecolor.create! valid_attributes
#      # Trigger the behavior that occurs when invalid params are submitted
#      Eyecolor.any_instance.stub(:save).and_return(false)
#      put :update, {:id => eyecolor.to_param, :eyecolor => { "abbreviation" => "invalid value" }}, valid_session
#      assigns(:eyecolor).should eq(eyecolor)
#    end
#
#    it "re-renders the 'edit' template" do
#      eyecolor = Eyecolor.create! valid_attributes
#      # Trigger the behavior that occurs when invalid params are submitted
#      Eyecolor.any_instance.stub(:save).and_return(false)
#      put :update, {:id => eyecolor.to_param, :eyecolor => { "abbreviation" => "invalid value" }}, valid_session
#      response.should render_template("edit")
#    end
#  end
#end
#
#describe "DELETE destroy" do
#  it "destroys the requested eyecolor" do
#    eyecolor = Eyecolor.create! valid_attributes
#    expect {
#      delete :destroy, {:id => eyecolor.to_param}, valid_session
#    }.to change(Eyecolor, :count).by(-1)
#  end
#
#  it "redirects to the eyecolors list" do
#    eyecolor = Eyecolor.create! valid_attributes
#    delete :destroy, {:id => eyecolor.to_param}, valid_session
#    response.should redirect_to(eyecolors_url)
#  end
#end

