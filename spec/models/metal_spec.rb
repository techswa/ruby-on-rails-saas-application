require 'spec_helper'

describe Metal do

  describe 'Factory' do
    context 'valid factory' do
      it 'is operational' do
        FactoryGirl.create(:metal).should be_valid
      end
    end
  end


  context 'Validations' do

    context 'name is' do
      it 'invalid with more than 15 chars' do
        FactoryGirl.build(:metal, name:  ('a' * 16) ).should_not be_valid
      end
      it 'invalid with nil' do
        FactoryGirl.build(:metal, name: nil).should_not be_valid
      end
      it 'invalid blank' do
        FactoryGirl.build(:metal, name: '').should_not be_valid
      end

    end

    context 'abbreviation is' do
      it 'invalid as nil' do
        FactoryGirl.build(:metal, abbreviation: nil).should_not be_valid
      end
      it 'valid with 4 chars max' do
        FactoryGirl.build(:metal, abbreviation: 'abcd').should be_valid
      end
      it 'invalid with 5 chars' do
        FactoryGirl.build(:metal, abbreviation: 'abcde').should_not be_valid
      end

      it 'valid with 2 chars min' do
        FactoryGirl.build(:metal, abbreviation: 'ab').should be_valid
      end
      it 'invalid with 1 char' do
        FactoryGirl.build(:metal, abbreviation: 'a').should_not be_valid
      end
    end

    context 'description is' do
      it 'invalid with more than 255 chars' do
        FactoryGirl.build(:metal, description:  ('a' * 256) ).should_not be_valid
      end
      it 'valid with nil' do
        FactoryGirl.build(:metal, description: nil).should be_valid
      end
    end

    context 'name is' do
      it 'invalid with more than 15 chars' do
        FactoryGirl.build(:metal, name:  ('a' * 16) ).should_not be_valid
      end
      it 'invalid with nil' do
        FactoryGirl.build(:metal, name: nil).should_not be_valid
      end
      it 'invalid blank' do
        FactoryGirl.build(:metal, name: '').should_not be_valid
      end
    end

    context 'factor is' do
      it 'invalid with a string' do
        FactoryGirl.build(:metal, factor:  'a' ).should_not be_valid
      end
      it 'invalid with nil' do
        FactoryGirl.build(:metal, factor: nil).should_not be_valid
      end
      it 'invalid blank' do
        FactoryGirl.build(:metal, factor: '').should_not be_valid
      end
      it 'valid with a float' do
        FactoryGirl.build(:metal, factor: (0.23)).should be_valid
      end
    end

    context 'purity is' do
      it 'invalid with nil' do
        FactoryGirl.build(:metal, purity: nil).should_not be_valid
      end
      it 'invalid blank' do
        FactoryGirl.build(:metal, purity: '').should_not be_valid
      end
      it 'invalid with more than 4 chars' do
        FactoryGirl.build(:metal, purity:  ('a' * 5) ).should_not be_valid
      end
    end
    context 'sort is' do
      it 'invalid with nil' do
        #FactoryGirl.build(:metal, sort: nil).should_not be_valid
      end
      it 'invalid blank' do
        #FactoryGirl.build(:metal, sort: '').should_not be_valid
      end
      it 'invalid with chars' do
        FactoryGirl.build(:metal, sort: 'aa5gfss' ).should_not be_valid

      end
      it 'invalid with chars' do
        FactoryGirl.build(:metal, sort: 'aa5gfss' ).should_not be_valid
      end

      it 'valid with numeric' do
        FactoryGirl.build(:metal, sort: 123 ).should be_valid
      end
    end


  end
end
