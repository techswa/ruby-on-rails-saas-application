require 'spec_helper'

describe Eyecolor do

  describe 'Factory' do
    context 'valid factory' do
      it 'is operational' do
        FactoryGirl.create(:eyecolor).should be_valid
      end
    end
    #context 'invalid factory' do
    #  it 'is operational' do
    #    FactoryGirl.create(:invalid_eyecolor).should_not be_valid
    #  end
    #end
  end

  describe 'Abbreviation' do

    context 'Validations' do

      context 'presence_of' do
        it 'is invalid as nil' do
          FactoryGirl.build(:eyecolor, abbreviation: nil).should_not be_valid
        end
        it 'is valid with data' do
          FactoryGirl.build(:eyecolor).should be_valid
        end
      end
      context 'length_of' do
        it 'is less than 3 charactors' do
          FactoryGirl.build(:eyecolor, abbreviation: 'ab').should_not be_valid
        end

        it 'is more than 2 charactors' do
          FactoryGirl.build(:eyecolor, abbreviation: 'abcd').should_not be_valid
        end
      end

      context 'format_of' do
        it 'no numeric charactors' do
          FactoryGirl.build(:eyecolor, abbreviation: '1').should_not be_valid
        end

      it 'no punctuation charactors' do
        FactoryGirl.build(:eyecolor, abbreviation: '@').should_not be_valid
      end

      end
    end
  end
  describe 'Description' do
    it 'has no additional tests'
  end

end