require 'spec_helper'

#:account, :status, :name, :contact_name_first, :contact_name_last, :contact_title, :email, :phone_office,
#    :phone_office_ext, :phone_cell, :address_office_1, :address_office_2, :address_office_city,
#    :address_office_state_id, :address_office_zipcode, :address_mail_1, :address_mail_2,
#    :address_mail_city, :address_mail_state_id, :address_mail_zipcode,
#    :anniversary, :notes

describe Account do


  describe 'Factory' do
    context 'valid factory' do
      it 'is operational' do
        FactoryGirl.create(:account).should be_valid
      end
    end
  end

  describe 'Account Number' do
  #
    context 'Validations' do
      context 'presence_of' do
        it 'is invalid as nil' do
          FactoryGirl.build(:account, number: nil).should_not be_valid
        end
      end
      context 'length_of' do
        it 'is more than 5 charactors' do
          FactoryGirl.build(:account, number: '12345').should_not be_valid
        end

        it 'is less than 7 charactors' do
         # FactoryGirl.build(:account, number: '1234567').should_not be_valid
        end
      end

      context 'format_of' do
        it 'only numbers 0-9' do
          FactoryGirl.build(:account, number: 'AAAAAA').should_not be_valid
        end

      end
    end
  end


  #
  describe 'Name' do
    it 'it is not blank' do
      FactoryGirl.build(:account, name: nil).should_not be_valid
    end
  end
  #
  describe 'Contact first name' do
    it 'it is not blank' do
     FactoryGirl.build(:account, first_name: nil).should_not be_valid
    end
  end

  #
  describe 'Contact last name' do
    it 'it is not blank' do
      FactoryGirl.build(:account, last_name: nil).should_not be_valid
    end
  end
  #
  describe 'Contact title' do
    it 'it is not blank' do
      FactoryGirl.build(:account, title: nil).should_not be_valid
    end
  end
  #
  describe 'Email' do
    context 'Validations' do
      it 'it is not blank' do
        FactoryGirl.build(:account, email: nil).should_not be_valid
      end

      it 'it is well formed' do
        FactoryGirl.build(:account, email: '123.com').should_not be_valid
      end

    end
  end
  describe 'Email_validator' do
    context 'Validations' do

      it 'it is not blank' do
        FactoryGirl.build(:account, email_confirmation: nil).should_not be_valid
      end

      it 'it is well formed' do
        FactoryGirl.build(:account, email_confirmation: '123.com').should_not be_valid
      end
    end
  end
  describe 'Email input matches' do
    it 'Email doesn\'t match Email_validator' do
      FactoryGirl.build(:account, email: 'ralph@smith.com').should_not eq(FactoryGirl.build(:account))
    end
  end


  describe 'Phone office' do
    it 'it is not nil or blank' do
      FactoryGirl.build(:account, phone_office: nil).should_not be_valid
    end

    it 'it is comprised of only numbers 0-9' do
      FactoryGirl.build(:account, phone_office: 'AAAA/-AA').should_not be_valid
    end
  end
  #
  describe 'Phone ext' do
    it 'can be nil' do
      FactoryGirl.build(:account, phone_office_ext: nil).should be_valid
    end
    it 'can be blank' do
      FactoryGirl.build(:account, phone_office_ext: '').should be_valid
    end
  end
  #
  describe 'Phone cell' do
    it 'it is not nil or blank' do
      FactoryGirl.build(:account, phone_cell: nil).should_not be_valid
    end

    it 'it is comprised of only numbers 0-9' do
      FactoryGirl.build(:account, phone_cell: 'AAAA/-AA').should_not be_valid
    end

  end
  #
  context 'Address Office' do
    describe 'Address Office 1' do
      it 'is not nil or blank' do
        FactoryGirl.build(:account, address_1: nil).should_not be_valid
      end
      it 'has contains text' do
        FactoryGirl.build(:account, address_1: '4587 S. Lookout').should be_valid
      end
    end
  #
    describe 'Address Office 2' do
      it 'is nil or blank' do
        FactoryGirl.build(:account, address_2: '').should be_valid
      end
      it 'contains text' do
        FactoryGirl.build(:account, address_2: 'Ste 214').should be_valid
      end
    end
  #
    describe 'Address Office city' do
      it 'is not nil or blank' do
        FactoryGirl.build(:account, city: nil).should_not be_valid
      end
    end

    describe 'Address Office zipcode' do
      it 'is comprised of only numbers 0-9' do
        FactoryGirl.build(:account, zipcode: 'AAAA/-AA').should_not be_valid
      end
    end
  end



  #describe 'Notes' do
  #  it 'can be blank' do
  #    FactoryGirl.build(:account, notes: 'wwww').should be_valid
  #  end
  #  it 'can be nil' do
  #    FactoryGirl.build(:account, notes: nil).should be_valid
  #  end
  #
  #end

  describe 'Anniversary' do
    it 'should not be nil' do
      FactoryGirl.build(:account, anniversary: nil).should_not be_valid
    end
    it 'should not be blank' do
      FactoryGirl.build(:account, anniversary: ' ').should_not be_valid
    end
    it 'should be a proper date' do
      FactoryGirl.build(:account, anniversary: Date.parse('31-01-2014') ).should be_valid
    end
  end

  describe 'Code' do
    it 'should not be nil' do
      FactoryGirl.build(:account, code: nil).should_not be_valid
    end
    it 'should not be blank' do
      FactoryGirl.build(:account, code: ' ').should_not be_valid
    end

  end
end