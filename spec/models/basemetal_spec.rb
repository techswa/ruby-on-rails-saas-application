require 'spec_helper'

describe Basemetal do

  it 'has a valid factory' do
    FactoryGirl.create(:basemetal).should be_valid
  end

  it 'is invalid without a name' do
     FactoryGirl.build(:basemetal, name: nil).should_not be_valid
  end


  it 'is valid with a name' do
    FactoryGirl.build(:basemetal).should be_valid
  end

  it 'is invalid without an abbreviation' do
    FactoryGirl.build(:basemetal, abbreviation: nil).should_not be_valid
  end

  it 'is invalid if the abbreviation is less than 2 charactors in length' do
    FactoryGirl.build(:basemetal, :abbreviation_1_char).should_not be_valid
  end

  it 'is valid if the abbreviation is 2 or more charactors in length' do
    FactoryGirl.build(:basemetal).should be_valid
  end

#End of Test
end


