require 'spec_helper'

describe Cellcarrier do

  describe 'Factory' do
    context 'valid factory' do
      it 'is operational' do
        FactoryGirl.create(:cellcarrier).should be_valid
      end
    end
  end


  it 'has valid factory' do
    FactoryGirl.create(:cellcarrier).should be_valid
  end

  it 'is invalid without a name' do
    FactoryGirl.build(:cellcarrier, name: nil).should_not be_valid
  end

  it 'is valid with a name' do
    FactoryGirl.build(:cellcarrier, name: 'Sprint').should be_valid
  end

  it 'is invalid without a sms' do
    FactoryGirl.build(:cellcarrier, sms: nil).should_not be_valid
  end

  it 'is invalid with a sms containing puctuation charactors' do
    FactoryGirl.build(:cellcarrier, :sms_punctuation_chars).should_not be_valid
  end

  it 'is invalid with a sms containing alpha charactors' do
    FactoryGirl.build(:cellcarrier, :sms_alpha_chars).should_not be_valid
  end

  it 'is invalid with a sms shorter than 10 numerical charactors' do
    FactoryGirl.build(:cellcarrier, :sms_numeric_short).should_not be_valid
  end

  it 'is invalid with a sms longer than 10 numerical charactors' do
    FactoryGirl.build(:cellcarrier, :sms_numeric_long).should_not be_valid
  end

end