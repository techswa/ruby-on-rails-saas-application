require 'spec_helper'


# :account_id, :address_1, :address_2, :city, :state_id, :zipcode, :anniversary,
# :name, :license, :notes, :phone_cell, :phone_office, :phone_office_ext,
# :location_id, :expires, :key

describe Store do

  describe 'Factory' do
    context 'valid factory' do
      it 'is operational' do
        FactoryGirl.create(:store).should be_valid
      end
    end
  end


  describe 'License Number' do
    context 'Validations' do
      context 'presence_of' do
        it 'is invalid as nil' do
          FactoryGirl.build(:store, license: nil).should_not be_valid
        end
        it 'is valid with data' do
          FactoryGirl.build(:store).should be_valid
        end
      end

      context 'length_of' do
        it 'is more than 5 charactors' do
          FactoryGirl.build(:store, license: '12345').should_not be_valid
        end

        it 'is less than 7 charactors' do
          FactoryGirl.build(:store, license: '1234567').should_not be_valid
        end
      end

      context 'format_of' do
        it 'only allows numbers 0-9' do
          FactoryGirl.build(:store, license: 'AAAAAA').should_not be_valid
        end

      end
    end
  end

  describe 'Key' do
    it 'isn\'t nil' do
      FactoryGirl.build(:store, key: nil).should_not be_valid
    end
    it 'isn\'t blank' do
      FactoryGirl.build(:store, key: '').should_not be_valid
    end
  end

  describe 'Anniversary' do
    it 'should not be blank' do
      FactoryGirl.build(:store, anniversary: nil).should_not be_valid
    end
    it 'should be a proper date' do
      FactoryGirl.build(:store, anniversary: Date.parse('31-01-2014') ).should be_valid
    end
  end

  describe 'Name' do
    it 'it is not blank' do
      FactoryGirl.build(:store, name: nil).should_not be_valid
    end
    it 'it is has text' do
      FactoryGirl.build(:store, name: 'My gold store').should be_valid
    end
  end

  describe 'Address 1' do
    it 'is not nil or blank' do
      FactoryGirl.build(:store, address_1: nil).should_not be_valid
    end
    it 'contains text' do
      FactoryGirl.build(:store, address_1: '4587 S. Lookout').should be_valid
    end
  end

  describe 'Address 2' do
    it 'is nil or blank' do
      FactoryGirl.build(:store, address_2: '').should be_valid
    end
    it 'contains text' do
      FactoryGirl.build(:store, address_2: 'Ste 214').should be_valid
    end
  end

  describe 'City' do
    it 'is not nil or blank' do
      FactoryGirl.build(:store, city: nil).should_not be_valid
    end
  end

  describe 'State_id' do
    it 'isn\'t blank' do
      FactoryGirl.build(:store, zipcode: '').should_not be_valid
    end
    it 'isn\'t nil' do
      FactoryGirl.build(:store, zipcode: nil).should_not be_valid
    end
  end

  describe 'Zipcode' do
    it 'is comprised of only numbers 0-9' do
      FactoryGirl.build(:store, zipcode: 'AAAA/-AA').should_not be_valid
    end
  end

  describe 'Notes' do
    it 'can be blank' do
      FactoryGirl.build(:store, notes: 'wwww').should be_valid
    end
    it 'can be nil' do
      FactoryGirl.build(:store, notes: nil).should be_valid
    end

  end

  describe 'Phone office' do
    it 'it is not nil or blank' do
      FactoryGirl.build(:store, phone_office: nil).should_not be_valid
    end

    it 'it is comprised of only numbers 0-9' do
      FactoryGirl.build(:store, phone_office: 'AAAA/-AA').should_not be_valid
    end
  end

  describe 'Phone ext' do
    it 'can be nil' do
      FactoryGirl.build(:store, phone_office_ext: nil).should be_valid
    end
    it 'can be blank' do
      FactoryGirl.build(:store, phone_office_ext: '').should be_valid
    end
  end

  describe 'Phone cell' do
    it 'it is not nil or blank' do
      FactoryGirl.build(:store, phone_cell: nil).should_not be_valid
    end

    it 'it is comprised of only numbers 0-9' do
      FactoryGirl.build(:store, phone_cell: 'AAAA/-AA').should_not be_valid
    end

  end

#End of test
end