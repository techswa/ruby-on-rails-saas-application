require 'spec_helper'

describe Quote do

  describe 'Factory' do
    context 'valid factory' do
      it 'is operational' do
        FactoryGirl.create(:quote).should be_valid
      end
    end
  end


  describe 'ag_spot' do
    context 'Validations' do
      it 'is invalid as nil' do
        FactoryGirl.build(:quote, ag_spot: nil).should_not be_valid
      end
      it 'is invalid with text data' do
        FactoryGirl.build(:quote, ag_spot: 'd123').should_not be_valid
      end
      it 'is valid with numerical data' do
        FactoryGirl.build(:quote, ag_spot: 123).should be_valid
      end
    end
  end

  describe 'ag_payout' do
    context 'Validations' do
      it 'is invalid as nil' do
        FactoryGirl.build(:quote, ag_payout: nil).should_not be_valid
      end
      it 'is invalid with text data' do
        FactoryGirl.build(:quote, ag_payout: 'd123').should_not be_valid
      end
      it 'is valid with numerical data' do
        FactoryGirl.build(:quote, ag_payout: 123).should be_valid
      end
    end
  end

  describe 'au_spot' do
    context 'Validations' do
      it 'is invalid as nil' do
        FactoryGirl.build(:quote, au_spot: nil).should_not be_valid
      end
      it 'is invalid with text data' do
        FactoryGirl.build(:quote, au_spot: 'd123').should_not be_valid
      end
      it 'is valid with numerical data' do
        FactoryGirl.build(:quote, au_spot: 123).should be_valid
      end
    end
  end

  describe 'au_payout' do
    context 'Validations' do
      it 'is invalid as nil' do
        FactoryGirl.build(:quote, au_payout: nil).should_not be_valid
      end
      it 'is invalid with text data' do
        FactoryGirl.build(:quote, au_payout: 'd123').should_not be_valid
      end
      it 'is valid with numerical data' do
        FactoryGirl.build(:quote, au_payout: 123).should be_valid
      end
    end
  end

  describe 'pt_spot' do
    context 'Validations' do
      it 'is invalid as nil' do
        FactoryGirl.build(:quote, pt_spot: nil).should_not be_valid
      end
      it 'is invalid with text data' do
        FactoryGirl.build(:quote, pt_spot: 'd123').should_not be_valid
      end
      it 'is valid with numerical data' do
        FactoryGirl.build(:quote, pt_spot: 123).should be_valid
      end
    end
  end

  describe 'pt_payout' do
    context 'Validations' do
      it 'is invalid as nil' do
        FactoryGirl.build(:quote, pt_payout: nil).should_not be_valid
      end
      it 'is invalid with text data' do
        FactoryGirl.build(:quote, pt_payout: 'd123').should_not be_valid
      end
      it 'is valid with numerical data' do
        FactoryGirl.build(:quote, pt_payout: 123).should be_valid
      end
    end
  end



  describe 'user_ref' do
    context 'Validations' do
      it 'is invalid as nil' do
        FactoryGirl.build(:quote, user_ref: nil).should_not be_valid
      end
      it 'is valid with text' do
        FactoryGirl.build(:quote, user_ref: 'text').should be_valid
      end
    end
  end

end
