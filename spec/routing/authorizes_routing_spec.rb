require "spec_helper"

describe AuthorizesController do
  describe "routing" do

    it "routes to #index" do
      get("/authorizes").should route_to("authorizes#index")
    end

    it "routes to #new" do
      get("/authorizes/new").should route_to("authorizes#new")
    end

    it "routes to #show" do
      get("/authorizes/1").should route_to("authorizes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/authorizes/1/edit").should route_to("authorizes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/authorizes").should route_to("authorizes#create")
    end

    it "routes to #update" do
      put("/authorizes/1").should route_to("authorizes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/authorizes/1").should route_to("authorizes#destroy", :id => "1")
    end

  end
end
