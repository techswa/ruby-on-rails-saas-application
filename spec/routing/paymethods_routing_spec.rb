require "spec_helper"

describe PaymethodsController do
  describe "routing" do

    it "routes to #index" do
      get("/paymethods").should route_to("paymethods#index")
    end

    it "routes to #new" do
      get("/paymethods/new").should route_to("paymethods#new")
    end

    it "routes to #show" do
      get("/paymethods/1").should route_to("paymethods#show", :id => "1")
    end

    it "routes to #edit" do
      get("/paymethods/1/edit").should route_to("paymethods#edit", :id => "1")
    end

    it "routes to #create" do
      post("/paymethods").should route_to("paymethods#create")
    end

    it "routes to #update" do
      put("/paymethods/1").should route_to("paymethods#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/paymethods/1").should route_to("paymethods#destroy", :id => "1")
    end

  end
end
