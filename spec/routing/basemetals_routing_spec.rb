require "spec_helper"

describe BasemetalsController do
  describe "routing" do

    it "routes to #index" do
      get("/basemetals").should route_to("basemetals#index")
    end

    it "routes to #new" do
      get("/basemetals/new").should route_to("basemetals#new")
    end

    it "routes to #show" do
      get("/basemetals/1").should route_to("basemetals#show", :id => "1")
    end

    it "routes to #edit" do
      get("/basemetals/1/edit").should route_to("basemetals#edit", :id => "1")
    end

    it "routes to #create" do
      post("/basemetals").should route_to("basemetals#create")
    end

    it "routes to #update" do
      put("/basemetals/1").should route_to("basemetals#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/basemetals/1").should route_to("basemetals#destroy", :id => "1")
    end

  end
end
