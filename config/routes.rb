Fmbstore::Application.routes.draw do


  #get '/test', :to => 'tests#index'
  #get '/test/:id', :to => 'tests#show'

  resources :tests

  #get "administration/index"

  #get "administration/users"

  #get "reports/index"

  #get "reports/sales"

  #resources :onboards
  #get "onboard/new"
  #
  #post "onboard/create" => "onboard#create", :as => :onboard_create
  #
  #get "onboard/show"
  #
  #get "onboard/edit"

  resources :authorizes

  resources :locations

  resources :standings

  resources :paymethods

  #
  #get 'portal', :to => 'portal#index'
  #
  #get "portal/new"
  #
  #get "portal/show"

 resources :comments



  resources :basemetals


  resources :metals


  resources :stones

  resources :eyecolors
  resources :genders
  resources :items
  resources :licenses
  resources :languages
  resources :cellcarriers

  resources :metals
  resources :months
  resources :payments
  resources :products
  resources :productcategories
  resources :purchases

  get 'quotes/current', :to => 'quotes#current'
  resources :quotes
  resources :ratingsystems
  resources :sellers
  resources :states
  resources :stoneclarities
  resources :stonecolors
  resources :stonecuts

  get 'stores/current', :to => 'stores#current'
  get 'stores/list', :to => 'stores#list'


  resources :stonesizes
  resources :stonetypes
  resources :tags
  resources :unitsizes
  resources :unitweights

  #get 'users/current', :to => 'users#current'

  devise_for :users


  #get 'admin/users/clientedit', :to => 'users#clientedit'
  scope '/admin' do
    get 'users/new_owner', :to => 'users#new_owner'
    resources :users
  end

  #resources :buyers
  #resources :managers


  #get "home/index"



  resources :accounts do
    resources :stores, :users

    #resources :commen
    # clts
  end

 # scope '/accounts' do
    put 'send_temp_password/:account_id/user/:id', to: 'users#send_temp_password', as: :send_temp_password
    get 'administration', to: 'administration#index'
    get 'reports', to: 'reports#index'
 # end


  #send to authenticated index page when logged in
  authenticated :user do
    root :to => 'home#index'
  end
  # send to unauthenticated user index page
  root :to => 'home#welcome'


  #get 'authorizes/list_stores', :to => 'authorizes#list_stores'
end
