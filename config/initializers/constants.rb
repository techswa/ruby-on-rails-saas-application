  # User defined constants

  # Roles for use with Rolify
  ROLE_ADMIN = "admin"
  ROLE_CSR = "csr"
  ROLE_ACCOUNT = "account"
  ROLE_MANAGER = "manager"
  ROLE_BUYER = "buyer"
  ROLE_LEO = "leo"
  ROLE_DEMO = "demo"

