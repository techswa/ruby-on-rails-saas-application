# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130805200215) do

  create_table "accounts", :force => true do |t|
    t.string   "account"
    t.boolean  "active"
    t.string   "company"
    t.string   "contact_name_first"
    t.string   "contact_name_last"
    t.string   "contact_title"
    t.string   "email"
    t.string   "phone_office"
    t.string   "phone_office_ext"
    t.string   "phone_cell"
    t.string   "address_office_1"
    t.string   "address_office_2"
    t.string   "address_office_city"
    t.integer  "address_office_state_id"
    t.string   "address_office_zipcode"
    t.string   "address_mail_1"
    t.string   "address_mail_2"
    t.string   "address_mail_city"
    t.integer  "address_mail_state_id"
    t.string   "address_mail_zipcode"
    t.date     "anniversary"
    t.text     "notes"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "authorizes", :force => true do |t|
    t.integer  "account_id"
    t.integer  "store_id"
    t.boolean  "active"
    t.boolean  "locked"
    t.integer  "user_id"
    t.integer  "grace"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.boolean  "renewed"
    t.date     "renewal_date"
    t.date     "expiration_date"
  end

  create_table "cellcarriers", :force => true do |t|
    t.string   "name"
    t.string   "sms"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "eyecolors", :force => true do |t|
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "genders", :force => true do |t|
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "items", :force => true do |t|
    t.string   "license"
    t.integer  "purchase_id"
    t.integer  "user_id"
    t.float    "scrap_percent"
    t.integer  "payment"
    t.integer  "gender_id"
    t.integer  "product_id"
    t.integer  "metal_id"
    t.float    "weight"
    t.integer  "unitweight_id"
    t.float    "size"
    t.integer  "unitsize_id"
    t.string   "description"
    t.string   "hallmark"
    t.string   "serialnumber"
    t.string   "engraving"
    t.string   "comments"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "languages", :force => true do |t|
    t.integer  "sort"
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "licenses", :force => true do |t|
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "metals", :force => true do |t|
    t.integer  "sort"
    t.integer  "language_id"
    t.string   "base"
    t.string   "purity"
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "months", :force => true do |t|
    t.string   "abbreviation"
    t.string   "name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "number"
  end

  create_table "payments", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "productcategories", :force => true do |t|
    t.integer  "sort"
    t.integer  "language_id"
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "products", :force => true do |t|
    t.integer  "sort"
    t.integer  "language_id"
    t.integer  "productcategory_id"
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "purchases", :force => true do |t|
    t.string   "license"
    t.integer  "store_id"
    t.integer  "user_id"
    t.integer  "tag_id"
    t.string   "receipt"
    t.integer  "temporarylocation_id"
    t.integer  "seller_id"
    t.text     "comment"
    t.float    "scarp_percent"
    t.integer  "pay_amount"
    t.integer  "pay_method"
    t.string   "pay_number"
    t.string   "pay_comment"
    t.integer  "promotion_id"
    t.integer  "conversion_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "quotes", :force => true do |t|
    t.integer  "account_id"
    t.integer  "store_id"
    t.integer  "user_id"
    t.datetime "date_time"
    t.integer  "au_spot"
    t.integer  "au_payout"
    t.integer  "ag_spot"
    t.integer  "ag_payout"
    t.integer  "pt_spot"
    t.integer  "pt_payout"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "ratingsystems", :force => true do |t|
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "sellers", :force => true do |t|
    t.integer  "state_id"
    t.string   "license_id"
    t.string   "number"
    t.string   "month_id"
    t.string   "day"
    t.string   "year"
    t.string   "name_last"
    t.string   "name_first"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "city"
    t.integer  "astate_id"
    t.string   "zip"
    t.integer  "height_feet"
    t.integer  "height_inches"
    t.integer  "gender_id"
    t.integer  "eyecolor_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "email"
  end

  create_table "states", :force => true do |t|
    t.string   "name",         :limit => 30
    t.string   "abbreviation", :limit => 2
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "stoneclarities", :force => true do |t|
    t.integer  "language_id"
    t.string   "method"
    t.integer  "sort"
    t.string   "name"
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "stonecolors", :force => true do |t|
    t.integer  "language_id"
    t.string   "method"
    t.integer  "sort"
    t.string   "name"
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "ratingsystem_id"
  end

  create_table "stonecuts", :force => true do |t|
    t.integer  "language_id"
    t.integer  "sort"
    t.string   "name"
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "stonesizes", :force => true do |t|
    t.integer  "language_id"
    t.integer  "sort"
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "stonetypes", :force => true do |t|
    t.integer  "language_id"
    t.integer  "sort"
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "stores", :force => true do |t|
    t.integer  "account_id"
    t.string   "company"
    t.integer  "user_id"
    t.string   "phone_office"
    t.string   "phone_office_ext"
    t.string   "phone_cell"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "city"
    t.integer  "state_id"
    t.date     "anniversary"
    t.text     "notes"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "zipcode"
    t.string   "license"
    t.boolean  "remotelocation"
  end

  create_table "tags", :force => true do |t|
    t.string   "number"
    t.integer  "purchase_id"
    t.datetime "release"
    t.boolean  "processed"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "unitsizes", :force => true do |t|
    t.integer  "sort"
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "unitweights", :force => true do |t|
    t.integer  "sort"
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.float    "factor"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.boolean  "active"
    t.integer  "account_id"
    t.string   "cellnumber"
    t.integer  "cellcarrier_id"
    t.string   "name_first"
    t.string   "name_last"
    t.text     "notes"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

end
