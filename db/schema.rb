# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140329160535) do

  create_table "accounts", :force => true do |t|
    t.string   "number"
    t.string   "name"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "title"
    t.string   "email"
    t.string   "phone_office"
    t.string   "phone_office_ext"
    t.string   "phone_cell"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "city"
    t.integer  "state_id"
    t.string   "zipcode"
    t.date     "anniversary"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "standing_id"
    t.string   "code"
    t.string   "key"
    t.string   "slug"
  end

  add_index "accounts", ["slug"], :name => "index_accounts_on_slug"

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                                :default => "", :null => false
    t.string   "encrypted_password",                   :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.string   "dealer_master",          :limit => 10
    t.string   "dealer_outlet",          :limit => 10
    t.integer  "role"
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "authorizes", :force => true do |t|
    t.integer  "store_id"
    t.integer  "user_id"
    t.boolean  "active"
    t.date     "payment_date"
    t.integer  "payment_cents"
    t.integer  "paymethod_id"
    t.string   "payment_note"
    t.date     "expiration_date"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "basemetals", :force => true do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "categories", :force => true do |t|
    t.string   "name",        :limit => 10
    t.text     "description"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "cellcarriers", :force => true do |t|
    t.string   "name"
    t.string   "sms"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "colors", :force => true do |t|
    t.string   "name",         :limit => 35
    t.string   "abbreviation", :limit => 3
    t.text     "description"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "comments", :force => true do |t|
    t.text     "content"
    t.boolean  "sticky"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "user_id"
  end

  create_table "counties", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "cuts", :force => true do |t|
    t.string   "name",        :limit => 35
    t.text     "description"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "dealers", :force => true do |t|
    t.string   "id_dealer",   :limit => 6
    t.string   "id_outlet",   :limit => 6
    t.string   "name",        :limit => 35
    t.string   "address",     :limit => 35
    t.string   "city",        :limit => 25
    t.string   "state",       :limit => 2
    t.string   "zipcode",     :limit => 5
    t.string   "phone",       :limit => 35
    t.string   "contact",     :limit => 35
    t.text     "note"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.boolean  "active"
    t.datetime "anniversary"
    t.integer  "county_id"
  end

  create_table "eyecolors", :force => true do |t|
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "name"
  end

  create_table "genders", :force => true do |t|
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "name"
  end

  create_table "geometry_columns", :id => false, :force => true do |t|
    t.string  "f_table_catalog",   :limit => 256, :null => false
    t.string  "f_table_schema",    :limit => 256, :null => false
    t.string  "f_table_name",      :limit => 256, :null => false
    t.string  "f_geometry_column", :limit => 256, :null => false
    t.integer "coord_dimension",                  :null => false
    t.integer "srid",                             :null => false
    t.string  "type",              :limit => 30,  :null => false
  end

  create_table "items", :force => true do |t|
    t.string   "license"
    t.integer  "user_id"
    t.string   "comments"
    t.string   "description"
    t.string   "engraving"
    t.string   "hallmark"
    t.integer  "gender_id"
    t.integer  "metal_id"
    t.integer  "payment"
    t.integer  "product_id"
    t.integer  "purchase_id"
    t.integer  "scrap_percent"
    t.string   "serialnumber"
    t.string   "size"
    t.integer  "unitsize_id"
    t.integer  "weight"
    t.integer  "unitweight_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "languages", :force => true do |t|
    t.string   "abbreviation"
    t.string   "description"
    t.integer  "sort"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "licenses", :force => true do |t|
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "name"
  end

  create_table "locations", :force => true do |t|
    t.string   "name"
    t.integer  "sort"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "metals", :force => true do |t|
    t.string   "name",         :limit => 50
    t.string   "abbreviation", :limit => 6
    t.string   "purity",       :limit => 4
    t.string   "description",  :limit => 50
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.integer  "basemetal_id"
    t.integer  "language_id"
    t.integer  "sort"
    t.float    "factor"
  end

  create_table "metals-good", :force => true do |t|
    t.string   "abbreviation"
    t.integer  "basemetal_id"
    t.string   "description"
    t.integer  "language_id"
    t.float    "purity"
    t.integer  "sort"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "meters", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "paymethods", :force => true do |t|
    t.string   "name"
    t.string   "use"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "productcategories", :force => true do |t|
    t.string   "description"
    t.integer  "language_id"
    t.string   "name"
    t.integer  "sort"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "products", :force => true do |t|
    t.string   "description"
    t.integer  "language_id"
    t.string   "name"
    t.integer  "productcategory_id"
    t.integer  "sort"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "purchases", :force => true do |t|
    t.string   "license"
    t.integer  "item_id"
    t.integer  "user_id"
    t.integer  "tag_id"
    t.string   "receipt"
    t.integer  "temporarylocation_id"
    t.integer  "seller_id"
    t.text     "comment"
    t.float    "scarp_percent"
    t.integer  "pay_amount"
    t.integer  "pay_method"
    t.string   "pay_number"
    t.string   "pay_comment"
    t.integer  "promotion_id"
    t.integer  "conversion_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "quotes", :force => true do |t|
    t.integer  "account_id"
    t.integer  "ag_spot"
    t.integer  "ag_payout"
    t.integer  "au_spot"
    t.integer  "au_payout"
    t.integer  "pt_spot"
    t.integer  "pt_payout"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "user_ref"
  end

  create_table "ratingsystems", :force => true do |t|
    t.string   "abbreviation"
    t.string   "description"
    t.integer  "sort"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "report_access", :force => true do |t|
    t.text "report_access_id"
  end

  create_table "report_access_users", :id => false, :force => true do |t|
    t.integer "report_access_id"
    t.integer "user_id"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "sellers", :force => true do |t|
    t.integer  "state_id"
    t.string   "license_id"
    t.string   "number"
    t.string   "dob_month"
    t.string   "dob_day"
    t.string   "dob_year"
    t.string   "name_last"
    t.string   "name_first"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "city"
    t.integer  "astate_id"
    t.string   "zipcode"
    t.integer  "height_feet"
    t.integer  "height_inches"
    t.integer  "gender_id"
    t.integer  "eyes_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "spatial_ref_sys", :id => false, :force => true do |t|
    t.integer "srid",                      :null => false
    t.string  "auth_name", :limit => 256
    t.integer "auth_srid"
    t.string  "srtext",    :limit => 2048
    t.string  "proj4text", :limit => 2048
  end

  create_table "staffmemberships", :force => true do |t|
    t.integer  "dealer_id"
    t.integer  "user_id"
    t.string   "id_outlet",  :limit => 6
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "standings", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "sort"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "model"
  end

  create_table "states", :force => true do |t|
    t.string   "name",         :limit => 30
    t.string   "abbreviation", :limit => 2
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "stonecolors", :force => true do |t|
    t.integer  "ratingsystem_id"
    t.string   "abbreviation"
    t.string   "description"
    t.integer  "language_id"
    t.string   "method"
    t.string   "name"
    t.integer  "sort"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "stones", :force => true do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.string   "description"
    t.integer  "sort"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "stones-org", :force => true do |t|
    t.string   "name",         :limit => 50
    t.string   "abbreviation", :limit => 3
    t.text     "description"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "stonesizes", :force => true do |t|
    t.string   "abbreviation"
    t.string   "description"
    t.integer  "language_id"
    t.integer  "sort"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "stonetypes", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "language_id"
    t.integer  "sort"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "stores", :force => true do |t|
    t.integer  "account_id"
    t.string   "license"
    t.string   "name"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "city"
    t.integer  "state_id"
    t.string   "zipcode"
    t.string   "phone_office"
    t.string   "phone_office_ext"
    t.date     "anniversary"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "location_id"
    t.string   "key"
    t.integer  "standing_id"
    t.string   "printkey"
  end

  create_table "unitsizes", :force => true do |t|
    t.string   "abbreviation"
    t.string   "description"
    t.integer  "sort"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "unitweights", :force => true do |t|
    t.string   "abbreviation"
    t.string   "description"
    t.integer  "sort"
    t.float    "factor"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                :default => "", :null => false
    t.string   "encrypted_password",                   :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.string   "first_name",             :limit => 15
    t.string   "last_name",              :limit => 19
    t.boolean  "locked"
    t.integer  "report_level"
    t.string   "phone_cell"
    t.integer  "cellcarrier_id"
    t.integer  "account_id"
    t.boolean  "owner"
    t.string   "slug"
    t.integer  "role_id"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["slug"], :name => "index_users_on_uuid"

  create_table "users_roles", :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

end
