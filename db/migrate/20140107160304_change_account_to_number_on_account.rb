class ChangeAccountToNumberOnAccount < ActiveRecord::Migration
  def up
    rename_column :accounts, :account, :number
  end

  def down
  end
end
