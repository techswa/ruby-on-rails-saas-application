class CreatePaymethods < ActiveRecord::Migration
  def change
    create_table :paymethods do |t|
      t.string :name
      t.string :use

      t.timestamps
    end
  end
end
