class CreateStonetypes < ActiveRecord::Migration
  def change
    create_table :stonetypes do |t|
      t.string :name
      t.string :description
      t.integer :language_id
      t.integer :sort

      t.timestamps
    end
  end
end
