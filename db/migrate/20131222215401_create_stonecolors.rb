class CreateStonecolors < ActiveRecord::Migration
  def change
    create_table :stonecolors do |t|
      t.integer :ratingsystem_id
      t.string :abbreviation
      t.string :description
      t.integer :language_id
      t.string :method
      t.string :name
      t.integer :sort

      t.timestamps
    end
  end
end
