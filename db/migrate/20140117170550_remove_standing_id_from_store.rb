class RemoveStandingIdFromStore < ActiveRecord::Migration
  def up
    remove_column :stores, :standing_id
  end

  def down
  end
end
