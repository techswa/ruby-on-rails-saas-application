class AddNameToEyecolor < ActiveRecord::Migration
  def change
    add_column :eyecolors, :name, :string
  end
end
