class CreateEyecolors < ActiveRecord::Migration
  def change
    create_table :eyecolors do |t|
      t.string :abbreviation
      t.string :description

      t.timestamps
    end
  end
end
