class CreateUnitweights < ActiveRecord::Migration
  def change
    create_table :unitweights do |t|
      t.string :abbreviation
      t.string :description
      t.integer :sort
      t.float :factor

      t.timestamps
    end
  end
end
