class ChangeGuidToKeyOnAccount < ActiveRecord::Migration
  def up
    rename_column :accounts, :guid, :key
  end

  def down
  end
end
