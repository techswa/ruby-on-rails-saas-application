class RemoveUserFromStores < ActiveRecord::Migration
  def up
    remove_column :stores, :user_id
  end

  def down
  end
end
