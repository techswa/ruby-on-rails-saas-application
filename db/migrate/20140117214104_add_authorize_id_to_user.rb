class AddAuthorizeIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :authorize_id, :integer
  end
end
