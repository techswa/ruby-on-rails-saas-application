class CreateRatingsystems < ActiveRecord::Migration
  def change
    create_table :ratingsystems do |t|
      t.string :abbreviation
      t.string :description
      t.integer :sort

      t.timestamps
    end
  end
end
