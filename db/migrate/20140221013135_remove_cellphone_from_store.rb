class RemoveCellphoneFromStore < ActiveRecord::Migration
  def up
    remove_column :stores, :phone_cell
  end

  def down
  end
end
