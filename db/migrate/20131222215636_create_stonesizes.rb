class CreateStonesizes < ActiveRecord::Migration
  def change
    create_table :stonesizes do |t|
      t.string :abbreviation
      t.string :description
      t.integer :language_id
      t.integer :sort

      t.timestamps
    end
  end
end
