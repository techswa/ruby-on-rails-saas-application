class CreateStones < ActiveRecord::Migration
  def change
    create_table :stones do |t|
      t.string :name
      t.string :abbreviation
      t.string :description
      t.integer :sort

      t.timestamps
    end
  end
end
