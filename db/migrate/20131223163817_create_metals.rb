class CreateMetals < ActiveRecord::Migration
  def change
    create_table :metals do |t|
      t.string :abbreviation
      t.integer :basemetal_id
      t.string :description
      t.integer :language_id
      t.float :purity
      t.integer :sort

      t.timestamps
    end
  end
end
