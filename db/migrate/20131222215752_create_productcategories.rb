class CreateProductcategories < ActiveRecord::Migration
  def change
    create_table :productcategories do |t|
      t.string :description
      t.integer :language_id
      t.string :name
      t.integer :sort

      t.timestamps
    end
  end
end
