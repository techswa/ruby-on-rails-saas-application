class CreateLocation < ActiveRecord::Migration
  def up
    create_table :locations do |t|

      t.string :name
      t.integer :sort

      t.timestamps
    end
  end

  def down
  end
end
