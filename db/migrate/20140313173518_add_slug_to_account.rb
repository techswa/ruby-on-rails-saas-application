class AddSlugToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :slug, :string
    add_index :accounts, :slug
  end
end
