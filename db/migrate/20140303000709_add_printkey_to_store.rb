class AddPrintkeyToStore < ActiveRecord::Migration
  def change
    add_column :stores, :printkey, :string
  end
end
