class RenameZipToZipcodeOnSeller < ActiveRecord::Migration
  def up
    rename_column :sellers, :zip, :zipcode
  end

  def down
  end
end
