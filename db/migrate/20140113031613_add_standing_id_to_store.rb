class AddStandingIdToStore < ActiveRecord::Migration
  def change
    add_column :stores, :standing_id, :integer
  end
end
