class RemoveNotesFromAccounts < ActiveRecord::Migration
  def up
    remove_column :accounts, :notes
  end

  def down
  end
end
