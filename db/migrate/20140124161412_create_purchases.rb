class CreatePurchases < ActiveRecord::Migration
  def up
    create_table :purchases do |t|
      t.string :license
      t.integer :purchase_id
      t.integer :user_id
      t.integer :tag_id
      t.string :receipt
      t.integer :temporarylocation_id
      t.integer :seller_id
      t.text :comment
      t.float :scarp_percent
      t.integer :pay_amount
      t.integer :pay_method
      t.string :pay_number
      t.string :pay_comment
      t.integer :promotion_id
      t.integer :conversion_id

      t.timestamps
    end
  end

  def down
  end
end
