class AddRemoteAndExpiresAndKeyToStore < ActiveRecord::Migration
  def change
    add_column :stores, :remote, :integer
    add_column :stores, :expires, :date
    add_column :stores, :key, :string
  end
end
