class RemoveColumnAuthorizeIdOnUsers < ActiveRecord::Migration
  def up
    remove_column :users, :authorize_id
  end

  def down
  end
end
