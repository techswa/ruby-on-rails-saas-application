class AddNameToGender < ActiveRecord::Migration
  def change
    add_column :genders, :name, :string
  end
end
