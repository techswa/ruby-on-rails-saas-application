class ChangeUseridToUserrefOnQuotes < ActiveRecord::Migration
  def up
    remove_column :quotes, :user_id
    add_column :quotes, :user_ref, :string
  end

  def down
  end
end
