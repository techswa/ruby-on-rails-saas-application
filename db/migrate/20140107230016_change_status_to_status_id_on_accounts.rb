class ChangeStatusToStatusIdOnAccounts < ActiveRecord::Migration
  def up
    rename_column :accounts, :status, :status_id
  end

  def down
  end
end
