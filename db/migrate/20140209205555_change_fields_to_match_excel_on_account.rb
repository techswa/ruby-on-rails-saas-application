class ChangeFieldsToMatchExcelOnAccount < ActiveRecord::Migration
  def up
    rename_column :accounts, :contact_first_name, :first_name
    rename_column :accounts, :contact_last_name, :last_name
    rename_column :accounts, :contact_title, :first_title
  end

  def down
  end
end
