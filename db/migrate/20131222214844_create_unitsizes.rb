class CreateUnitsizes < ActiveRecord::Migration
  def change
    create_table :unitsizes do |t|
      t.string :abbreviation
      t.string :description
      t.integer :sort

      t.timestamps
    end
  end
end
