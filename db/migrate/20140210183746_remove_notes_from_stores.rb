class RemoveNotesFromStores < ActiveRecord::Migration
  def up
    remove_column :stores, :notes
  end

  def down
  end
end
