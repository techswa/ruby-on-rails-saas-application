class CreateLicenses < ActiveRecord::Migration
  def change
    create_table :licenses do |t|
      t.string :abbreviation
      t.string :description

      t.timestamps
    end
  end
end
