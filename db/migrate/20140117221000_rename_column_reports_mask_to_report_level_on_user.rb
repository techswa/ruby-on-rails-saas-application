class RenameColumnReportsMaskToReportLevelOnUser < ActiveRecord::Migration
  def up
    rename_column :users, :reports_mask, :report_level
  end

  def down
  end
end
