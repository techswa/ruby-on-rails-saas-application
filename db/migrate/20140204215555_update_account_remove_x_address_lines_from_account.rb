class UpdateAccountRemoveXAddressLinesFromAccount < ActiveRecord::Migration

  def up
    rename_column :accounts, :address_office_1, :address_1
    rename_column :accounts, :address_office_2, :address_2
    rename_column :accounts, :address_office_city, :city
    rename_column :accounts, :address_office_state_id, :state_id
    rename_column :accounts, :address_office_zipcode, :zipcode
  end

  def down
  end
end
