class RemoveExpireFromStore < ActiveRecord::Migration
  def up
    remove_column :stores, :expires
  end

  def down
  end
end
