class DropStatusIdToAuthorize < ActiveRecord::Migration
  def up
    remove_column :authorizes, :status_id
  end

  def down
  end
end
