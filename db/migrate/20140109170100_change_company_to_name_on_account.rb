class ChangeCompanyToNameOnAccount < ActiveRecord::Migration
  def up
    rename_column :accounts, :company, :name
  end

  def down
  end
end
