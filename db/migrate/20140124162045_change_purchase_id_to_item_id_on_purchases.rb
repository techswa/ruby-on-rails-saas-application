class ChangePurchaseIdToItemIdOnPurchases < ActiveRecord::Migration
  def up
    rename_column :purchases, :purchase_id, :item_id
  end

  def down
  end
end
