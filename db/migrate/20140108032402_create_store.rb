class CreateStore < ActiveRecord::Migration
  def up
    create_table :stores do |t|
      t.integer :account_id
      t.string :license
      t.string :company
      t.string :address_1
      t.string :address_2
      t.string :city
      t.integer :state_id
      t.string :zipcode
      t.string :phone_office
      t.string :phone_office_ext
      t.string :phone_cell
      t.date :anniverary
      t.text :notes
      t.timestamps
    end
  end

  def down
  end
end
