class RenameUuidToSlugOnUser < ActiveRecord::Migration
  def up
    rename_column :users, :uuid, :slug
  end

  def down
  end
end
