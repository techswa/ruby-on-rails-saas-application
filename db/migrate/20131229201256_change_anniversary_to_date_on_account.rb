class ChangeAnniversaryToDateOnAccount < ActiveRecord::Migration
  def up
    change_column :accounts, :anniversary, :date
  end

  def down
  end
end
