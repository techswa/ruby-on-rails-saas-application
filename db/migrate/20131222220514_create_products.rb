class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :description
      t.integer :language_id
      t.string :name
      t.integer :productcateory_id
      t.integer :sort

      t.timestamps
    end
  end
end
