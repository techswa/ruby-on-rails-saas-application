class RemoveColumnsIdDealerAndIdAccountFromUser < ActiveRecord::Migration
  def up
    remove_column :users, :id_dealer
    remove_column :users, :id_outlet
  end

  def down
  end
end
