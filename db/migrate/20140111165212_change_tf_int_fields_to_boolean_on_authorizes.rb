class ChangeTfIntFieldsToBooleanOnAuthorizes < ActiveRecord::Migration
  def up
      remove_column :authorizes, :active
      remove_column :authorizes, :locked
      remove_column :authorizes, :renewed
  end

  def down
  end
end
