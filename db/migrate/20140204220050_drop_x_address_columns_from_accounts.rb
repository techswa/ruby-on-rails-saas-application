class DropXAddressColumnsFromAccounts < ActiveRecord::Migration
  def up
    remove_column :accounts, :address_mail_1
    remove_column :accounts, :address_mail_2
    remove_column :accounts, :address_mail_city
    remove_column :accounts, :address_mail_state_id
    remove_column :accounts, :address_mail_zipcode

  end

  def down
  end
end
