class RenamePaymentAmountToPaymentCentsOnAuthorizes < ActiveRecord::Migration
  def up
    rename_column :authorizes, :payment_amount, :payment_cents
  end

  def down
  end
end
