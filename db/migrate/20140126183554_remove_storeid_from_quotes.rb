class RemoveStoreidFromQuotes < ActiveRecord::Migration
  def up
    remove_column :quotes, :store_id
  end

  def down
  end
end
