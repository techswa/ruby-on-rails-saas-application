class ChangeRemoteToLocationIdOnStore < ActiveRecord::Migration
  def up
    rename_column :stores, :remote, :location_id
  end

  def down
  end
end
