class AddStandingIdToAuthorize < ActiveRecord::Migration
  def change
    add_column :authorizes, :standing_id, :integer
  end
end
