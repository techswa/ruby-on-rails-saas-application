class RemoveAccountIdFromAuthorize < ActiveRecord::Migration
  def up
    remove_column :authorizes, :account_id
  end

  def down
  end
end
