class CreateAccounts < ActiveRecord::Migration
  def up
    create_table :accounts do |t|
      t.string :account
      t.boolean :active
      t.string :company
      t.string :contact_name_first
      t.string :contact_name_last
      t.string :contact_title
      t.string :email
      t.string :phone_office
      t.string :phone_office_ext
      t.string :phone_cell
      t.string :address_office_1
      t.string :address_office_2
      t.string :address_office_city
      t.references :address_office_state
      t.string :address_office_zipcode
      t.string :address_mail_1
      t.string :address_mail_2
      t.string :address_mail_city
      t.references :address_mail_state
      t.string :address_mail_zipcode
      t.datetime :anniversary
      t.text :notes
      t.timestamps
    end
  end

  def down
  end
end
