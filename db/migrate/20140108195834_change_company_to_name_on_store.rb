class ChangeCompanyToNameOnStore < ActiveRecord::Migration
  def up
    rename_column :stores, :company, :name
  end

  def down
  end
end
