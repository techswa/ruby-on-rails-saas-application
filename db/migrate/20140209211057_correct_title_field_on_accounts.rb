class CorrectTitleFieldOnAccounts < ActiveRecord::Migration
  def up
    rename_column :accounts, :first_title, :title
  end

  def down
  end
end
