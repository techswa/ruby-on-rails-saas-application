class CreateItem < ActiveRecord::Migration
  def up
    create_table :items do |t|

      t.string :license
      t.integer :user_id
      t.string :comments
      t.string :description
      t.string :engraving
      t.string :hallmark
      t.integer :gender_id
      t.integer :metal_id
      t.integer :payment
      t.integer :product_id
      t.integer :purchase_id
      t.integer :scrap_percent
      t.string :serialnumber
      t.string :size
      t.integer :unitsize_id
      t.integer :weight
      t.integer :unitweight_id

      t.timestamps
    end

  end
 # :comments, :description, :engraving, :gender_id, :hallmark,
 # :#license, :metal_id, :payment, :product_id, :purchase_id, :scrap_percent,
 # :serialnumber, :size, :unitsize_id, :unitweight_id, :user_id, :weight
  def down
  end
end
