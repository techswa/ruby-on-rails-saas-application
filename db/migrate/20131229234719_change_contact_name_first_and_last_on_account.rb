class ChangeContactNameFirstAndLastOnAccount < ActiveRecord::Migration
  def up
    rename_column :accounts, :contact_name_first, :contact_first_name
    rename_column :accounts, :contact_name_last, :contact_last_name
  end

  def down
  end
end
