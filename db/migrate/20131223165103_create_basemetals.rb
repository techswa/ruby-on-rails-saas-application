class CreateBasemetals < ActiveRecord::Migration
  def change
    create_table :basemetals do |t|
      t.string :name
      t.string :abbreviation

      t.timestamps
    end
  end
end
