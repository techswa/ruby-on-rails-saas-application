class CreateAuthorizes < ActiveRecord::Migration
  def change
    create_table :authorizes do |t|
      t.integer :store_id
      t.integer :user_id
      t.boolean :active
      t.date :payment_date
      t.integer :payment_amount
      t.integer :paymethod_id
      t.string :payment_note
      t.date :expiration_date

      t.timestamps
    end
  end
end
