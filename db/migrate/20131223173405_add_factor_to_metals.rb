class AddFactorToMetals < ActiveRecord::Migration
  def change
    add_column :metals, :factor, :float
  end
end
