class ChangeStatusToStandingOnAccount < ActiveRecord::Migration
  def up
    rename_column :accounts, :status_id, :standing_id
  end

  def down
  end
end
