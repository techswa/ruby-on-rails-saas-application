class ChangeActiveToStatusOnAccount < ActiveRecord::Migration
  def up
    remove_column :accounts, :active
    add_column :accounts, :status, :integer
  end

  def down
  end
end
