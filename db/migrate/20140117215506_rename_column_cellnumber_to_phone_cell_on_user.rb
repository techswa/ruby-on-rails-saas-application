class RenameColumnCellnumberToPhoneCellOnUser < ActiveRecord::Migration
  def up
    rename_column :users, :cellnumber, :phone_cell
  end

  def down
  end
end
