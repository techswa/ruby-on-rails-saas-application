class ChangeSlugTypeOnUser < ActiveRecord::Migration
  def up
    change_column :users, :slug, :string
  end

  def down
  end
end
