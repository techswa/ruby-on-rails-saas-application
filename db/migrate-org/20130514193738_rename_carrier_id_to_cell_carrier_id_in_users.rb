class RenameCarrierIdToCellCarrierIdInUsers < ActiveRecord::Migration
  def up
    rename_column :users, :carrier_id, :cellcarrier_id

  end

  def down
  end
end
