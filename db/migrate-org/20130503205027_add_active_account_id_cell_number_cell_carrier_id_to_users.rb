class AddActiveAccountIdCellNumberCellCarrierIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :active, :boolean
    add_column :users, :account_id, :integer
    add_column :users, :cellnumber, :string
    add_column :users, :carrier_id, :integer
  end
end
