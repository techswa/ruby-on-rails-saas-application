class AddRatingsystemIdToStonecolors < ActiveRecord::Migration
  def change
    add_column :stonecolors, :ratingsystem_id, :integer
  end
end
