class CreateProductcategories < ActiveRecord::Migration
  def change
    create_table :productcategories do |t|
      t.integer :sort
      t.integer :language_id
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
