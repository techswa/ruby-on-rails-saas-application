class ChangeAbbrevitationToNameOnStonetype < ActiveRecord::Migration
  def up
    rename_column :stonetypes, :abbreviation , :name
  end

  def down
  end
end
