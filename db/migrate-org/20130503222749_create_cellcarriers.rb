class CreateCellcarriers < ActiveRecord::Migration
  def change
    create_table :cellcarriers do |t|
      t.string :name
      t.string :sms

      t.timestamps
    end
  end
end
