class ChangeDobMonthToMonthidOnSeller < ActiveRecord::Migration
  def up
    rename_column :sellers, :dob_month , :month_id
  end

  def down
  end
end
