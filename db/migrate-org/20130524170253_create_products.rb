class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :sort
      t.integer :language_id
      t.integer :productcategory_id
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
