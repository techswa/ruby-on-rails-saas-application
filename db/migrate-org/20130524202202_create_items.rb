class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :license
      t.integer :purchase_id
      t.integer :user_id
      t.float :scrap_percent
      t.integer :payment
      t.integer :gender_id
      t.integer :product_id
      t.integer :metal_id
      t.float :weight
      t.integer :unit_weights
      t.float :size
      t.integer :unit_size
      t.string :description
      t.string :hallmark
      t.string :serialnumber
      t.string :engraving
      t.string :comments

      t.timestamps
    end
  end
end
