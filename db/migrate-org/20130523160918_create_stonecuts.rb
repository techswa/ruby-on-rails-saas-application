class CreateStonecuts < ActiveRecord::Migration
  def change
    create_table :stonecuts do |t|
      t.integer :language_id
      t.string :method
      t.integer :sort
      t.string :name
      t.string :abbreviation
      t.string :description

      t.timestamps
    end
  end
end
