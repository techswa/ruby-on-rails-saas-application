class CreateStonecolors < ActiveRecord::Migration
  def change
    create_table :stonecolors do |t|
      t.integer :language_id
      t.string :method
      t.integer :sort
      t.string :name
      t.string :abbreviation
      t.string :description

      t.timestamps
    end
  end
end
