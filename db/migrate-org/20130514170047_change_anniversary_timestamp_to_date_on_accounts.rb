class ChangeAnniversaryTimestampToDateOnAccounts < ActiveRecord::Migration
  def up
    change_table :accounts do |t|
      t.change :anniversary, :date
    end

  end

  def down
  end
end
