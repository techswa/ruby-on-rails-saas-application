class CreateLanguages < ActiveRecord::Migration
  def change
    create_table :languages do |t|
      t.integer :sort
      t.string :abbreviation
      t.string :description

      t.timestamps
    end
  end
end
