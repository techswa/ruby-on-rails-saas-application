class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.integer :account_id
      t.string :company
      t.integer :user_id
      t.string :phone_office
      t.string :phone_office_ext
      t.string :phone_cell
      t.string :address_office_1
      t.string :address_office_2
      t.string :address_office_city
      t.integer :address_office_state_id
      t.date :anniversary
      t.text :notes

      t.timestamps
    end
  end
end
