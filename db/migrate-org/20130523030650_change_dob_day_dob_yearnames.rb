class ChangeDobDayDobYearnames < ActiveRecord::Migration
  def up
    rename_column :sellers, :dob_day, :day
    rename_column :sellers, :dob_year, :year
  end

  def down
  end
end
