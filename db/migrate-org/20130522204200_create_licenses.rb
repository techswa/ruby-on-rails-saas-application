class CreateLicenses < ActiveRecord::Migration
  def change
    create_table :licenses do |t|
      t.string :abreviation
      t.string :description

      t.timestamps
    end
  end
end
