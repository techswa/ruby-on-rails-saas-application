class AddRemoteLocationToStore < ActiveRecord::Migration
  def change
    add_column :stores, :remotelocation, :boolean
  end
end
