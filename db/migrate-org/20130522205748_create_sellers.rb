class CreateSellers < ActiveRecord::Migration
  def change
    create_table :sellers do |t|
      t.integer :state_id
      t.string :license_id
      t.string :number
      t.string :dob_month
      t.string :dob_day
      t.string :dob_year
      t.string :name_last
      t.string :name_first
      t.string :address_1
      t.string :address_2
      t.string :city
      t.integer :astate_id
      t.string :zip
      t.integer :height_feet
      t.integer :height_inches
      t.integer :gender_id
      t.integer :eyes_id

      t.timestamps
    end
  end
end
