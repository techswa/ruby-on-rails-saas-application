class ChangeI18nToLanguageIdOnPieces < ActiveRecord::Migration
  def up
    rename_column :pieces, :i18n, :language_id

  end

  def down
  end
end
