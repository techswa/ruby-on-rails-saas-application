class ChangeLicenseFieldsOnPurchase < ActiveRecord::Migration
  def up
    rename_column :purchases, :purchase_id, :store_id
  end

  def down
  end
end
