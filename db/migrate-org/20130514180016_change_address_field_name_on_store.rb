class ChangeAddressFieldNameOnStore < ActiveRecord::Migration
  def up
    rename_column :stores, :address_office_1 , :address_1
    rename_column :stores, :address_office_2 , :address_2
    rename_column :stores, :address_office_city , :city
  end

  def down
  end
end
