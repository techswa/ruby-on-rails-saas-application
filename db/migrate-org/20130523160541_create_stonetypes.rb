class CreateStonetypes < ActiveRecord::Migration
  def change
    create_table :stonetypes do |t|
      t.integer :language_id
      t.integer :sort
      t.string :abbreviation
      t.string :description

      t.timestamps
    end
  end
end
