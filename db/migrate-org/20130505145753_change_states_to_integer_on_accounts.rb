class ChangeStatesToIntegerOnAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :address_office_state_id, :integer
    add_column :accounts, :address_mail_state_id, :integer
    remove_column :accounts, :address_office_state
    remove_column :accounts, :address_mail_state
  end

  def down
  end
end
