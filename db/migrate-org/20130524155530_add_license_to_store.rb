class AddLicenseToStore < ActiveRecord::Migration
  def change
    add_column :stores, :license, :string
  end
end
