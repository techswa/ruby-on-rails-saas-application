class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :account_id
      t.integer :store_id
      t.integer :user_id
      t.datetime :date_time
      t.integer :au_spot
      t.integer :au_payout
      t.integer :ag_spot
      t.integer :ag_payout
      t.integer :pt_spot
      t.integer :pt_payout

      t.timestamps
    end
  end
end
