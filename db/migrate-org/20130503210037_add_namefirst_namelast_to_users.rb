class AddNamefirstNamelastToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name_first, :string
    add_column :users, :name_last, :string
    add_column :users, :notes, :text
  end
end
