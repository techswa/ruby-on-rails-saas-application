class ChangeWeightSizeToIdOnItem < ActiveRecord::Migration
  def up
    rename_column :items, :unit_weights, :unit_weight_id
    rename_column :items, :unit_size, :unit_size_id
  end

  def down
  end
end
