class AddFactorToUnitweight < ActiveRecord::Migration
  def change
    add_column :unitweights, :factor, :float
  end
end
