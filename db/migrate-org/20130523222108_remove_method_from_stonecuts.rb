class RemoveMethodFromStonecuts < ActiveRecord::Migration
  def up
    remove_column :stonecuts, :method
  end

  def down
  end
end
