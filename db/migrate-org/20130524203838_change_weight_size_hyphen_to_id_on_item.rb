class ChangeWeightSizeHyphenToIdOnItem < ActiveRecord::Migration
  def up
    rename_column :items, :unit_weight_id, :unitweight_id
    rename_column :items, :unit_size_id, :unitsize_id
  end

  def down
  end
end
