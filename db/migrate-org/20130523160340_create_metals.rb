class CreateMetals < ActiveRecord::Migration
  def change
    create_table :metals do |t|
      t.integer :sort
      t.integer :language_id
      t.string :base
      t.string :purity
      t.string :abbreviation
      t.string :description

      t.timestamps
    end
  end
end
