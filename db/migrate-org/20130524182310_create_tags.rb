class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :number
      t.integer :purchase_id
      t.datetime :release
      t.boolean :processed

      t.timestamps
    end
  end
end
