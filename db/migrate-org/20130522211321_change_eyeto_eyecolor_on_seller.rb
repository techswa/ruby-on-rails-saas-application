class ChangeEyetoEyecolorOnSeller < ActiveRecord::Migration
  def up
    rename_column :sellers, :eyes_id , :eyecolor_id
  end

  def down
  end
end
