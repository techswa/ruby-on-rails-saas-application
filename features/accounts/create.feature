@init
Feature: Create new account
  In order to add a new account
  A user
  Should create a new account by entering Account data

  Scenario: User creates new account
    Given I notify that the "Account:create" test is beginning
    Given I visit the accounts index page
    Then I select create new account
    And I fill out the form
    And I submit the form
    Then I should see the account show page
    And the data should match the factory
    And I should see alert to create user to be the account manager
