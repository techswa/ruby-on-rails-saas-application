@init
Feature: List stores belong to account
  In order to list all stores belonging to an account
  A user
  Should select the account

#  Scenario: User wishes view a list of stores belonging to an account
#    Given I notify that the "Account:list_stores" test is beginning
#    Given there is an account, authorize and store records to view
#    Given I visit the accounts index page
#    And I click the list link in the stores column
#    #Given there is a store to view
#    #Then I should see the show page



  #Create factories
    #Given Lookup table Standings exist
    #Given Lookup table States exist

#    And I click the link "New account"
#    Then the new page should have the h tag "New account"
    #Then I should be on the "New account" page

  #This replaces the form filling below
#    And I fill in the form with new account information

#    And I fill in the text field labeled "* Account #" with "012345"
#    And I fill in the text field labeled "* Name" with "Bills Gold World"
#
#    And I select "Good" from the "account_standing_id" select control
#    And I fill in the text field labeled "* First name" with "Rusty"
#    And I fill in the text field labeled "* Last name" with "Smith"
#    And I fill in the text field labeled "* Title" with "Owner"
#    And I fill in the text field labeled "* email" with "rusty@BillsGoldWorld.com"
#    And I fill in the text field labeled "* Confirm email" with "rusty@BillsGoldWorld.com"
#    And I fill in the text field labeled "* Office Phone" with "214-896-5421"
#    And I fill in the text field labeled "Ext" with "125"
#    And I fill in the text field labeled "* Cell phone" with "972-658-9652"
#    #Mail address
#    And I fill in the text field named "account_address_office_1" with "P.O. Box 182736"
#    And I fill in the text field labeled "account_address_office_2" with ""
#    And I fill in the text field labeled "account_address_office_city" with "Addison"
#    And I select "Texas" from the "account_address_office_state_id" select control
#    And I fill in the text field labeled "account_address_office_zipcode" with "75001"
#    #Shipping address
#    And I fill in the text field named "account_address_mail_1" with "3658 Beltline Rd"
#    And I fill in the text field labeled "account_address_mail_2" with "Ste 321"
#    And I fill in the text field labeled "account_address_mail_city" with "Addison"
#    And I select "Texas" from the "account_address_mail_state_id" select control
#    And I fill in the text field labeled "account_address_mail_zipcode" with "75001"
  # Save the record
#    And I click the button labeled "Create Account"
#    Then the new page should have the h tag "Account details"




#    When I sign in with valid credentials
#    Then I see an invalid login message
#    And I should be signed out
#
#  Scenario: User signs in successfully
#    Given I exist as a user
#    And I am not logged in
#    When I sign in with valid credentials
#    Then I see a successful sign in message
#    When I return to the site
#    Then I should be signed in
#
#  Scenario: User enters wrong email
#    Given I exist as a user
#    And I am not logged in
#    When I sign in with a wrong email
#    Then I see an invalid login message
#    And I should be signed out
#
#  Scenario: User enters wrong password
#    Given I exist as a user
#    And I am not logged in
#    When I sign in with a wrong password
#    Then I see an invalid login message
#    And I should be signed out