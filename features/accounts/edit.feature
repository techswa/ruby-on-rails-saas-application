@init
Feature: Edit existing account
  In order to edit an existing account
  A user
  Should select the account and edit it

  Scenario: User edits an account
    Given I notify that the "Account:edit" test is beginning
    Given an account exists
    And I visit the accounts index page
    Then I select the first account
    Then I should see the account show page
    Then I click on edit
    And I set the field with the label "name" to "Better Gold Company"
    And I set the field with the label "city" to "Plano"
    Then I submit the form
    Then the page should contain "Better Gold Company"
    Then the page should contain "Plano"
    #Then Open the page

