module AccountHelpers

  #def change_city new_city
  #  new_city = 'dddsd'
  #  set_field_by_label('account.city', new_city)
  #end

  def visit_accounts_index_page
    visit '/accounts'
    page.should have_content I18n.t('account.title_index')
  end

  def select_first_row_account
    #puts 'select_first_row_account'
    #click_link('123456')
    #find("//a[@rel='show-account']").click
    save_and_open_page
  end

  def select_first_row_store_list
    puts "Account count: #{Account.all.count}"
    @accounts = Account.all
    puts @accounts[0].inspect
    @standings = Standing.all
    puts @standings[0].inspect
    puts "Store count: #{Store.all.count}"
    @stores = Store.all
    @stores[0].state_id = @accounts[0].state_id
    puts @stores[0].inspect
    puts State.all[0].inspect
    #create_authorize_factory
    #@authorizes = Authorize.all
    #puts @authorizes[0].inspect


    @link = "/stores/list?search=#{@accounts[0].id}"
    puts @link
    visit "/stores/#{@stores[0].id}" #@link

     #within '.table' do
     #   puts 'Found table'
     #   find("a[@rel='list-stores']").click
     #   #click_link('123456')
     #end
      #click_link('List')

    save_and_open_page
    #if find("//a[@rel='list_stores']")
    #  puts 'Found rel'
    #end
  end

  def fill_in_new_account
    #Create dependant FK factories
    create_standing_factory
    create_state_factory
    page.click_link I18n.t('form.create')
    #save_and_open_page
    page.should have_content I18n.t('account.title_new')
  end

  def create_store_for_account
    create_authorize_factory
    @accounts = Account.all
    @account = @accounts.first
    create_store_factory @account.id
  end

  def fill_account_form

    #Create account record to use
    @account = build_account_factory
    puts @account.inspect
    #Reference fields by I18n and fill with factory data
    puts 'Filling in new account form'

    puts "Filling & testing #{I18n.t('account.section_legend_account')} section."
    #Enter data into fields
    set_field_by_label('account', 'name', @account.name)
    #save_and_open_page
    set_field_by_label('account', 'number', @account.number)
    set_field_by_label('account', 'code', @account.code)
    #Skip anniversary - use default
    #Open Standing select
    set_select_option_value('Good','account_standing_id')

    puts "Filling #{I18n.t('account.section_legend_contact')} section."

    set_field_by_label('account', 'first_name', @account.first_name)
    set_field_by_label('account', 'last_name', @account.last_name)
    set_field_by_label('account', 'contact_title', @account.title)
    set_field_by_label('account', 'email', @account.email)
    set_field_by_label('account', 'email_confirmation', @account.email_confirmation)
    set_field_by_label('account', 'phone_office', @account.phone_office)
    set_field_by_label('account', 'phone_office_ext', @account.phone_office_ext)
    set_field_by_label('account', 'phone_cell', @account.phone_cell)

    puts "Filling #{I18n.t('account.section_legend_address')} section."

    within id_from_I18n I18n.t('account.section_legend_address') do
      set_field_by_label('account', 'address_1', @account.address_1)
      set_field_by_label('account', 'address_2', @account.address_2)
      set_field_by_label('account', 'city', @account.city)
      set_select_option_value('Texas','account_state_id')
      set_field_by_label('account', 'zipcode', @account.zipcode)
    end

  end

end

World(AccountHelpers)