module FieldHelpers

  def verify_field_by_label (model,field_label, contents)
    translated_field_label = model + '.' + field_label
    page.should have_content I18n.t(filed_label)
  end

  # Fill in text fields - cjhecks for required * marker
  def set_field_by_label(model,field_label, data)
    field_label = model + '.' + field_label
    field_optional = I18n.t(field_label)
    field_required = "* #{I18n.t(field_label)}"

    puts "Field label: #{field_label}"
    puts "Field optional: #{field_optional}"
    puts "Field required: #{field_required}"

    #Is it a required field
    if page.should have_field(field_optional)
      fill_in "#{I18n.t(field_label)}", with: data
    elsif page.should have_field(field_required)
      fill_in "* #{I18n.t(field_label)}", with: data
    end
  end

  #Select option
  def set_select_option_value(selection, option_list)
    select selection, from: option_list
  end

  #Replace spaces with hyphens and lower case
  def id_from_I18n(section_id)
      result = section_id.replace(section_id.gsub(' ', '-'))
      result = result.downcase
      "##{result}"
  end

end

World(FieldHelpers)