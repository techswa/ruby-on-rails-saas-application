module FactoryHelpers

  def create_state_factory
    puts 'Creating state factory'
    FactoryGirl.create(:state, name: 'Alabama', abbreviation: 'AL').should be_valid
    FactoryGirl.create(:state, name: 'Texas', abbreviation: 'TX').should be_valid
  end

  def create_standing_factory
    puts 'Creating standing factory'
    FactoryGirl.create(:standing, name: 'Bad').should be_valid
    FactoryGirl.create(:standing, name: 'Good').should be_valid
  end

  def build_account_factory
    puts 'Building account factory'
    FactoryGirl.build(:account, id: 1)
  end

  def create_account_factory
    puts 'Creating account factory'
    FactoryGirl.create(:account)
  end

  def create_store_factory
    puts 'Creating store factory'
    FactoryGirl.create(:store)
    puts :store.inspect
  end

  def create_authorize_factory
    puts 'Creating authorize factory'
    #Clean date
    @a = Authorize.all
    puts "@a count: #{@a.count}"
    FactoryGirl.create(:authorize)

  end

  def create_list_stores_factory
    puts 'Creating factories for list-stores feature'
    #FactoryGirl.create(:authorize)
    FactoryGirl.create(:store)

    #FactoryGirl.create(:user)

    #@stores = Store.all
    #puts @stores[0].inspect
    #@accounts = Account.all
    #puts @accounts[0].inspect
    #@states = State.all
    #puts @states[0].inspect
    #@locations = Location.all
    #puts @locations[0].inspect
    #@authorizes = Authorize.all
    #puts @authorizes[0].inspect
    #@users = User.all
    #puts @users[0].inspect
  end

  #Verify store factory will create all needed FK's

end

World(FactoryHelpers)