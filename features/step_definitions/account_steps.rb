#Test start notification
Given(/^I notify that the "(.*?)" test is beginning$/) do |test_name|
  puts "**** Beginning the #{test_name} test"
end

Then(/^the page should contain "(.*?)"$/) do |content|
  page.should have_content content
end

Then(/^I click the list link in the stores column$/) do
  select_first_row_store_list
end


#Visit the accounts index page
Given(/^I visit the accounts index page$/) do
  visit_accounts_index_page
end

# Fill in fields on form
Then(/^I select create new account$/) do
  fill_in_new_account
end


Then(/^I select the first account$/) do
  #select_table_first_row
  within '.table' do
    puts 'Found table'
    @accounts = Account.all
    click_link(@accounts.first.number)
  end
  #puts 'Looking for account link'
  #save_and_open_page
end

Then(/^I fill out the form$/) do
  fill_account_form
end


Then(/^I click on edit$/) do
  click_link(I18n.t('form.edit'))
  #save_and_open_page
end

Then(/^I submit the form$/) do
  click_button I18n.t('form.submit')
end

Then(/^I should see the account show page$/) do
  #create_store_for_account
  page.should have_content I18n.t('account.title_show')
end

Then(/^the data should match the factory$/) do
  page.should have_content @account.name
  page.should have_content @account.city
  page.should have_content @account.address_1
end

Then(/^I set the field with the label "(.*?)" to "(.*?)"$/) do |field_label, contents|
  set_field_by_label('account',field_label, contents)
end

Then(/^the field with the label "(.*?)" should contain "(.*?)"$/) do |field_label, contents|
  verify_field_by_label('account',field_label, contents)
end

Then(/^I confirm the email address$/) do
  set_field_by_label('account', 'account.email_confirmation', 'bobsmith@mygoldbuyer.com')
end

Then(/^Open the page$/) do
  save_and_open_page
end

### Factories

# Create account via factory
Given(/^an account exists$/) do
  create_state_factory
  create_account_factory
end

Given(/^there is an account, authorize and store records to view$/) do
  create_list_stores_factory
end


#Click and follow a link
#Given(/^I click the link "(.*?)"$/) do |link_text|
#  page.click_link link_text
#end
#
##Click button
#And(/^I click the button labeled "(.*?)"$/) do |label_text|
#  click_button label_text
#end

