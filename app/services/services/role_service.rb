module Services
# Manages roles for application
  class RoleService
    #Restricts users ability to ceate users with roles that
    # are more powerful than allowed via params manipulation
    # meaning that a userwith an :accounting role can only create :primary, :manager & :buyer users
    # meaning that a user with a :primary role can only create :manager & :buyer users

    def initialize(current_user, params)
      #When displaying the record, the params are nil
      puts 'Initializing role_service...'
      @current_user = current_user
      @params_original = params.dup
      puts 'params orig: ' + @params_original.to_s
      @role_names = %w(admin accounting support leo primary manager buyer unassigned)
      @role_symbols = [:admin, :accounting, :support, :leo, :primary, :manager, :buyer, :unassigned]
      @user_id = nil
      @users = nil
      @account_id = nil
      @params_stripped = nil
      @roles = nil
      @selected_role_id = nil
      @selected_role_name = nil
      puts 'exiting init...'
    end


    def get_new_role_name
      puts 'Entering role_filter#get_new_role_name'
      self.get_selected_role_id
      # Get role_id from params
      puts '@roles_names ' + @role_names.inspect
      puts '@selected_role_id ' + @selected_role_id.to_s
      _return = @role_names[(@selected_role_id.to_i - 1)]
      # Subtract one to tie zero indexed select list with 1 index database id
      puts 'Exiting role_filter#get_new_role_name'
      _return
    end

    # Return the role id from a symbol
    # used to set default role for new record
    def get_role_id_from_symbol(role_symbol)
      _index = @role_symbols.index(role_symbol)
      _return = _index + 1 #Offset the index
      _return
    end

    def system_role?(current_user)
      _return = true if current_user.has_role? :admin
      _return = true if current_user.has_role? :accounting
      _return = true if current_user.has_role? :support
      _return
    end

    def get_user_id
      @user_id = params[:user][:role_id]
    end

    def get_current_role_index(user)
      # Get users current role if any and returns index for form creation
      puts 'Entering role_filter.get_current_role_index...'
      _return = nil
      _return = 1 if user.has_role? :admin
      _return = 2 if user.has_role? :accounting
      _return = 3 if user.has_role? :support
      _return = 4 if user.has_role? :leo
      _return = 5 if user.has_role? :primary
      _return = 6 if user.has_role? :manager
      _return = 7 if user.has_role? :buyer
      _return = 8 if user.has_role? :unassigned
      if _return.nil? then _return = 8 end
      puts 'Exiting role_service.get_current_role_index with: ' + _return.to_s
      _return.to_s #Return value
    end

    def display_primary_role?
      true
    end

    def get_params_stripped
      puts '@role_service#get_params_stripped ' + @params_original[:user].to_s
      @params_stripped = @params_original[:user].delete :role_id
      puts 'Exiting with ' + @params_stripped.to_s
    end

    def get_account_id
      @account_id = @params_original[:account_id].to_i
    end

    def get_selected_role_id
      @selected_role_id = @params_original[:user][:role_id]
    end

    def clear_all_roles(user)

      user.remove_role :accounting
      user.remove_role :support
      user.remove_role :leo
      user.remove_role :primary
      user.remove_role :manager
      user.remove_role :buyer
      user.remove_role :unassigned

    end

    # Clear existing role & set newly selected one
    def set_user_role(user)
      puts 'Entered User#add_user_role with ' + user.inspect
      self.clear_all_roles(user)

      #Just get one role
      _new_role_name = self.get_new_role_name
      puts 'role to add ' + _new_role_name
      user.add_role _new_role_name.to_sym
    end


    def build_allowable_roles
      # was allowable roles
      # Returns active record object containing
      # roles supported by user's role
      # uses class /named scopes
      puts 'Entering build_allowable_roles...'

      if @current_user.has_role? :admin
        _roles = Role.admin
      elsif @current_user.has_role? :accounting
        if self.display_primary_role?
          _roles = Role.accounting_with_primary
        else
          _roles = Role.accounting_without_primary
        end
      elsif @current_user.has_role? :support
        _roles = Role.support
      elsif @current_user.has_role? :leo
        _roles = Role.leo
      elsif @current_user.has_role? :primary
        _roles = Role.primary
      elsif @current_user.has_role? :manager
        _roles = Role.manager
      elsif @current_user.has_role? :buyer
        _roles = Role.buyer
      elsif @current_user.has_role? :unassigned
        _roles = Role.unassigned
      else
        _roles = nil
      end
      puts 'Exiting build_allowable_roles...'
      _roles
    end

    def get_parameters
      #Return usable parameters for active record
      @params_stripped
    end

    #
    #def get_original_parameters
    #  @params_original
    #end

    def power_role_selected?
      #Has an allowable role been passed back based upon current_user's role
      puts 'Entered allowable role selected'
      _selected_role_id = @params_original[:user][:role_id]
      # Get selected role from params
      _return = nil

      if @current_user.has_role? :admin
        if %w(2 3).include?(_selected_role_id)
          _return = true
        end

      elsif @current_user.has_role? :accounting
        if %w(4 5 6 7 8).include?(_selected_role_id)
          _return = true
        end

      elsif @current_user.has_role? :primary
        if %w(6 7 8).include?(_selected_role_id)
          _return = true
        end

      elsif @current_user.has_role? :manager
        if %w(7 8).include?(_selected_role_id)
          _return = true
        end
      else
        _return = false
      end
      puts 'Exiting allowable role selected with ' + _return.to_s
      _return
    end

  end

end