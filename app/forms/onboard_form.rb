class OnboardForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations


#Use delegation to address models have fields with the same name
#Delegate to account
  delegate :id, :number1, :name1, :first_name, :last_name, :title, :email, :phone_office,
           :phone_office_ext, :phone_cell, :address_1, :address_2, :city,
           :state_id, :zipcode, :anniversary, :standing_id,
           :code, to: :account, prefix: true

  attr_accessor :id, :number, :name, :first_name, :last_name, :title, :email, :phone_office,
                  :phone_office_ext, :phone_cell, :address_1, :address_2, :city,
                  :state_id, :zipcode, :anniversary, :standing_id, :code

#
###Delegate to Store
#delegate :account_id, :address_1, :address_2, :city, :state_id, :zipcode,
#         :anniversary, :name, :license, :phone_cell, :phone_office,
#         :phone_office_ext, :location_id, :standing_id, :key, to: :store, prefix: true

  def persisted?
    false
  end

  #def model_name
  #  self.model_name
  #end

  def self.model_name
    ActiveModel::Name.new(self, nil, 'Onboard')
  end

  def to_hash
    hash = {}
    instance_variables.each {|var| hash[var.to_s.delete("@")] = instance_variable_get(var) }
    hash
  end

  def account
    @account ||= Account.new
  end

  def initialize(params = {})
    puts 'Initializing class'
    puts 'initialized params: ' + params.inspect
    @id = params.fetch(:id, '')
    @number = params.fetch(:number, '')
    @name = params.fetch(:name, '')
    #@first_name = params.fetch(:first_name, '')
    #@last_name = params.fetch(:last_name, '')
    #@title = params.fetch(:title, '')
    #@email = params.fetch(:last_name, '')
    #@phone_office = params.fetch(:phone_office, '')
    #@phone_office_ext = params.fetch(:phone_office_ext, '')
    #@address_1 = params.fetch(:address_1, '')
    #@address_2 = params.fetch(:address_2, '')
    #@city = params.fetch(:city, '')
    #@state_id = params.fetch(:state_id, '')
    #@zipcode = params.fetch(:zipcode, '')
    #@anniversary = params.fetch(:anniversary, '')
    #@code = params.fetch(:code, '')

    puts 'initialized value' + account.inspect

  end
  #def store
  #  puts 'store called'
  #  @store ||= Store.new
  #end

  def id
    @id = account.id
  end

  #Validations are in the respective models

  def save

    account.number = @number
    account.name = @name
    #account.first_name = @first_name
    #account.last_name = @last_name
    #account.title = @title
    #account.email = @email
    #account.phone_office = @phone_office
    #account.phone_office_ext = @phone_office_ext
    #account.address_1 = @address_1
    #account.address_2 = @address_2
    #account.city = @city
    #account.state = @state_id
    #account.zipcode = @zipcode
    #account.anniversary = @anniversary
    #account.code = @code




   # store.attributes = params.slice(:account_id, :address_1, :address_2, :city, :state_id, :zipcode,
   #                                 :anniversary, :name, :license, :phone_cell, :phone_office,
   #                                 :phone_office_ext, :location_id, :standing_id, :key)


    if valid?
      account.save!
      #get_account_id
      #store.save!
      puts 'Account has been saved'
      puts 'Saved record: ' + account.inspect
      #Set object id to saved record id
      self.id = account.id
      true
    else
      false
    end
  end

  def get_account_id
    @account_saved = Account.find_by_number @account.number
    #Link the records
    @store.account_id = @account_saved.id
  end


end