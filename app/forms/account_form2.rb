class AccountForm2
  extend ActiveModel::Naming
  extend ActiveModel::Translation
  include ActiveModel::Conversion
  include ActiveModel::Validations

  def persisted?
    false
  end

  def initialize new_record
    @record = new_record
  end

  attr_accessor :number, :name, :first_name, :last_name, :title, :email, :phone_office,
                :phone_office_ext, :phone_cell, :address_1, :address_2, :city,
                :state_id, :zipcode, :anniversary, :code

  attr_accessor :email_confirmation

  #before_validation :phone_office_clean


  validates :number,
            #uniqueness: true,
            presence: true,
            numericality: true, length: { minimum: 6, maximum: 7 }

  validates :name,
            #uniqueness: true,
            presence: true

  validates :email,
            presence: true,
            confirmation: true,
            format: {:with => /@/}

  # Run manually
  validates :email_confirmation,
            presence: true, if: :email_changed?

  validates :first_name,
            presence: true

  validates :last_name,
            presence: true

  validates :title,
            presence: true

  validates :code,
            presence: true


  validates_format_of :email, :with => /@/

  validates_presence_of :phone_office
  validate :phone_office_clean, message: 'Bad phone number' #removes non numbers
  validates_format_of :phone_office, with: /\A[0-9]+\z/
  validates_presence_of :address_1
  validates_presence_of :city
  validates_presence_of :zipcode
  validates_format_of :zipcode, with: /\A[0-9]+\z/
  validates_presence_of :anniversary
   

  #Validations are in the respective models

  def submit(params)
    @params = params

    #Manually assign data to fields
    set_params @params

    #replaces ActiveRecord before_validation
    manual_before_validation

    if valid?
      puts 'Valid'
      @record.save!
      true
    else
      false
      puts 'Invalid'
    end
  end

  def set_params(params)
    @record.number = params[:number]
    @record.name = params[:name]
    @record.first_name = params[:first_name]
    @record.last_name = params[:last_name]
    @record.title = params[:title]
    @record.email = params[:email]
    @record.phone_office = params[:phone_office]
    @record.phone_office_ext = params[:phone_office_ext]
    @record.address_1 = params[:address_1]
    @record.address_2 = params[:address_2]
    @record.city = params[:city]
    @record.state_id = params[:state_id]
    @record.zipcode = params[:zipcode]
    @record.anniversary = params[:anniversary]
    puts "Anniversary params #{params[:anniversary]}"
    puts "Anniversary self #{@record.anniversary}"
    @record.code = params[:code]
  end

  def account_state
    self.state.abbreviation
  end

private

  def manual_before_validation
    #Call any translations
    phone_office_clean
  end

  def email_changed?
    false
  end
  # Clean phone numbers before saving
  def phone_office_clean
    self.phone_office = self.phone_office.to_s.gsub(/[^0-9]/, '')
  end

  def validate_anniversary?
    if !anniversary.is_a?(Date)
      #errors.add(:anniversary, 'must be a valid date')
      false
    else
      true
    end
  end


end