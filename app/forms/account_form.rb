class AccountForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  delegate :number, :name, :first_name, :last_name, :title, :email, :phone_office,
           :phone_office_ext, :phone_cell, :address_1, :address_2, :city,
           :state_id, :zipcode, :anniversary, :standing_id,
           :code, to: :account

  attr_accessor :email_confirmation

  #before_validation :phone_office_clean


  validates :number,
            #uniqueness: true,
            presence: true,
            numericality: true, length: { minimum: 6, maximum: 7 }

  validates :name,
            #uniqueness: true,
            presence: true

  validates :email,
            presence: true,
            confirmation: true,
            format: {:with => /@/}

  # Run manually
  validates :email_confirmation,
            presence: true, if: :email_changed?

  validates :first_name,
            presence: true

  validates :last_name,
            presence: true

  validates :title,
            presence: true

  validates :code,
            presence: true


  validates_format_of :email, :with => /@/

  validates_presence_of :phone_office
  validate :phone_office_clean, message: 'Bad phone number' #removes non numbers
  validates_format_of :phone_office, with: /\A[0-9]+\z/
  validates_presence_of :address_1
  validates_presence_of :city
  validates_presence_of :zipcode
  validates_format_of :zipcode, with: /\A[0-9]+\z/
  validates_presence_of :anniversary


  def initialize

  end

  def persisted?
    #false
    account.id
  end

  def id
    account.id
  end

  def self.model_name
    ActiveModel::Name.new(self, nil, 'Account')
  end

  def account
    account ||= Account.new
  end

  def find(account_id)
    account = Account.find account_id
    self.edit account
  end


  def edit(record)
    @record = record
    #set persisted
   # :id = @record.id
    puts "RECORD: #{@record.inspect}"
    #load variables for form
    account.id = @record.id
    account.number = @record.number
    account.name = @record.name
    account.first_name = @record.first_name
    account.last_name = @record.last_name
    account.title = @record.title
    account.email = @record.email
    #account.email_confirmation = @record.email
    account.phone_office = @record.phone_office
    account.phone_office_ext = @record.phone_office_ext
    account.phone_cell = @record.phone_cell
    account.address_1 = @record.address_1
    account.address_2 = @record.address_2
    account.city = @record.city
    account.state_id = @record.state_id
    account.zipcode = @record.zipcode
    account.anniversary = @record.anniversary
    account.code = @record.code
  end

  def update(params)
    @params = params
    puts "Form updating with params #{params}"
    #Manually assign data to fields
    set_params @params
    #replaces ActiveRecord before_validation
    manual_before_validation

    if valid?
      account.save
    else
      false
    end
  end


  def submit(params)
    @params = params
    puts "Form save params #{params}"

    #Manually assign data to fields
    set_params @params

    #replaces ActiveRecord before_validation
    manual_before_validation

    if valid?
      account.save!
      true
    else
      false
      puts 'Invalid'
    end
  end

  def set_params(params)
    puts "set_params"
    account.number = params[:number]
    account.name = params[:name]
    account.first_name = params[:first_name]
    account.last_name = params[:last_name]
    account.title = params[:title]
    account.email = params[:email]
    account.phone_office = params[:phone_office]
    account.phone_office_ext = params[:phone_office_ext]
    account.address_1 = params[:address_1]
    account.address_2 = params[:address_2]
    account.city = params[:city]
    account.state_id = params[:state_id]
    account.zipcode = params[:zipcode]
    account.anniversary = params[:anniversary]
    #puts "Anniversary params #{params[:anniversary]}"
    #puts "Anniversary self #{account.anniversary}"
    account.code = params[:code]
  end

  def account_state
    self.state.abbreviation
  end

private

  def manual_before_validation
    #Call any translations
    phone_office_clean
  end

  def email_changed?
    false
  end
  # Clean phone numbers before saving
  def phone_office_clean
    account.phone_office = account.phone_office.to_s.gsub(/[^0-9]/, '')
  end

  def validate_anniversary?
    if !anniversary.is_a?(Date)
      #errors.add(:anniversary, 'must be a valid date')
      false
    else
      true
    end
  end


end