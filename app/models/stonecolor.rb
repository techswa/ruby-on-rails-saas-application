class Stonecolor < ActiveRecord::Base
  attr_accessible :ratingsystem_id, :abbreviation, :description, :language_id, :method, :name, :sort

  belongs_to :language
  belongs_to :ratingsystem

end
