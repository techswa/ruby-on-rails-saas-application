class Metal < ActiveRecord::Base
  attr_accessible :abbreviation, :basemetal_id, :description, :factor, :language_id, :name, :purity, :sort

  belongs_to :basemetal
  belongs_to :language

  validates :name, presence: true, length: {minimum: 1, maximum: 15}
  validates :abbreviation, presence: true, length: { minimum: 2, maximum:4 }
  validates :description, length: {minimum: 0, maximum: 255}
  validates :factor, presence: true, numericality: true
  validates :purity, presence:true, length: { maximum:4 }
  validates :sort, presence:true, numericality: true


  scope :sorted, order('basemetal_id ASC', 'sort ASC','purity ASC')


  def listbox_method
    abbreviation + ' : ' + description

  end

end
