class Cellcarrier < ActiveRecord::Base
  attr_accessible :name, :sms

  validates :name,presence: true
  validates :sms, presence: true

  validates_format_of :sms, with: /[0-9]/
  validates_length_of :sms, minimum: 10 , maximum: 10

  has_many :users

end
