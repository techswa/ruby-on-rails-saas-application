class Role < ActiveRecord::Base
  has_and_belongs_to_many :users, :join_table => :users_roles
  belongs_to :resource, :polymorphic => true
  
  scopify

  def self.admin
    where(name: [%w(accounting support unassigned)])
  end

  def self.accounting_with_primary
    where(name: [%w(manager buyer primary leo unassigned)])
  end

  def self.accounting_without_primary
    where(name: [%w(manager buyer leo unassigned)])
  end

  def self.support
    where(name: [%w(manager buyer unassigned)])
  end

  def self.leo
    where(name: [nil])
  end

  def self.primary
    where(name: [%w(manager buyer unassigned)])
  end

  def self.manager
    where(name: [%w(buyer unassigned)])
  end
  def self.buyer
    where(name: [nil])
  end

  def self.unassigned
    where(name: [nil])
  end

  # attr_accessible :title, :body
end
