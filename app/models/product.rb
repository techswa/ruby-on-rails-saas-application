class Product < ActiveRecord::Base
  attr_accessible :description, :language_id, :name, :productcategory_id, :sort

  validates_presence_of :name, :productcategory_id

  belongs_to :language
  belongs_to :productcategory
end
