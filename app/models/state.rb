class State < ActiveRecord::Base

  #Connect as accounts lookup table
  #has_many :accounts
  has_many :office_states, :class_name => "Account", :foreign_key => "address_mail_state_id"
  has_many :mail_states, :class_name => "Account", :foreign_key => "address_mail_state_id"

  has_many :stores
  has_many :accounts

  #Cnnect as seller lookup table
  has_many :state, :class_name => "Seller", :foreign_key => :state_id
  has_many :astate, :class_name => "Seller", :foreign_key => :astate_id

  def listbox_options
    abbreviation + ' ' + name
  end

end