class Language < ActiveRecord::Base
  attr_accessible :abbreviation, :description, :sort

  has_many :metals
  has_many :productcategories
  has_many :products
  has_many :stonecolors
  has_many :stonecuts
  has_many :stonetypes
  has_many :stonesizes

  def language_title
    description
  end

end
