class Location < ActiveRecord::Base
  attr_accessible :name, :sort

  has_many :stores
end
