class Quote < ActiveRecord::Base
  attr_accessible :account_id, :ag_payout, :ag_spot, :au_payout, :au_spot, :pt_payout, :pt_spot, :user_ref

  belongs_to :account

  validates :ag_spot, numericality: true, presence: true
  validates :ag_payout, numericality: true, presence: true
  validates :au_spot, numericality: true, presence: true
  validates :au_payout, numericality: true, presence: true
  validates :pt_spot, numericality: true, presence: true
  validates :pt_payout, numericality: true, presence: true
  validates :user_ref, presence: true
  #Related fields

  def account_name
    self.account.name
  end

  def account_number
    self.account_number
  end

end
