class Stonesize < ActiveRecord::Base
  attr_accessible :abbreviation, :description, :language_id, :sort

  belongs_to :language
end
