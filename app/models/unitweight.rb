class Unitweight < ActiveRecord::Base
  attr_accessible :abbreviation, :description, :sort, :factor
end
