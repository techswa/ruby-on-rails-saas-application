class Account < ActiveRecord::Base

  include ActionController::UrlFor
  #include Rails.application.routes.url_helpers

  extend FriendlyId
  friendly_id :create_uuid, use: :slugged

  has_many :users
  has_many :stores
  has_many :quotes
  has_many :comments, as: :commentable

  belongs_to :standing
  belongs_to :state


  # Setup accessible (or protected) attributes for your model
  attr_accessible :number, :slug, :name, :first_name, :last_name, :title, :email, :phone_office,
                  :phone_office_ext, :phone_cell, :address_1, :address_2, :city,
                  :state_id, :zipcode, :anniversary, :code, :standing_id

  attr_reader :key


  before_validation :phone_cell_clean
  before_validation :phone_office_clean
                       #Account
  #
  #def to_param
  #  #Change degault search from :id field
  #  guid
  #end


  #Email
  #validates :email, confirmation: true
  #validates_format_of :email, :with => /@/

  #validates :email_confirmation, presence: true, if: :email_changed?

  #
  validates :number, uniqueness: true, presence: true, numericality: true
  #validates :number, format: :with /\A[0-9]+\z/
  #validates_presence_of :number
  #validates_uniqueness_of :number

  validates_length_of :number, { minimum: 6, maximum: 7 }
  #validates_format_of :number, with: /\A[0-9]+\z/

  #Status is derived from status table and select
  #Name of Company
  validates_presence_of :name
  #Contact first name
  validates_presence_of :first_name
  #Contact last name
  validates_presence_of :last_name
  #Contact title
  validates_presence_of :title
  validates_presence_of :code


  validates_presence_of :phone_office
  validate :phone_office_clean, message: 'Bad phone number' #removes non numbers
  validates_format_of :phone_office, with: /\A[0-9]+\z/
  #validates_presence_of :phone_cell
  validate :phone_cell_clean, message: 'Bad phone number', allow_blank:  true, allow_nil: true #removes non numbers

  validates_format_of :phone_cell, with: /\A[0-9]+\z/, allow_blank:  true, allow_nil: true
  validates_presence_of :address_1
  validates_presence_of :city
  validates_presence_of :zipcode
  validates_format_of :zipcode, with: /\A[0-9]+\z/
  validates_presence_of :anniversary


  def create_uuid
    _return = SecureRandom.uuid
    _return
  end

  def account_slug
    self.slug
  end

  #Format full name
  def contact_full_name
    display_string =
        self.last_name +
            ', ' +
            self.first_name

    #Return
    display_string.html_safe
  end

  def account_state
    self.state.abbreviation
  end

  def standing_name
    self.standing.name
  end

  def contact_title
    self.title
  end

  def phone_office_formatted
    display_string =
        self.phone_office = hyphen_phone_number self.phone_office
    if self.phone_office_ext.present?
      '<br />' +
          'Ext: ' +
          self.phone_office_ext
    end
    #Return
    display_string.html_safe
  end

  def phone_cell_formatted
    display_string =
        self.phone_cell = hyphen_phone_number self.phone_cell
    if self.phone_office_ext.present?
      '<br />' +
          'Ext: ' +
          self.phone_office_ext
    end
    #Return
    display_string.html_safe
  end

  def account_address
    display_string =
        self.address_1 +
            '<br />' +
            #Check for blank line
            blank_line_check(self.address_2) +
            self.city + ', ' +
            self.account_state +
            ' ' +
            self.zipcode
    #Return
    display_string.html_safe
  end

  def test_link
    #@title = 'sssddd'
    Rails.application.routes.url_helpers.accounts_path()
    #@text = h.t('account.list_stores')
   # @path = account_stores_path(@account, key: @account.key)
    # 't("account.list_stores"), account_stores_path(@account, key: @account.key)'
  end
  private

  def hyphen_phone_number phone
    if phone.present?
      phone = phone.insert -5,'-'
      phone = phone.insert -9,'-'
    else
      phone = ''
    end
  end

  def blank_line_check line
    #Suppress line if blank
    if line.present?
      line + '<br />'
    else
      ''
    end
  end

  # Clean phone numbers before saving
  def phone_office_clean
    self.phone_office = self.phone_office.to_s.gsub(/[^0-9]/, '')
  end
  # Remove non numerical characters from string
  def phone_cell_clean
    if self.phone_cell?
      self.phone_cell = self.phone_cell.to_s.gsub(/[^0-9]/, '')
    end
  end

  def validate_anniversary?
    if !anniversary.is_a?(Date)
      #errors.add(:anniversary, 'must be a valid date')
      false
    else
      true
    end
  end


end