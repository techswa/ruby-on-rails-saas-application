class Item < ActiveRecord::Base
  attr_accessible :comments, :description, :engraving, :gender_id, :hallmark, :license, :metal_id, :payment, :product_id, :purchase_id, :scrap_percent, :serialnumber, :size, :unitsize_id, :unitweight_id, :user_id, :weight

  belongs_to :user
  belongs_to :purchase
  belongs_to :gender
  belongs_to :metal
  belongs_to :product
  belongs_to :unitsize
  belongs_to :unitweight

end
