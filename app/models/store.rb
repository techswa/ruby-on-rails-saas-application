class Store < ActiveRecord::Base
  attr_accessible :account_id, :address_1, :address_2, :city, :state_id, :zipcode, :anniversary, :name, :license,
                  :phone_office, :phone_office_ext, :location_id, :standing_id, :printkey

  attr_reader :key

  #todo-bb Remove :phone_cell from Store

  belongs_to :account
  belongs_to :state
  belongs_to :location
  belongs_to :standing

  has_many :authorizes

  has_many :comments, as: :commentable

  validates :license, presence: :true,
                      uniqueness: :false, #Change back to true
                      length: {minimum: 6, maximum: 6},
                      format: {with: /\A[0-9]+\z/, message: 'only allows Numbers'}


  validates :name,  presence: :true

  #validates :key, presence: :true,
  #                uniqueness: :true

  validates :phone_office,  presence: :true,
                            uniqueness: :true

  validates :address_1, presence: :true

  validates :city, presence: :true

  #Address zipcode
  validates_presence_of :zipcode
  validates_format_of :zipcode, with: /\A[0-9]+\z/

  validates :anniversary, presence: :true

  #Phone Office
  validates :phone_office,  presence: :true
  validate :phone_office_clean, message: 'Bad phone number' #removes non numbers
  validates_format_of :phone_office, with: /\A[0-9]+\z/

  #Scopes
  #Return stores for a given account
  def list account_id
    #account_id = 1
    where(:account_id => account_id)
  end

  #Associated record fields

  #Get acount name
  def account_name
    self.account.name
  end

  def account_number
    self.account.number
  end

  def account_city
    self.account.city
  end

  def location_name
    self.location.name + ' '
  end

  #Account standing
  def standing_name
    self.account.standing_name
  end

  def address
    display_string =
        self.address_1 +
            '<br />' +
            #Check for blank line
            blank_line_check(self.address_2) +
            self.city + ', ' +
            self.state.abbreviation +
            ' ' +
            self.zipcode
    #Return
    display_string.html_safe
  end

  def phone_number
    display_string =
        self.phone_office = hyphen_phone_number self.phone_office
    if self.phone_office_ext.present?
      '<br />' +
          'Ext: ' +
          self.phone_office_ext
    end
    #Return
    display_string.html_safe
  end

  #Renewal date is derived from the Authorize model
  #def renewal_date
  #  if self.authorizes[0].present?
  #    self.authorizes[0].renewal_date
  #  else
  #    '' #return a blank if nil
  #  end
  #end

  private


  def hyphen_phone_number phone
    #Add hyphens to phone number
    phone = phone.insert -5,'-'
    phone = phone.insert -9,'-'
  end

  def blank_line_check line
    #Suppress line if blank
    if line.present?
      line + '<br />'
    else
      ''
    end
  end


  # Clean phone numbers before saving
  def phone_office_clean
    self.phone_office = self.phone_office.to_s.gsub(/[^0-9]/, '')
  end

end
