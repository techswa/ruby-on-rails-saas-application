class Eyecolor < ActiveRecord::Base
  attr_accessible :abbreviation, :description, :name

  #Abbreviation
  validates_presence_of :abbreviation
  validates_format_of :abbreviation, with: /\A[a-zA-Z]+\z/
  validates_length_of :abbreviation, minimum: 3 , maximum: 3


end
