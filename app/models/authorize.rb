class Authorize < ActiveRecord::Base
  attr_accessible :archived, :expiration_date, :payment_date, :payment, :payment_note, :paymethod_id, :store_id, :user_id

  #attr_accessor :license  #Needed for check sum

  belongs_to :paymethod
  belongs_to :store
  belongs_to :user

  monetize :payment_cents, :as => 'payment'

  #monetize :discount_subunit, :as => "discount"
  #composed_of :payment_amount,
  #            :class_name => 'Money',
  #            :mapping => %w(price cents),
  #            :converter => Proc.new { |value| value.respond_to?(:to_money) ? value.to_money : Money.empty }

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(inactive: true) }

  scope :authorized, -> { where(active: true) }



end
