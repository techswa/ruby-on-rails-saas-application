class Seller < ActiveRecord::Base
  attr_accessible :address_1, :address_2, :astate_id, :city, :day, :month_id, :year, :eyecolor_id, :gender_id,
                  :height_feet, :height_inches, :license_id, :name_first, :name_last, :number, :state_id, :zip, :email

  belongs_to :eyecolor
  belongs_to :license
  belongs_to :gender
  belongs_to :month
  # Connect to states lookup table
  belongs_to :state, :class_name => "State"
  belongs_to :astate, :class_name => "State"
end
