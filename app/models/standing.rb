class Standing < ActiveRecord::Base
  attr_accessible :description, :name, :sort

  has_many :accounts
  has_many :stores
  has_many :authorizes
end
