class User < ActiveRecord::Base

  rolify
  #resourcify

  extend FriendlyId
  friendly_id :create_uuid, use: :slugged

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :trackable, :validatable#, :rememberable

  # Setup accessible (or protected) attributes for your model

   attr_accessible :account_id, :slug, :first_name, :last_name, :email, :password,
                   :role_id,:remember_me, :cellcarrier_id, :phone_cell


   attr_accessor   :primary, :password_confirmation


  validates_presence_of :first_name

  belongs_to :account
  belongs_to :authorize
  belongs_to :cellcarrier

  has_many :authorizes


  validates :phone_cell, :presence => true
  validates :cellcarrier_id, :presence => true

  #validates_uniqueness_of :uuid

  def create_uuid
    _return = SecureRandom.uuid
    _return
  end

  # Verify current_user has necessary role to complete operation
  #def filter_list_by_role(current_user)
  #  # Only display records for roles that are managed
  #  #puts 'current user filter' + current_user.inspect
  #  _current_user = current_user
  #  if _current_user.has_role? :admin
  #    @account.users.admin_roles_managed
  #
  #  elsif _current_user.has_role? :accounting
  #    #@users = @account.users.accounting_roles_managed
  #    @account.users.accounting_roles_managed
  #
  #  elsif _current_user.has_role? :support
  #    @account.users.support_roles_managed
  #
  #  elsif _current_user.has_role? :primary
  #    @account.users.primary_roles_managed
  #
  #  elsif _current_user.has_role? :manager
  #    @account.users.manager_roles_managed
  #
  #  else
  #    @account.users.no_roles_managed
  #  end
  #end

  # Class scopes
  def self.who_is_primary
    with_any_role(:primary)
  end

  def self.admin_roles_managed
    with_any_role(:accounting, :support, :unassigned)
  end

  def self.accounting_roles_managed
    with_any_role(:primary, :manager, :buyer, :leo, :unassigned)
  end

  def self.support_roles_managed
    with_any_role(:manager, :buyer, :unassigned)
  end

  def self.primary_roles_managed
    with_any_role(:manager, :buyer, :unassigned)
  end

  def self.manager_roles_managed
    with_any_role(:buyer, :unassigned)
  end

  def self.no_roles_managed

  end

  # demeter reductions
  def account_slug
    self.account.slug
  end

  def company
    self.account.name
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def account_name
    self.account.name
  end

  def account_number
    self.account.number
  end

  def authorize_standing
    self.authorize.standing
  end

  def user_registered
    self.created_at.to_date
  end

  def account_name
    self.account.name
  end
end
