class Gender < ActiveRecord::Base
  attr_accessible :abbreviation, :description, :name

  has_many :sellers

  def listbox_method
    abbreviation
  end

end
