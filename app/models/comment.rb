class Comment < ActiveRecord::Base
  attr_accessible :content, :sticky

  belongs_to :commentable, :polymorphic => true


end
