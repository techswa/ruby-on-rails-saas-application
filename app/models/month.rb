class Month < ActiveRecord::Base
  attr_accessible :number, :abbreviation, :name

  has_many :sellers

  def listbox_options
    number + ' | ' + name
  end

end
