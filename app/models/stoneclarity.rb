class Stoneclarity < ActiveRecord::Base
  attr_accessible :abbreviation, :description, :language_id, :method, :name, :sort
end
