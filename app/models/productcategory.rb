class Productcategory < ActiveRecord::Base
  attr_accessible :description, :language_id, :name, :sort

  belongs_to :language

  has_many :products

end
