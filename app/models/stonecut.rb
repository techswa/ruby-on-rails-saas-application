class Stonecut < ActiveRecord::Base
  attr_accessible :abbreviation, :description, :language_id, :name, :sort

  belongs_to :language
end
