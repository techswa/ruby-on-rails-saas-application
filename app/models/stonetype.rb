class Stonetype < ActiveRecord::Base
  attr_accessible :name, :description, :language_id, :sort

  belongs_to :language

end
