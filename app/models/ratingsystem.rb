class Ratingsystem < ActiveRecord::Base
  attr_accessible :abbreviation, :description

  has_many :stonecolors

  def listbox_stonecolor
    abbreviation + ' : ' + description
  end
end
