class License < ActiveRecord::Base
  # Drivers, ID Card, etc
  attr_accessible :abbreviation, :description, :name

  has_many :sellers
end
