class Basemetal < ActiveRecord::Base
  attr_accessible :abbreviation, :name

  validates_presence_of :name, :abbreviation
  validates_length_of :abbreviation, minimum: 2
  has_many :metals

end
