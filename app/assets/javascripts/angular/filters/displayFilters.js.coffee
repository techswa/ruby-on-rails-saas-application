angular.module('displayFilters', [])

    .filter "test1", ->
      (input) ->
        alert "filter triggered" + input
        input = '@'

    .filter "extensionUnit", ->
      (input, extension) ->
        input = input + ' ' + extension

    .filter "gridfilter", ->
      (input) ->
        input = 'mouse'

    .filter "Example-range", ->
      (input, min, max) ->
        min = parseInt(min) #Make string input int
        max = parseInt(max)
        i = min
        while i < max
          input.push i
          i++
        input

