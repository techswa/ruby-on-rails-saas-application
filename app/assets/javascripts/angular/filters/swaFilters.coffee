angular.module('swaFilters', [])

    #Money filter convert pennies to dollars
    #All internal currenct is stored in pennies by using integer type
    .filter "formatDollar", ->
      (input) ->
        # Convert pennies to dollars
        output = input / 100
        #Will this need a comma separator
        if output > 999
          comma = true

        # Add decimal point, convert to string
        temp = output.toFixed(2).toString()

        # Add comma thousands separator
        if comma
          position = 1
          #ret = [temp.slice(0, position), ",", temp(position)].join('')
          ret = temp.substr(0, position) + "," + temp.substr(position)
        else
          ret = temp

        input = ret



    .filter "range", ->
      (input, min, max) ->
        min = parseInt(min) #Make string input int
        max = parseInt(max)
        i = min
        while i < max
          input.push i
          i++
        input

    .filter "minLen", ->
      (input, minLen, message) ->
          minLen = minLen or 0
          message = message or "Too short"
          return message  if typeof (input) is "undefined" or input.length < minLen
          input


    .filter "findChildren", ->
      (input) ->


      #alert("im working");
      # alert(input);

      #            minLen = minLen || 0;
      #            message = message || 'Too short';
      #
      #            if (typeof (input) === 'undefined' || input.length < minLen)
      #                return message;
      #            return input;

    .filter "keywordFilter", ->
      (items, name) ->
        arrayToReturn = []
        i = 0

        while i < items.length
          arrayToReturn.push items[i]  unless items[i].name is name.name
          i++
        arrayToReturn