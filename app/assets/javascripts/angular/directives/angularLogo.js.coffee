
#==============================
#
# Usage: <angular-logo></angular-logo>
# Result: Inserts image inside of element
#
#==============================

App.directive "angularLogo", ->
  restrict: 'ECMA'
  link: (scope, element, attrs) ->
    img = document.createElement('img')
    img.src = 'http://goo.gl/ceZGf'

    if (element[0].nodeType == 8)
      element.replaceWith(img)
    else
      element[0].appendChild(img)

