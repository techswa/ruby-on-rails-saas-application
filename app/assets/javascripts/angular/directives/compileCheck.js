App.directive('compileCheck', function() {
    return {
        restrict: 'A',
        compile: function(tElement, tAttrs) {
            tElement.append('<br/>Added during compilation phase!');
        }
    };
});