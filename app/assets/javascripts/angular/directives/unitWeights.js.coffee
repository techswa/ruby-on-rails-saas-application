App.directive "unitWeightSrvc", ->
  replace: true
  restrict: "E"
  scope:
    data: "="

  link: (scope, element, attrs) ->


    #scope.options = attrs.selectOptions;
  template: '<div><select name="testSelect" ng-model="unitWeight" ng-options="unit.abbreviation for unit in UnitWeights"><option value="">Code</option></select></div>'
