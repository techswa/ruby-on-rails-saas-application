# State dictionary
#attr_accessible :abbreviation, :name


App.factory "SellerDialogCtrlErrors", ($rootScope, $q, swaFn, SellerModel)  ->

  debug = false
  swaFn.logString(debug, "Entering SellerDialogError")

  #Holds tab details for dialog
  tabName = [
    {tab: 0, id: 'tab0', label: 'Id'},
    {tab: 1, id: 'tab1', label: 'Address'},
    {tab: 2, id: 'tab2', label: 'Details'}
    ]

  errorLog = []

  errorCodes = {}
  errorCodes =
    state_id:
      title: 'issuing state'
      required: true
      tab: 0
      msg: 'Please select the issuing state.'
      error: false
    license_id:
      title: 'id type'
      required: true
      tab: 0
      msg: 'Please select the license type.'
      error: false
    number:
      title: 'number'
      required: true
      tab: 0
      msg: 'Please enter the license number.'
      error: false
    month:
      title: 'month'
      required: true
      tab: 0
      msg: 'Please select month of birth.'
      error: false
    day:
      title: 'day'
      required: true
      tab: 0
      msg: 'Please select day of birth.'
      error: false
    year:
      title: 'year'
      required: true
      tab: 0
      msg: 'Please select year of birth.'
      error: false
    name_last:
      title: 'last name'
      required: true
      tab: 1
      msg: 'Please enter the last name of seller.'
      error: false
    name_first:
      title: 'first name'
      required: true
      tab: 1
      msg: 'Please enter the first name of seller.'
      error: false
    address_1:
      title: 'address line 1'
      required: true
      tab: 1
      msg: 'Please enter the block number and street as listed on the id.'
      error: false
    address_2:
      title: 'address line 2'
      required: false
      tab: 1
      msg: 'This is used for apt and suite numbers.'
      error: false
    city:
      title: 'city'
      required: true
      tab: 1
      msg: 'Please enter the city as listed on the id.'
      error: false
    astate:
      title: 'state'
      required: true
      tab: 1
      msg: 'Please enter the state as listed on the id.'
      error: false
    zip:
      title: 'zip code'
      required: true
      tab: 1
      msg: 'Please enter the zip code as listed on the id.'
      error: false
    height_feet:
      title: 'height feet'
      required: true
      tab: 2
      msg: 'Please enter the height in feet as listed on the id.'
      error: false
    height_inches:
      title: 'height inches'
      required: true
      tab: 2
      msg: 'Please enter the height in inches as listed on the id.'
      error: false
    gender:
      title: 'gender'
      required: true
      tab: 2
      msg: 'Please enter the gender as listed on the id.'
      error: false
    eyecolor:
      title: 'eye color'
      required: true
      tab: 2
      msg: 'Please enter the eye color as listed on the id.'
      error: false

  #Retun all error codes
  getErrorCodes: ->
    #SellerCtrl.errors
    return errorCodes

  #Return specific error code
  getErrorCode: (ctrl) ->
    return errorCodes[ctrl].error

  #Return the error report array
  getErrorLog: () ->
    return errorLog

  #Error logic- test each control for validity
  #Test each control for the form in here
  isError: (ctrl) ->
    return false

  #Set error error to true for passed control
  errorSet: (ctrl) ->
    swaFn.logString(debug, "Setting error flag on : " + ctrl)
    errorCodes[ctrl].error = true
    swaFn.logObject(debug, errorCodes, "errorCodes after to setting flag")

  #Tests all fields forerro flag value
  #returns true if any field shows an error
  #Adds error to error report array
  errorsExist: ->
    swaFn.logString(debug, "Entering hasErrors()")
    #Clear flag upon entering
    hasErrors = false
    #clear error log
    this.errorLogClear()
    #swaFn.logObject(debug, errorCodes, "errorCodes prior to error checking")

    for k, v of errorCodes
      swaFn.logString(debug, "field : " + k + " has value : " + v.error)
      #Check if error flag is set
      if v.error == true
        #alert "v.error = true"
        hasErrors = true
        swaFn.logString(debug, "Error code says " + k + " has error")
        #Add entry to errorReport for display to user
        errorLog.push k
    swaFn.logObject(debug, errorCodes, "errorCodes after checking")
    return hasErrors

  #Clear the error log of prior errors
  errorLogClear: () ->
    swaFn.logString(debug, 'Clearing errors from log.')
    errorLog = []

  errorClear: (ctrl)->
    #Clear all errors
    if ctrl.toLowerCase() == "all"
      #alert "Clearing errors"
        #loop through all codes and set to false
      swaFn.logObject(debug, errorCodes, "errorCodes")
      for k, v of errorCodes
        swaFn.logString("key: " + k)
        swaFn.logObject("value: " + v.error)
        #swaFn.logString("record: " + k)
        v.error = false
      swaFn.logObject(debug, errorCodes, "Cleared errorCodes")
    else
      errorCodes[ctrl].error = false


