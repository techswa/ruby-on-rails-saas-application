
App.factory "qryQuote", ($http, $q, swaFn) ->

  #Retreive current quote from Server based upon
  #All query parameters are determined at the server

  Query: ->

    #create our deferred object.
    deferred = $q.defer()


    #when data is returned resolve the deferment.
    $http.get("/quotes/current.json").success((data ,status) ->
      #console.log 'qryQuote Data OK: ' + status
      deferred.resolve data

    ).error (data,status)->
      alert 'Quote REST Error: ' + status
      #or reject it if there's a problem.
      deferred.reject()

    #return the promise that work will be done.
    deferred.promise


  #Creates new quote entry on server

  Create: (data) ->

  #Create new record on Server via REST and JSON
  #Query parameters for User, Store  and Account are determined at the server
  Create: ->

    #make the call.
    swaFn.logString(debug, "Entering REST Quote Create")
    swaFn.logObject(debug, data, "JSON record to save")
    #create our deferred object.
    deferred = $q.defer()

    #Set payment info
    #quote = {}
    #quote = {account_id:1, ag_payout:100, ag_spot:200, au_payout:300, au_spot:400, pt_payout:500, pt_spot:600, date_time:null, store_id:1, user_id:1}

    #make the call.

    #when data is returned resolve the deferment.
    $http.post("/quotes/", data).success((data,status, headers, config) ->
        # console.log 'Fetched genders from server'
        #alert "Sucessful create data: " + data
        swaFn.logObject(debug, data, "Sucessful create data:")
        swaFn.logObject(debug, status, "Sucessful create status:")
        swaFn.logObject(debug, headers, "Sucessful create headers:")
        swaFn.logObject(debug, config, "Sucessful create config:")
        deferred.resolve data
      ).error (data, status)->
        alert "Quotes REST failed. Status: " + status
        #swaFn.logObject(debug, status, "Failed create status:")
        #or reject it if there's a problem.
        deferred.reject()

    #return the promise that work will be done.
    deferred.promise