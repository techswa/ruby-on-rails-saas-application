# Sellers dictionary
#attr_accessible :address_1, :address_2, :astate_id, :city, :day, :month_id, :year, :eyecolor_id, :gender_id,
#:height_feet, :height_inches, :license_id, :name_first, :name_last, :number, :state_id, :zip, :email

App.factory "qrySeller", ($http, $q, swaFn) ->

  debug = true

  Create: (data) ->
    #make the call.
    swaFn.logString(debug, "Entering Seller REST Create")
    swaFn.logObject(debug, data, "JSON record to save")
    #create our deferred object.
    deferred = $q.defer()

    #Set payment info
    #quote = {}
    #quote = {account_id:1, ag_payout:100, ag_spot:200, au_payout:300, au_spot:400, pt_payout:500, pt_spot:600, date_time:null, store_id:1, user_id:1}

    #make the call.

    #when data is returned resolve the deferment.
    $http.post("/sellers/", data).success((data,status, headers, config) ->
      # console.log 'Fetched genders from server'
      #alert "Sucessful create data: " + data
      swaFn.logObject(debug, data, "Sucessful create data:")
      swaFn.logObject(debug, status, "Sucessful create status:")
      swaFn.logObject(debug, headers, "Sucessful create headers:")
      swaFn.logObject(debug, config, "Sucessful create config:")
      deferred.resolve data
    ).error (data, status)->
      swaFn.logObject(debug, status, "Failed create status:")
      #or reject it if there's a problem.
      deferred.reject()

    #return the promise that work will be done.
    deferred.promise

#Used to retreive all Sellers from server
App.factory('qrySellers', ['$resource', ($resource) ->
  $resource('/sellers.json', {}, {
    query: { method: 'GET', isArray: true }
    create: { method: 'POST' }
  })
]).
#Use to Create Seller record
factory('qrySeller', ['$resource', ($resource) ->
  $resource('/sellers/:id.json', {id:'@id'}, {
    show: { method: 'GET' }
    update: { method: 'PUT' }
    destroy: { method: 'DELETE' }
  })
]).factory('TmpSeller', () ->
  return {"address_1":"2312 Walker","address_2":"#213","astate_id":1,"city":"Dallas","created_at":"2013-05-22T22:17:07Z","day":"3","email":"jsmth@wwww.com","eyecolor_id":4,"gender_id":1,"height_feet":6,"height_inches":1,"id":1,"license_id":"1","month_id":"2","name_first":"Jerry","name_last":"Smith","number":"2548712","state_id":1,"updated_at":"2013-05-23T15:03:57Z","year":"1992","zip":"75874"}
)

