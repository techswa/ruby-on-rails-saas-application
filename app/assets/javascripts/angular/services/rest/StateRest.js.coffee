# State dictionary
#attr_accessible :abbreviation, :name

App.factory "qryStates", ($http, $q) ->
  Query: ->

    #create our deferred object.
    deferred = $q.defer()

    #make the call.

    #when data is returned resolve the deferment.
    $http.get("/states.json").success((data) ->
      deferred.resolve data
    ).error ->
      alert 'State REST Error: ' + status
      #or reject it if there's a problem.
      deferred.reject()

    #return the promise that work will be done.
    deferred.promise

#
#App.factory('States', ['$resource', ($resource) ->
#  $resource('/states.json', {}, {
#    query: { method: 'GET', isArray: true }
#    create: { method: 'POST' }
#  })
#]).
#factory('State', ['$resource', ($resource) ->
#  $resource('/states/:id.json', {id:'@id'}, {
#    show: { method: 'GET' }
#    update: { method: 'PUT' }
#    destroy: { method: 'DELETE' }
#  })
#])