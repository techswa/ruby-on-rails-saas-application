# StoneTypes dictionary
#attr_accessible :name, :description, :language_id, :sort

App.factory "qryStoneTypes", ($http, $q) ->
  Query: ->

    #create our deferred object.
    deferred = $q.defer()

    #make the call.

    #when data is returned resolve the deferment.
    $http.get("/stonetypes.json").success((data) ->
      deferred.resolve data
    ).error ->
      alert 'Stone Type Rest Error: ' + status
      #or reject it if there's a problem.
      deferred.reject()

    #return the promise that work will be done.
    deferred.promise
