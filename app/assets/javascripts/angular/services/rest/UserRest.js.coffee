# Sellers dictionary
#attr_accessible :address_1, :address_2, :astate_id, :city, :day, :month_id, :year, :eyecolor_id, :gender_id,
#:height_feet, :height_inches, :license_id, :name_first, :name_last, :number, :state_id, :zip, :email


App.factory('qryUser', ['$resource', ($resource) ->
  $resource('/users/:id.json', {id:'@id'}, {
    show: { method: 'GET' }
    update: { method: 'PUT' }
    destroy: { method: 'DELETE' }
  })
]).
factory "qryUserCurrent", ($http, $q) ->
  Query: ->

    #create our deferred object.
    deferred = $q.defer()

    #make the call.

    #when data is returned resolve the deferment.
    $http.get("/users/current.json").success((data ,status) ->
      #alert 'Data OK: ' + status
      deferred.resolve data

    ).error (data,status)->
      alert 'User REST Error: ' + status
      #or reject it if there's a problem.
      deferred.reject()

    #return the promise that work will be done.
    deferred.promise
