# State dictionary
#attr_accessible :abbreviation, :name


App.factory "qryEyeColors", ($http, $q) ->
  Query: ->

    #create our deferred object.
    deferred = $q.defer()

    #make the call.

    #when data is returned resolve the deferment.
    $http.get("/eyecolors.json").success((data) ->
      deferred.resolve data
    ).error ->
      alert 'Eye Color REST Error: ' + status
      #or reject it if there's a problem.
      deferred.reject()

    #return the promise that work will be done.
    deferred.promise

