# StoneSizes dictionary
#attr_accessible :abbreviation, :description, :language_id, :sort

App.factory "qryStoneSizes", ($http, $q) ->
  Query: ->

    #create our deferred object.
    deferred = $q.defer()

    #make the call.

    #when data is returned resolve the deferment.
    $http.get("/stonesizes.json").success((data) ->
      deferred.resolve data
    ).error ->
      alert 'Stone Size REST Error: ' + status
      #or reject it if there's a problem.
      deferred.reject()

    #return the promise that work will be done.
    deferred.promise
