# StoneCuts dictionary
#attr_accessible :abbreviation, :description, :language_id, :name, :sort

App.factory "qryStoneCuts", ($http, $q) ->
  Query: ->

    #create our deferred object.
    deferred = $q.defer()

    #make the call.

    #when data is returned resolve the deferment.
    $http.get("/stonecuts.json").success((data) ->
      deferred.resolve data
    ).error ->
      alert 'Stone Cut REST Error: ' + status
      #or reject it if there's a problem.
      deferred.reject()

    #return the promise that work will be done.
    deferred.promise
