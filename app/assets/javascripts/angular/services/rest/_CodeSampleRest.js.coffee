# Items dictionary
# id, po_id, name, cost, qty, price, created_at, updated_up

# Works perfectly for new records only
#App.factory 'BookModel', ['$resource', ($resource) ->
#  $resource '/books/:id', id: '@id', update: {method: "PUT"}
#]

#App.factory 'BookModel', ['$resource', ($resource) ->
#  $resource '/books/:id', id: '@id', update: {method: "PUT"}
#]

App.factory('Skippy', ['$resource', ($resource) ->
  $resource('/books.json', {}, {
    query: { method: 'GET', isArray: true }
    create: { method: 'POST' }
  })
]).
factory('Dopy', ['$resource', ($resource) ->
  $resource('/books/:id.json', {id:'@id'}, {
     show: { method: 'GET' }
     update: { method: 'PUT' }
     destroy: { method: 'DELETE' }
  })
])