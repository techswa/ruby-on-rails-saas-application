# StoneColor dictionary
#attr_accessible :ratingsystem_id, :abbreviation, :description, :language_id, :method, :name, :sort

App.factory "qryProductCategories", ($http, $q) ->
  Query: ->

    #create our deferred object.
    deferred = $q.defer()

    #make the call.

    #when data is returned resolve the deferment.
    $http.get("/productcategories.json").success((data ,status) ->
      #alert 'Data OK: ' + status
      deferred.resolve data

    ).error (data,status)->
      alert 'Product Categories REST Error: ' + status
         #or reject it if there's a problem.
      deferred.reject()

    #return the promise that work will be done.
    deferred.promise
