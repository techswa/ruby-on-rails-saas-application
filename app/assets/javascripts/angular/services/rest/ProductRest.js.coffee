# Product dictionary
#attr_accessible :description, :language_id, :name, :productcategory_id, :sort

App.factory "qryProducts", ($http, $q) ->
  Query: ->

    #create our deferred object.
    deferred = $q.defer()

    #make the call.

    #when data is returned resolve the deferment.
    $http.get("/products.json").success((data ,status) ->
      #alert 'Data OK: ' + status
      deferred.resolve data

    ).error (data,status)->
      alert 'Product REST Error: ' + status
         #or reject it if there's a problem.
      deferred.reject()

    #return the promise that work will be done.
    deferred.promise
