# State dictionary
#attr_accessible :abbreviation, :name


App.factory "qryPaymentMethods", ($http, $q) ->
  Query: ->

    #create our deferred object.
    deferred = $q.defer()

    #make the call.

    #when data is returned resolve the deferment.
    $http.get("/payments.json").success((data) ->
     # console.log 'Fetched genders from server'
      deferred.resolve data
    ).error ->
      alert 'Payment Query REST Error: ' + status
      #or reject it if there's a problem.
      deferred.reject()

    #return the promise that work will be done.
    deferred.promise

