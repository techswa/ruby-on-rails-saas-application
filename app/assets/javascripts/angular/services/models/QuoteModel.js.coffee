# State dictionary
#attr_accessible :abbreviation, :name
#au_spot, au_payout, ag_spot, ag_payout, pt_spot, pt_payout
#:account_id, :ag_payout, :ag_spot, :au_payout, :au_spot, :date_time, :pt_payout, :pt_spot, :store_id, :user_id
App.factory "QuoteModel", ($rootScope, $q, qryQuote, swaFn)  ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering QuoteModel")


  self = this
  quotes = []



  # Load metal details
  LoadDataTables = () ->

    $q.all([
      qryQuote.Query(),
    ]).then (result) ->

      quotes = result[0]
      ret = 0

      $rootScope.$broadcast('QuoteChange')

  #Load the data
# Stop query to backend
  #LoadDataTables()

  saveQuotes: ->
    swaFn.logString(debug, "QuoteModel.saveQuotes")
    qryQuote.Create()


  getQuotes: ->
    console.log "QuoteModel getQuotes(): " + JSON.stringify(quotes, null, 4)
    return quotes

  getSpotGold: ->
    return quotes.au_spot

  #Inform listeners of change
  BroadcastQuoteChange: ->
    $rootScope.$broadcast('QuoteChange')
    return 0

  #gold quote in $/oz
  #20 dwt/troy oz

  getSpotDisplay: (metal) ->
    if metal == 'Au'
      input = quotes.au_spot
    else if metal == 'Ag'
      input = quotes.ag_spot
    else if metal == 'Pt'
      input = quotes.pt_spot
    # Convert pennies to dollars
    output = input / 100
    #Will this need a comma separator
    if output > 999
      comma = true
    # Add decimal point, convert to string
    temp = output.toFixed(2).toString()
    # Add comma thousands separator
    if comma
      position = 1
      #ret = [temp.slice(0, position), ",", temp(position)].join('')
      ret = temp.substr(0, position) + "," + temp.substr(position)
    else
      ret = temp
    input = ret



  setPayout: (metal,value) ->
    if metal == 'Au'
      quotes.au_payout = value
    else if metal == 'Ag'
      quotes.ag_payout = value
    else if metal == 'Pt'
      quotes.pt_payout = value

  getPayout: (metal) ->
    if metal == 'Au'
      return quotes.au_payout
    else if metal == 'Ag'
      return quotes.ag_payout
    else if metal == 'Pt'
      return quotes.pt_payout

  setSpot: (metal,value) ->
    if metal == 'Au'
      quotes.au_spot = value
    else if metal == 'Ag'
      quotes.ag_spot = value
    else if metal == 'Pt'
      quotes.pt_spot = value

  getSpot: (metal) ->

    if metal == 'Au'
      return quotes.au_spot
    else if metal == 'Ag'
      return quotes.ag_spot
    else if metal == 'Pt'
      return quotes.pt_spot

  #Used to format dollar display
  DollarFormatted: (amount) ->
    input = parseInt(amount)
    # Convert pennies to dollars
    output = input / 100
    #Will this need a comma separator
    if output > 999
      comma = true

    # Add decimal point, convert to string
    temp = output.toFixed(2).toString()

    # Add comma thousands separator
    if comma
      position = 1
      #ret = [temp.slice(0, position), ",", temp(position)].join('')
      ret = temp.substr(0, position) + "," + temp.substr(position)
    else
      ret = temp

    input = ret


