
App.factory "SellerModel", ($rootScope, $q, qrySeller, swaFn)  ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering SellerModel")

  #==========================================
  #  Define arrays and structures needed
  #==========================================

  # Used to hold record
  Sellers = [{
    id: null
    state_id: null
    state: null
    license_id: null
    license: null
    number: null
    month: null
    day: null
    year: null
    name_last: null
    name_first: null
    address_1: null
    address_2: null
    city: null
    astate_id: null
    astate: null
    zipcode: null
    height_feet: null
    height_inches: null
    gender_id: null
    gender: null
    eyecolor_id: null
    eyecolor: null
  }]

  # Used to hold edit data
  buffer = {}


  fixtureSeller = [{
    id: null
    state_id: 1
    state: 'TX'
    license_id: 1
    license: 'DL'
    number: '01234567'
    month: 8
    day: 25
    year: 1958
    name_last: 'Smith'
    name_first: 'Justin'
    address_1: '2314 Oak St'
    address_2: 'Apt 43'
    city: 'Dallas'
    astate_id: 1
    astate: 'TX'
    zipcode: '75231'
    height_feet: 5
    height_inches: 7
    gender_id: 1
    gender: "M"
    eyecolor_id: 3
    eyecolor: 'BLK'
  }]

  # Testing only - load sample data
  Sellers[0] = fixtureSeller[0]

  Fields = [
    {
      field: 'id'
      value: null
      title:'My id field title'
      required: false
      error: false
      error_msg:"My error msg for id field"
    },{
      field: 'state_id'
      value: null
      title: null
      required: false
      error: true
      error_msg:null
    },{
      field: 'license_id'
      value: null
      title: null
      required: false
      error: false
      error_msg:null
    },{
      field: 'number'
      value: 369
      title: null
      required: false
      error: false
      error_msg:null
    },{
      field: 'month'
      value: null
      title: null
      required: false
      error: true
      error_msg:null
    },{
      field: 'day'
      value: null
      title: null
      required: false
      error: false
      error_msg:null
    },{
      field: 'year'
      value: null
      title: null
      required: false
      error: false
      error_msg:null
    },{
      field: 'name_last'
      value: null
      title: null
      required: false
      error: false
      error_msg:null
    },{
      field: 'name_first'
      value: null
      title: null
      required: false
      error: false
      error_msg:null
    },{
      field: 'city'
      value: null
      title: null
      required: false
      error: false
      error_msg:null
    },{
      field: 'state'
      value: null
      title: null
      required: false
      error: false
      error_msg:null
    },{
      field: 'zipcode'
      value: null
      title: null
      required: false
      error: false
      error_msg:null
    },{
      field: 'height_feet'
      value: null
      title: null
      required: false
      error: false
      error_msg:null
    },{
      field: 'height_inches'
      value: null
      title: null
      required: false
      error: false
      error_msg:null
    },{
      field: 'gender_id'
      value: null
      title: null
      required: false
      error: false
      error_msg:null
    },{
      field:'eyecolor_id'
      value: null
      title: null
      required: false
      error: false
      error_msg:null
    }
  ]


  #=============================================
  #  Buffer Getters and Setters
  #=============================================
  #Buffer is the temp area for a record to be edited in

  # Load record into buffer from from Items {}
  # Called from the main dialog controller
  # This buffer is shared by the tab contollers

  loadFixtureRecord: () ->
    swaFn.logString(debug, 'Called SellerModel.loadFixtureRecord()')
    #load fixture data for testing
    buffer = fixtureSeller[0]

  loadBuffer: () ->
    # Copy Sellers to Buffer
    buffer = Sellers[0]

  # Save buffer
  saveBuffer: () ->
      # Call create Seller
      this.createSeller()

  # Return buffer for processing
  getBuffer: ->
    return buffer

  clearBuffer: ->
    buffer = {}

  getBufferValue: (Key) ->
    for key, value of buffer
      if key == Key
        return value

  #=============================================
  #  Sellers Getters and Setters
  #=============================================

  # Return all records
  getSellers: ->
    return Sellers

  # Return value for display
  getFieldValue: (field) ->
    for key, value of Sellers[0]
      if key == field
        return value

  # Ceate new record to fill
  # id: null to signify new record
  getNewSeller: ->
    return editSeller

  # Copy Buffer data to new item entry
  createSeller: ->
    # Delete old record
    Sellers = []
    # Add buffer as single array item
    Sellers.push buffer


  # Update existing item from Buffer
  updateSeller: ->
    for record in Sellers
      if record.id == buffer.id  # Found record to update
        record.state_id = buffer.state_id
        record.license_id = buffer.license_id
        record.number = buffer.number
        record.month = buffer.month
        record.day = buffer.day
        record.year = buffer.year
        record.name_last = buffer.name_last
        record.name_first = buffer.name_first
        record.address_1 = buffer.address_1
        record.address_2 = buffer.address_2
        record.city = buffer.city
        record.astate_id = buffer.astate_id
        record.zipcode = buffer.zipcode
        record.height_feet = buffer.height_feet
        record.height_inches = buffer.height_inches
        record.gender_id = buffer.gender_id
        record.eyecolor_id = buffer.eyecolor_id
        record.hallmark = buffer.hallmark
        record.engraving = buffer.engraving
        record.comments = buffer.comments


  #=============================================
  #  Error Getters and Setters
  #=============================================

  setErrorAll: ->
    for k,v of Fields
      v.error = true

  setError: (field) ->
    for k,v of Fields
      if k == field
        v.error = true

  getError: (field) ->
    for k,v of Fields
      if k == field
        return v.error


#=============================================
  #  Save record via REST
  #=============================================

#  #Add Seller record to server
#  createSeller: ->
#    swaFn.logString(debug, "SellerModel.createSeller")
#    qrySeller.Create(Seller)

