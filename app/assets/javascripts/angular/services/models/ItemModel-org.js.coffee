App.factory "ItemModel-org", ($rootScope, swaFn)  ->

  #Set debug flag
  debug = false

  swaFn.logString(debug, "Entered ItemModel")

  self = this

  Items = [{id:1, productCategory_id: 1, product_id: 2, product: 'Charm bracelet', gender_id: 1, gender: 'F', metal_base: 'Au', metal_purity: 417, metal_id: 1, metal_name: '10K/YG', unitWeight_id: 1, weight: '22.3', weightSummary: '22.3 dwt', payment: 12345, unitSize_id: 1, size: '18', sizeSummary: '18 cm', description: 'Really nice ring', engraving:'', comments: '' },
    {id:2, productCategory_id: 4, product_id: 4, product:'Engagement ring', gender_id: 2, gender: 'F', metal_base: 'Au', metal_purity: 500, metal_id: 2, metal_name: '12K/YG', unitWeight_id: 1, weight: '26.8', weightSummary: '26.8 dwt', payment: 87523, unitSize_id: 3, size: '8', sizeSummary: '8 rs', description: 'Nice ring', engraving:'', comments: '' },
    {id:6, productCategory_id: 3, product_id: 6, product:'Hoop earrings', gender_id: 2, gender: 'F', metal_base: 'Au', metal_purity: 583, metal_id: 3, metal_name: '14K/YG', unitWeight_id: 1, weight: '104.5', weightSummary: '104.5 dwt', payment: 25589, unitSize_id: 1, size: '17', sizeSummary: '17 cm', description: 'Really large hoops', engraving:'', comments: '' },
    {id:9, productCategory_id: 4, product_id: 5, product:'Class ring', gender_id: 1, gender: 'M', metal_base: 'Au', metal_purity: 750, metal_id: 4, metal_name: '18K/YG', unitWeight_id: 1, weight: '69.3', weightSummary: '69.3 dwt', payment: 1537, unitSize_id: 3, size: '11', sizeSummary: '11 rs', description: 'Class of 1974 Falon High', engraving:'', comments: '' }]

  test: ->
    alert "Hello"

  #Total up all item's payments
  getItemsTotalValue: ->
    total = 0
    for record in Items
      total = total + record.payment
    return total

  #Get Items total by metal
  getItemsTotalValueByMetalId: (metal_id) ->
    total = 0
    for record in Items
      if record.metal_id == metal_id
        total = total + record.payment
    return total

  #Get total weight by metal
  getItemsTotalWeightByMetalId: (metal_id) ->
    total = 0
    for record in Items
      if record.metal_id == metal_id
        total = total + parseFloat(record.weight)
    return total

  #Get all records
  getItems: ->
    return Items

  #Get record by id as integer
  getItem: (id) ->
    for record in Items
      if record.id = id
        return record
      else
        return "Record not found."

  #Delete selected item by record id
  deleteItem: (record_id) ->
    swaFn.logObject(debug, record_id, "ItemModel.deleteItem - record_id")
    row_index = 0 # Count the rows before deletion
    swaFn.logObject(debug, Items, "ItemModel.deleteItem - Items")

    for record in Items
      #console.log "ItemModel.deleteItem - record in Items" + JSON.stringify(record, null, 4)
      if record.id == record_id
        #alert row_index
        #Delete 1 record starting at the index
        swaFn.logString(debug, "ItemModel.deleteItem - splicing Item")
        Items.splice(row_index,1)
        break
      row_index++
    #Alert listeners
    $rootScope.$broadcast('ModelUpdated')



  #New item for editing - passed to createItem() after edit session for appendng to array
  #Ceate new record to fill
  #id: null to signify new record
  newItem: ->
    return [{id:null, productCategory_id: null, product_id: null, product:'', gender_id: null, gender: '', metal_base: '', metal_purity: null, metal_id: null, metal_name: '', unitWeight_id: null, weight: null, weightSummary: '', payment: null, unitSize_id: null, size: '', sizeSummary: '', description: '', engraving:'', comments: '' }]

  #Create new item from passed object data
  createItem: (item) ->
      #grab next record id for this record
      item.id = this.nextRowId()
      #Add new item to item array
      Items.push(item)
      #Alert listeners
      $rootScope.$broadcast('ModelUpdated')
      swaFn.logObject(debug, item, "Item added in Model")
      return "Record has been saved"

  #Update existing record by record id
  updateItem: (updatedRecord) ->
    #Interate records and select target
    swaFn.logString(debug, "updated record id: " + updatedRecord.id)
    swaFn.logString(debug, "item count: " + Items.length)
    for targetRecord in Items
      swaFn.logString(debug, "item: " + updatedRecord.product)
      swaFn.logString(debug, "testing updated record.id: " + updatedRecord.id)
      swaFn.logString(debug, "testing target record.id: " + targetRecord.id)
      if targetRecord.id == updatedRecord.id
        swaFn.logString(debug, "Updating")
        #Process changes when record is found
        targetRecord.productCategory_id = updatedRecord.productCategory_id
        targetRecord.product_id = updatedRecord.product_id
        targetRecord.product = updatedRecord.product
        targetRecord.gender_id = updatedRecord.gender_id
        targetRecord.gender = updatedRecord.gender
        targetRecord.metal_base = updatedRecord.metal_base
        targetRecord.metal_purity = updatedRecord.metal_purity
        targetRecord.metal_id = updatedRecord.metal_id
        targetRecord.metal_name = updatedRecord.metal_name
        targetRecord.unitWeight_id = updatedRecord.unitWeight_id
        targetRecord.weight = updatedRecord.weight
        targetRecord.weightSummary = updatedRecord.weightSummary
        targetRecord.payment = updatedRecord.payment
        targetRecord.unitSize_id = updatedRecord.unitSize_id
        targetRecord.size = updatedRecord.size
        targetRecord.sizeSummary = updatedRecord.sizeSummary
        targetRecord.description = updatedRecord.description
        targetRecord.engraving = updatedRecord.engraving
        targetRecord.comments = updatedRecord.comments
#
#      else
#        return "Record not found."
#      #Alert listeners
      $rootScope.$broadcast('ItemChange')

  #Save new item record to array
  saveNewItem: (record) ->



  #Find next row id in order to add new row
  nextRowId: ->
    lastRow = 0
    for record in Items
      if record.id > lastRow
        lastRow = record.id
    return lastRow + 1

  getSpotDisplay: (metal) ->
    if metal == 'Au'
      input = quotes.spotGold
    else if metal == 'Ag'
      input = quotes.spotSilver
    else if metal == 'Pt'
      input = quotes.spotPlatinum
    # Convert pennies to dollars
    output = input / 100
    #Will this need a comma separator
    if output > 999
      comma = true
    # Add decimal point, convert to string
    temp = output.toFixed(2).toString()
    # Add comma thousands separator
    if comma
      position = 1
      #ret = [temp.slice(0, position), ",", temp(position)].join('')
      ret = temp.substr(0, position) + "," + temp.substr(position)
    else
      ret = temp
    input = ret



  setPayout: (metal,value) ->
    if metal == 'Au'
      quotes.payoutGold =value
    else if metal == 'Ag'
      quotes.payoutSilver =value
    else if metal == 'Pt'
      quotes.payoutPlatinum =value

  getPayout: (metal) ->
    if metal == 'Au'
      return quotes.payoutGold
    else if metal == 'Ag'
      return quotes.payoutSilver
    else if metal == 'Pt'
      return quotes.payoutPlatinum

  setSpot: (metal,value) ->
    if metal == 'Au'
      quotes.spotGold = value
    else if metal == 'Ag'
      quotes.spotSilver = value
    else if metal == 'Pt'
      quotes.spotPlatinum = value

  getSpot: (metal) ->

    if metal == 'Au'
      return quotes.spotGold
    else if metal == 'Ag'
      return quotes.spotSilver
    else if metal == 'Pt'
      return quotes.spotPlatinum

  #Used to format dollar display
  DollarFormatted: (amount) ->
    input = parseInt(amount)
    # Convert pennies to dollars
    output = input / 100
    #Will this need a comma separator
    if output > 999
      comma = true

    # Add decimal point, convert to string
    temp = output.toFixed(2).toString()

    # Add comma thousands separator
    if comma
      position = 1
      #ret = [temp.slice(0, position), ",", temp(position)].join('')
      ret = temp.substr(0, position) + "," + temp.substr(position)
    else
      ret = temp

    input = ret


