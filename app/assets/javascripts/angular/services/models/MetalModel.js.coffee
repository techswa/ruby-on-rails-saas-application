# State dictionary
#attr_accessible :abbreviation, :name

App.factory "MetalModel", ($rootScope, $q, qryMetals, swaFn)  ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering MetalModel")


 #MetalDetails = Gon.metals

# # Code below works - commented out to test Gon
#  # Load metal details
#  LoadDataTables = () ->
##    console.log "LoadForeignKeys"
#    $q.all([
#      qryMetals.Query(),
#    ]).then (result) ->
#      MetalDetails = result[0]
#      ret = 0
#
#  #Load the data
#  LoadDataTables()
