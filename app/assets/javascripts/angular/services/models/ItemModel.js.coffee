
App.factory "ItemModel", ($rootScope, swaFn)  ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering ItemModel")


  #==========================================
  #  Define arrays and structures needed
  #==========================================
  #self = this

  #Used to hold all active records
  Items = []

  # Used as temp file for editing.
  # Appended to Items if new and saved
  # Replaces data in existing record if an edit
  Buffer = {}

  Error = {}
  # Contains a fresh array to clear buffer with
  RecordTemplate =
    {
      id: null
      productCategory_id: null
      product_id: null
      product: null
      gender_id: null
      gender: null
      metal_base: null
      metal_purity: null
      metal_id: null
      metal_name: null
      unitWeight_id: null
      weight: null
      weightSummary: null
      payment: null
      unitSize_id: null
      size: null
      sizeSummary: null
      description: null
      hallmark: null
      engraving: null
      comments: null
    }



  fixtureItems = [
    {id:1, productCategory_id: 1, product_id: 2, product: 'Charm bracelet', gender_id: 2, gender: 'F',
    metal_base: 'Au', metal_purity: 417, metal_id: 1, metal_name: '10K/YG', unitWeight_id: 1, weight: '22.3', weightSummary: '22.3 dwt',
    payment: 12345, unitSize_id: 1, size: '18', sizeSummary: '18 cm',
    description: 'Really nice bracelet', engraving: 'Engraving details', hallmark: 'Hallmark details', comments: 'Comments',
    stones:[{id:1, qty:1, size:'S', color:'Red', type:'Costume'}]},
    {id:2, productCategory_id: 4, product_id: 4, product:'Engagement ring', gender_id: 2, gender: 'F',
    metal_base: 'Au', metal_purity: 500, metal_id: 2, metal_name: '12K/YG', unitWeight_id: 1, weight: '26.8', weightSummary: '26.8 dwt',
    payment: 87523, unitSize_id: 3, size: '8', sizeSummary: '8 rs',
    description: 'Nice ring', engraving: 'Engraving details', hallmark: 'Hallmark details', comments: 'Comments',
    stones:[{id:1, qty:1, size:'S', color:'Red', type:'Costume'},
    {id:2, qty:2, size:'M', color:'Blue', type:'Costume'}]},
    {id:6, productCategory_id: 3, product_id: 6, product:'Hoop earrings', gender_id: 2, gender: 'F',
    metal_base: 'Au', metal_purity: 583, metal_id: 3, metal_name: '14K/YG', unitWeight_id: 1, weight: '104.5', weightSummary: '104.5 dwt',
    payment: 25589, unitSize_id: 1, size: '17', sizeSummary: '17 cm',
    description: 'Really large hoops', engraving: 'Engraving details', hallmark: 'Hallmark details', comments: 'Comments',
    stones:[]},
    {id:9, productCategory_id: 4, product_id: 5, product:'Class ring', gender_id: 1, gender: 'M',
    metal_base: 'Au', metal_purity: 750, metal_id: 4, metal_name: '18K/YG', unitWeight_id: 1, weight: '69.3', weightSummary: '69.3 dwt',
    payment: 1537, unitSize_id: 3, size: '11', sizeSummary: '11 rs', description: 'Class of 1974 Falon High', engraving: 'Engraving details', hallmark: 'Hallmark details', comments: 'Comments' },
    {id:6, productCategory_id: 3, product_id: 6, product:'Hoop earrings', gender_id: 2, gender: 'F',
    metal_base: 'Au', metal_purity: 583, metal_id: 3, metal_name: '14K/YG', unitWeight_id: 1, weight: '104.5', weightSummary: '104.5 dwt',
    payment: 25589, unitSize_id: 1, size: '17', sizeSummary: '17 cm',
    description: 'Really large hoops', engraving: 'Engraving details', hallmark: 'Hallmark details', comment: 'Comments',
    stones:[]},
    {id:9, productCategory_id: 4, product_id: 5, product:'Class ring', gender_id: 1, gender: 'M',
    metal_base: 'Au', metal_purity: 750, metal_id: 4, metal_name: '18K/YG', unitWeight_id: 1, weight: '69.3', weightSummary: '69.3 dwt',
    payment: 1537, unitSize_id: 3, size: '11', sizeSummary: '11 rs',
    description: 'Class of 1974 Falon High', engraving: 'Engraving details', hallmark: 'Hallmark details', comment: 'Comments',
    stones:[]}]




#  stones:[{qty:1, size:'S', color:'Red', type:'Costume'}]

  #Items holds all data for the fields
  Fields = [
    {
      id: 0
      field: 'id'
      title: null
      required: false
      initial: null
      tab: null
      error: false
      error_msg: null
    },{
      id: 1
      field: 'productCategory_id'
      title: null
      required: false
      initial: null
      tab: 0
      error: false
      error_msg: null
    },{
      id: 2
      field: 'product_id'
      title: null
      required: false
      initial: null
      tab: 0
      error: false
      error_msg: null
    },{
      id: 3
      field: 'gender_id'
      title: null
      required: false
      initial: null
      tab: null
      error: false
      error_msg: 'Please select the license type.'
    },{
      id: 4
      field: 'metal_id'
      title: null
      required: false
      initial: null
      tab: 1
      error: true
      error_msg: 'Please enter the license number.'
    },{
      id: 5
      field: 'unitWeight_id'
      title: null
      required: false
      initial: null
      tab: 1
      error: false
      error_msg: 'This is used for apt and suite numbers.'
    },{
      id: 6
      field: 'weight'
      title: null
      required: false
      initial: null
      tab: 1
      error: false
      error_msg: 'Please enter the block number and street as listed on the id.'
    },{
      id: 7
      field: 'unitSize_id'
      title: null
      required: false
      initial: null
      tab: 1
      error: false
      error_msg: 'Please enter the height in feet as listed on the id.'
    },{
      id: 8
      field: 'size'
      title: null
      required: false
      initial: null
      tab: 1
      error: false
      error_msg: 'Please enter the height in inches as listed on the id.'
    },{
      id: 9
      field:'description'
      title: null
      required: false
      initial: null
      tab: 2
      error: false
      error_msg: 'Please select the description test :)- as listed on the id.'
    },{
      id: 10
      field: 'engraving'
      title: null
      required: false
      initial: null
      tab: 2
      error: false
      error_msg: 'Please select the gender as listed on the id.'
    },{
      id: 11
      field:'hallmark'
      title: null
      required: false
      initial: null
      tab: 2
      error: false
      error_msg: 'Please select the eye color as listed on the id.'
    },{
    id: 12
    field:'comments'
    title: null
    required: false
    initial: null
    tab: 2
    error: false
    error_msg: 'Please select the eye color as listed on the id.'
    }
  ]



  #=============================================
  #  Broadcast changes
  #=============================================


  #Inform listeners of change
#  BroadcastQuoteChange: ->
#    $rootScope.$broadcast('QuoteChange')
#    return 0

  #Listen for quote changes
#  $scope.$on "QuoteChange",  ->
#    $scope.UpdateQuoteDisplay()

  #=============================================
  #  Buffer Getters and Setters
  #=============================================
  #Buffer is the temp area for a record to be edited in

  # Load record into buffer from from Items {}
  # Called from the main dialog controller
  # This buffer is shared by the tab contollers
  loadBuffer: (recId) ->
    for record in Items
      if record.id == recId
        # Make copy of record / save in buffer
        angular.copy(record, Buffer)
        #swaFn.alertObject(debug, Buffer, "Loaded Buffer")

  # New buffer for creating record
  newBuffer: ->
    Buffer = RecordTemplate

  # Save buffer
  saveBuffer: () ->
    #  New record
    if Buffer.id is null || Buffer.id is undefined
      this.createItem()
    else
      # Existing record
      this.updateItem()

  # Return buffer for processing
  getBuffer: ->
    return Buffer

  #Reset all field values to null
  clearBuffer: () ->
    #Buffer = BufferTemplate
    Buffer = {}

  getBufferValue: (Key) ->
    for key, value of Buffer
      if key == Key
        return value

  setBufferValue: (Key, Value) ->
    #find the record to update
    for key, value of Buffer
      if key == Key
        value.value = Value



  #=============================================
  #  Item Getters and Setters
  #=============================================

  #Add stone
  addStone: (stone_details) ->

    #Get next stone id
    #Test for no stones / first one
    if Buffer.stones.length == 0
      next_id = 1
    else
      #get next stone id
      last_id = 0
      for stone in Buffer.stones
        if stone.id > last_id
          last_id = stone.id
        next_id = last_id + 1

    #Add stone to array
    Buffer.stones.push({id:next_id, qty:stone_details.qty, size:stone_details.size, color:stone_details.color, type:stone_details.type})
    #alert "Next stone id: #{next_id}"
    #alert Buffer.stones[1].color
#    .push({id:3, qty:2, size:'L', color:'Green', type:'Diamond'})

  #Return stone by id
  getStone: (stone_id) ->
    for stone in Buffer.stones
      if stone.id is stone_id
        alert "Id #{stone.id} color #{stone.color}"
#    .push({id:3, qty:2, size:'L', color:'Green', type:'Diamond'})

  #Delete stone
  deleteStone: (stone_id) ->
    for key, value of Buffer.stones
      if value.id == stone_id
        Buffer.stones.splice(key,1)

  # Return all records
  getItems: ->
    return Items

  #Load test fixture
  useTestItems: ->
    Items = fixtureItems

  # Ceate new record to fill
  # id: null to signify new record
  getNewItem: ->
    return editItem

  # Copy Buffer data to new item entry
  createItem: ->
    # Set Buffer.id to next record
    Buffer.id = this.nextRowId()
    #alert "Next row id is: #{Buffer.id}"
    Items.push Buffer

  # Update existing item from Buffer
  updateItem: ->
    for record in Items
      if record.id == Buffer.id  # Found record to update
        record.productCategory_id = Buffer.productCategory_id
        record.product_id = Buffer.product_id
        record.product = Buffer.product
        record.gender_id = Buffer.gender_id
        record.gender = Buffer.gender
        record.metal_base = Buffer.metal_base
        record.metal_purity = Buffer.metal_purity
        record.metal_id = Buffer.metal_id
        record.metal_name = Buffer.metal_name
        record.unitWeight_id = Buffer.unitWeight_id
        record.weight = Buffer.weight
        record.weightSummary = Buffer.weightSummary
        record.payment = Buffer.payment
        record.unitSize_id = Buffer.unitSize
        record.size = Buffer.size
        record.sizeSummary = Buffer.sizeSummary
        record.description = Buffer.description
        record.hallmark = Buffer.hallmark
        record.engraving = Buffer.engraving
        record.comments = Buffer.comments

  #Called from controller to update a single field value
  setFieldValue: (field,value) ->
    #find the record to update
    for k,v of Items
      if v.field == field
        v.value = value

  #Called from controller to get field value
  getFieldValue: (field) ->
    #find the record to retrieve
    for k,v of Seller
      if v.field == field
        return v.value


  #=============================================
  #  UNUSED Getters and Setters
  #=============================================

  # Item's summary queries

  #Total up all item's payments
  getItemsTotalValue: ->
    total = 0
    for record in Items
      total = total + record.payment
    return total

  #Get Items total by metal
  getItemsTotalValueByMetalId: (metal_id) ->
    total = 0
    for record in Items
      if record.metal_id == metal_id
        total = total + record.payment
    return total

  #Get total weight by metal
  getItemsTotalWeightByMetalId: (metal_id) ->
    total = 0
    for record in Items
      if record.metal_id == metal_id
        total = total + parseFloat(record.weight)
    return total


  #Get record by id as integer
  getItem: (id) ->
    for record in Items
      if record.id = id
        return record
      else
        return false

  #Delete selected item by record id
  deleteItem: (record_id) ->
    swaFn.logObject(debug, record_id, "ItemModel.deleteItem - record_id")
    row_index = 0 # Count the rows before deletion
    swaFn.logObject(debug, Items, "ItemModel.deleteItem - Items")

    for record in Items
      if record.id == record_id
        #alert row_index
        #Delete 1 record starting at the index
        swaFn.logString(debug, "ItemModel.deleteItem - splicing Item")
        Items.splice(row_index,1)
        break
      row_index++




  #Find next row id in order to add new row
  nextRowId: ->
    lastRow = 0
    for record in Items
      if record.id > lastRow
        lastRow = record.id
    return lastRow + 1




  #Used to format dollar display
  DollarFormatted: (amount) ->
    input = parseInt(amount)
    # Convert pennies to dollars
    output = input / 100
    #Will this need a comma separator
    if output > 999
      comma = true

    # Add decimal point, convert to string
    temp = output.toFixed(2).toString()

    # Add comma thousands separator
    if comma
      position = 1
      #ret = [temp.slice(0, position), ",", temp(position)].join('')
      ret = temp.substr(0, position) + "," + temp.substr(position)
    else
      ret = temp

    input = ret



  #=============================================
  #  Error Getters and Setters
  #=============================================


  setErrorAll: ->
    for k,v of Fields
      v.error = true

  #Set all erros true for a tab
  setErrorTab: (tab) ->
    for k,v of Fields
      if k == field
        v.error = true

  getErrorsAll: ->
    return Fields

  setError: (field) ->
    for k,v of Fields
      if k == field
        v.error = true

  # Return current error setting for a single field
  getError: (field) ->
    for k,v of Fields
      if v.field is field
        return v.error


#=============================================
  #  Save record via REST
  #=============================================

#  #Add Seller record to server
#  createSeller: ->
#    swaFn.logString(debug, "SellerModel.createSeller")
#    qrySeller.Create(Seller)

