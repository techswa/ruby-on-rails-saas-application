# State dictionary
#attr_accessible :abbreviation, :name


App.service "SharedData", ($rootScope)  ->
  sharedData =
    staffId: "JD"
    totalPayment: 0




  BroadcastRecalcTotalPayment: ->
    $rootScope.$broadcast('RecalcTotalPayment')


  getStaffId: ->
    return sharedData.staffId
  setStaffId: (value) ->
    sharedData.staffId = value


  getTotalPayment: ->
    return sharedData.totalPayment
  setTotalPayment: (value) ->
    sharedData.totalPayment = value

  getTotalPaymentFormatted: ->
    sharedData.totalPayment
    input = parseInt(sharedData.totalPayment)
    # Convert pennies to dollars
    output = input / 100
    #Will this need a comma separator
    if output > 999
      comma = true

    # Add decimal point, convert to string
    temp = output.toFixed(2).toString()

    # Add comma thousands separator
    if comma
      position = 1
      #ret = [temp.slice(0, position), ",", temp(position)].join('')
      ret = temp.substr(0, position) + "," + temp.substr(position)
    else
      ret = temp

    input = ret






