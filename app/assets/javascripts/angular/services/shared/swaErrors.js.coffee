# State dictionary
#attr_accessible :abbreviation, :name


App.factory "swaErrors", ($rootScope)  ->

# Various error test to be shared between models

  isUnset: (ctrl) ->
    if ctrl == '' || ctrl is null
      return true

#  logObject: (debug, object, label) ->
#    if debug == true
#      label = label || "Result"
#      console.log label + " " + JSON.stringify(object, null, 4)
#
#  logString: (debug, string) ->
#    if debug == true
#      console.log string
#
#  alert: (debug, string) ->
#    if debug == true
#      alert string
