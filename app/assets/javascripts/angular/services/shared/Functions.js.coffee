# State dictionary
#attr_accessible :abbreviation, :name


App.factory "Functions", ($rootScope, SharedData)  ->
  astaffId = "JD"
  atotalPayment = 510
  aquoteGold = 12
  aquoteSilver = 1700
  aquotePlatinum = 165400
  atestService = "it works"

  formatDollar = (input) ->
    # Convert pennies to dollars
    output = input / 100
    #Will this need a comma separator
    if output > 999
      comma = true

    # Add decimal point, convert to string
    temp = output.toFixed(2).toString()

    # Add comma thousands separator
    if comma
      position = 1
      #ret = [temp.slice(0, position), ",", temp(position)].join('')
      ret = temp.substr(0, position) + "," + temp.substr(position)
    else
      ret = temp

    input = ret



  aBroadcastMetalQuoteChange: ->
    #alert "Broadcast triggered"
    $rootScope.$broadcast('MetalQuoteChange')

  a1BroadcastRecalcTotalPayment: ->
    #$rootScope.$broadcast('RecalcTotalPayment')

  astaffId: ->
    return staffId

  #gold quote in $/oz
  #20 dwt/troy oz
  aquoteGold: ->
    return quoteGold

  aquoteSilver: ->
    return quoteSilver

  aquotePlatinum: ->
    return quotePlatinum

  atotalPayment: ->
    return totalPayment

  atotalPaymentDisplay: ->
    return formatDollar(2345)
#    input = totalPayment
#    # Convert pennies to dollars
#    output = input / 100
#    #Will this need a comma separator
#    if output > 999
#      comma = true
#
#    # Add decimal point, convert to string
#    temp = output.toFixed(2).toString()
#
#    # Add comma thousands separator
#    if comma
#      position = 1
#      #ret = [temp.slice(0, position), ",", temp(position)].join('')
#      ret = temp.substr(0, position) + "," + temp.substr(position)
#    else
#      ret = temp
#
#    input = ret

  testService: ->
    return testService




