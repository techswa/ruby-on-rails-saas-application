# State dictionary
#attr_accessible :abbreviation, :name


App.factory "swaFn", ($rootScope)  ->

  #==========================================
  #  Returns the index of an item in an array
  #  This value is then used to select the
  #  the item to display
  #==========================================

  ArrayItemIndex: (sourceArray, foreign_key) ->
    idx = 0
    for option in sourceArray
      if option.id == foreign_key
        return idx
      idx++




  #=========================================
  #  Error Logging / Error Display Routines
  #=========================================

  logObject: (debug, object, label) ->
    if debug == true
      label = label || "Result"
      console.log label + " " + JSON.stringify(object, null, 4)

  logString: (debug, string) ->
    if debug == true
      console.log string

  alertString: (debug, string) ->
    if debug == true
      alert string

  alertObject: (debug, object, label) ->
    if debug == true
      alert JSON.stringify(object, null, 4)

