App.controller 'ModalInstanceCtrl', ['$scope', '$modalInstance', 'items', '$log','swaFn', ($scope, $rootScope, $modalInstance, items, $log, swaFn ) ->

  #==========================================
  #  Debug flags
  #==========================================
#  debug = true
#  swaFn.logString(debug, "Entering into ModalInstanceCtrl")

  $scope.items = items
  $scope.selected = item: $scope.items[0]
  $scope.ok = ->
   $modalInstance.close $scope.selected.item
   return

  $scope.cancel = ->
   $modalInstance.dismiss "cancel"
   return

  return
]