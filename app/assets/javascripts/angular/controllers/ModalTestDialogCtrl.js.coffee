App.controller 'ModalTestDialogCtrl', ['$scope', '$modalInstance', 'items','swaFn', ($scope, $modalInstance, items, swaFn) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into ModalTestDialogCtrl")

  $scope.items = items

  $scope.selected = item: $scope.items[0]

  $scope.ok = ->
    $modalInstance.close $scope.selected.item

  $scope.cancel = ->
    $modalInstance.dismiss "cancel"


]