App.controller 'QuoteCtrl', ['$scope','$q', '$modal', '$resource', 'QuoteModel', 'swaFn', ($scope, $q, $modal, $resource, QuoteModel, swaFn) ->

  debug = true



  $scope.UpdateQuoteDisplay = () ->
    $scope.displayAuPayout = QuoteModel.getPayout('Au')
    $scope.displayAgPayout = QuoteModel.getPayout('Ag')
    $scope.displayPtPayout = QuoteModel.getPayout('Pt')

  $scope.UpdateQuoteDisplay()

  #Listen for quote changes
  $scope.$on "QuoteChange",  ->
    $scope.UpdateQuoteDisplay()

  #called from main page
  $scope.quote = (metal) ->
    modalInstance = $modal.open(
      backdrop: 'static',
      keyboard: true,
      templateUrl: 'dialogs/quoteDialog.html',
      controller: 'QuoteDialogCtrl',
#      templateUrl: 'modalTest.html',
#      controller: 'ModalTestDialogCtrl',
      windowClass: 'modal quoteDialog'
      resolve:
        items: ->
          metal
    )
    modalInstance.result.then ((result) ->
      swaFn.logString(debug, "dialog return area")
      swaFn.logString(debug, "Dialog has returned: #{result}")
      # Update and Display Seller
      if result == false
        swaFn.logString(debug, "Changes have been discarded.")
      else
        swaFn.logString(debug, "Changes have been saved.")
        #displayRecord()

    ), ->
      #$log.info "Modal dismissed at: " + new Date()





  #Test saving quotes
  $scope.createQuote = () ->
    swaFn.logString(debug, "QuoteCtrl.createQuote")
    QuoteModel.createQuote()

  # Dialog definition
  dialogOptions =
    backdrop: true
    keyboard: false
    controller: "QuoteDialogCtrl"
    templateUrl: "dialogs/QuoteDialog.html"


#  #Create Seller Dialog function
  $scope.quoteDialogShow = () ->

    $dialog.dialog(angular.extend(dialogOptions,
      resolve:
        quotes: ->
          null
          #angular.copy $scope.quotes
    )).open().then (result) ->

      if result is true
        QuoteModel.BroadcastQuoteChange()
      else
        #Nothing needs to be done
]


