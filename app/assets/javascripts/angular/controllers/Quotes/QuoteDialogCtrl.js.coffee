App.controller 'QuoteDialogCtrl', ['$scope', '$q', 'swaFn', '$modal', '$modalInstance', 'items', 'QuoteModel', ($scope, $q, swaFn, $modal, $modalInstance, items, QuoteModel) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering QuoteDialogCtrl")

  #Used to get data structure
  #$scope.currentQuotes = QuoteModel.getQuotes()
  swaFn.logString(debug, "$scope.currentQuotes: " + $scope.currentQuotes)
  #Used to return in case dialog is cancelled
  $scope.undoQuotes = $scope.currentQuotes

  #===================
  # Form display
  # Set metal name
  # Set ng-show
  #===================

  #Hide metal divs
  setNgShow = (metal) ->
    $scope.AuShow = false
    $scope.AgShow = false
    $scope.PtShow = false
    if metal is 'Au'
      $scope.AuShow = true
    else if metal is 'Ag'
      $scope.AgShow = true
    else if metal is 'Pt'
      $scope.PtShow = true


  if items is 'Au'
    setNgShow('Au')
    $scope.metalName = 'Gold'
  else if items is 'Ag'
    setNgShow('Ag')
    $scope.metalName = 'Silver'
  else if items is 'Pt'
    setNgShow('Pt')
    $scope.metalName = "Platinum"

#  $scope.tabs = [
#    {title: "Gold", template: "partials/quoteGoldPartial.html", active: true, disabled: false},
#    {title: "Silver", template: "partials/quoteSilverPartial.html", disabled: false},
#    {title: "Platinum", template: "partials/quotePlatinumPartial.html", disabled: false}
#    ]

  $scope.updatedQuotes =
    au_spot: 0
    au_payout: 0
    ag_spot: 0
    ag_payout: 0
    pt_spot: 0
    pt_payout: 0

  #Load existing prices from QuoteModel service / replaced passed in quotes for no good reason??
  $scope.getCurrentQuotes = () ->
    $scope.updatedQuotes.auSpot = QuoteModel.getSpot('Au')
    $scope.updatedQuotes.auPayout = QuoteModel.getPayout('Au')
    $scope.updatedQuotes.auPercent = $scope.updatedQuotes.auPayout / $scope.updatedQuotes.auSpot
    $scope.updatedQuotes.agSpot = QuoteModel.getSpot('Ag')
    $scope.updatedQuotes.agPayout = QuoteModel.getPayout('Ag')
    $scope.updatedQuotes.agPercent = $scope.updatedQuotes.agPayout / $scope.updatedQuotes.agSpot
    $scope.updatedQuotes.ptSpot = QuoteModel.getSpot('Pt')
    $scope.updatedQuotes.ptPayout = QuoteModel.getPayout('Pt')
    $scope.updatedQuotes.ptPercent = $scope.updatedQuotes.ptPayout / $scope.updatedQuotes.ptSpot

  $scope.getCurrentQuotes()
  #Replace current quotes in QuoteModel wnhen changes are saved
  $scope.setCurrentQuotes = () ->
    QuoteModel.setSpot('Au', $scope.updatedQuotes.auSpot)
    QuoteModel.setPayout('Au', $scope.updatedQuotes.auPayout)
    QuoteModel.setSpot('Ag' ,$scope.updatedQuotes.agSpot)
    QuoteModel.setPayout('Ag', $scope.updatedQuotes.agPayout)
    QuoteModel.setSpot('Pt', $scope.updatedQuotes.ptSpot)
    QuoteModel.setPayout('Pt', $scope.updatedQuotes.ptPayout)


  # Cal if user cancels changes
  $scope.cancelChanges = () ->
    alert "All changes are being canceled"

  #Display routines
  AuSpotDisplayRefresh = ->
    $scope.auSpotDisplay =  ($scope.updatedQuotes.auSpot / 100).toFixed(2)
  AuPayoutDisplayRefresh = ->
    $scope.auPayoutDisplay = ($scope.updatedQuotes.auPayout / 100).toFixed(2)
  AuPercentDisplayRefresh = ->
    swaFn.logString(debug, "$scope.updatedQuotes.auPayout: " + $scope.updatedQuotes.auPayout)
    swaFn.logString(debug, "$scope.updatedQuotes.auSpot * 100 " + $scope.updatedQuotes.auSpot * 100)
    $scope.auPercentDisplay  =  ($scope.updatedQuotes.auPayout / $scope.updatedQuotes.auSpot * 100).toFixed(2)
    swaFn.logString(debug, "$scope.auPercentDisplay " + $scope.auPercentDisplay)

  AgSpotDisplayRefresh = ->
    $scope.agSpotDisplay =  ($scope.updatedQuotes.agSpot / 100).toFixed(2)
  AgPayoutDisplayRefresh = ->
    $scope.agPayoutDisplay = ($scope.updatedQuotes.agPayout / 100).toFixed(2)
  AgPercentDisplayRefresh = ->
    $scope.agPercentDisplay  =  ($scope.updatedQuotes.agPayout / $scope.updatedQuotes.agSpot * 100).toFixed(2)

  PtSpotDisplayRefresh = ->
    $scope.ptSpotDisplay =  ($scope.updatedQuotes.ptSpot / 100).toFixed(2)
  PtPayoutDisplayRefresh = ->
    $scope.ptPayoutDisplay = ($scope.updatedQuotes.ptPayout / 100).toFixed(2)
  PtPercentDisplayRefresh = ->
    $scope.ptPercentDisplay  =  ($scope.updatedQuotes.ptPayout / $scope.updatedQuotes.ptSpot * 100).toFixed(2)

  # Load default display
  AuSpotDisplayRefresh()
  AuPayoutDisplayRefresh()
  AuPercentDisplayRefresh()
  AgSpotDisplayRefresh()
  AgPayoutDisplayRefresh()
  AgPercentDisplayRefresh()
  PtSpotDisplayRefresh()
  PtPayoutDisplayRefresh()
  PtPercentDisplayRefresh()


  #Called when changing the spot price
  #Recalcs the payout based upon unchaged percent
  $scope.changeSpotEntry = (metal) ->

    if metal =='Au'
      #Set spot variable to integer value of input field
      $scope.updatedQuotes.auSpot = $scope.auSpotDisplay * 100 #Get raw number as integer
      #Calc new payout based upon th enew spot time current percentage
      $scope.updatedQuotes.auPayout = $scope.updatedQuotes.auPercent * $scope.updatedQuotes.auSpot
      AuPayoutDisplayRefresh() #only change the field that was recalced

    if metal =='Ag'
      $scope.updatedQuotes.agSpot = $scope.agSpotDisplay * 100 #Get raw number as integer
      $scope.updatedQuotes.agPayout = $scope.updatedQuotes.agPercent * $scope.updatedQuotes.agSpot
      AgPayoutDisplayRefresh()

    if metal =='Pt'
      $scope.updatedQuotes.ptSpot = $scope.ptSpotDisplay * 100 #Get raw number as integer
      $scope.updatedQuotes.ptPayout = $scope.updatedQuotes.ptPercent * $scope.updatedQuotes.ptSpot
      PtPayoutDisplayRefresh()

  #Called when changing the payout amount
  $scope.changePayoutEntry = (metal) ->
    if metal == 'Au'
      $scope.updatedQuotes.auPayout = $scope.auPayoutDisplay * 100 #Get raw number as integer
      AuPercentDisplayRefresh()

    if metal == 'Ag'
      $scope.updatedQuotes.agPayout = $scope.agPayoutDisplay * 100 #Get raw number as integer
      AgPercentDisplayRefresh()

    if metal == 'Pt'
      $scope.updatedQuotes.ptPayout = $scope.ptPayoutDisplay * 100 #Get raw number as integer
      PtPercentDisplayRefresh()

  #Called when changing the percentage
  $scope.changePercentEntry = (metal) ->
    #swaFn.logString(debug, "Key pressed for " + metal
    if metal == 'Au'
      #Save change to raw varable - remove decimal
      $scope.updatedQuotes.auPercent = $scope.auPercentDisplay / 100 #$scope.displayau_percent.replace(".","")
      #Figure gold payout
      $scope.updatedQuotes.auPayout = $scope.updatedQuotes.auSpot * $scope.updatedQuotes.auPercent
      #display updated and formatted gold payout amount
      AuPayoutDisplayRefresh()

    if metal == 'Ag'
        #Save change to raw varable - remove decimal
      $scope.updatedQuotes.agPercent = $scope.agPercentDisplay / 100 #$scope.displayag_percent.replace(".","")
      #Figure gold payout
      $scope.updatedQuotes.agPayout = $scope.updatedQuotes.agSpot * $scope.updatedQuotes.agPercent
      #display updated and formatted gold payout amount
      AgPayoutDisplayRefresh()

    if metal == 'Pt'
        #Save change to raw varable - remove decimal
      $scope.updatedQuotes.ptPercent = $scope.ptPercentDisplay / 100 #$scope.displaypt_percent.replace(".","")
      #Figure gold payout
      $scope.updatedQuotes.ptPayout = $scope.updatedQuotes.ptSpot * $scope.updatedQuotes.ptPercent
      #display updated and formatted gold payout amount
      PtPayoutDisplayRefresh()



      #configure dialog box
  $scope.calcOptions = [
    {
      id:0,
      legend:"Percent"
    },
    {
      id:1,
      legend:"Manual"
    }
  ]
#  #Used to hold quoteAuto selection set to default values
  $scope.setCalcMethodDefaults = ->
    #Set dialog select value
    $scope.calcMethodAu = $scope.calcOptions[1];
    $scope.calcMethodAg = $scope.calcOptions[1];
    $scope.calcMethodPt = $scope.calcOptions[1];
    $scope.calcAuPercent = true
    $scope.calcAuManual = false
    $scope.calcAgPercent = true
    $scope.calcAgManual = false
    $scope.calcPtPercent = true
    $scope.calcPtManual = false
#    #alert "$scope.calcPercent: " + $scope.calcPercent + " $scope.calcManual: " + $scope.calcManual
#
#  #Cal defaults settings
  $scope.setCalcMethodDefaults()
#
  $scope.setCalcMethod = (metal) ->
    # toggle calc settings when called
    if metal == 'Au'
      $scope.calcAuPercent =! $scope.calcAuPercent
      $scope.calcAuManual =! $scope.calcAuManual
    if metal == 'Ag'
      $scope.calcAgPercent =! $scope.calcAgPercent
      $scope.calcAgManual =! $scope.calcAgManual
    if metal == 'Pt'
      $scope.calcPtPercent =! $scope.calcPtPercent
      $scope.calcPtManual =! $scope.calcPtManual

  # dialog buttons
  $scope.save = ->
    #update changes
    $scope.setCurrentQuotes()
    #Save to server
    #QuoteModel.createQuote()
    $scope.result = true
    $modalInstance.close $scope.result
#
  $scope.close = ->
    #swaFn.logString(debug, "Dialog close button clicked"
    #'Rollback any changes'
    # $scope.cancelChanges()
    $scope.result = false
    $modalInstance.close $scope.result
#
#

]