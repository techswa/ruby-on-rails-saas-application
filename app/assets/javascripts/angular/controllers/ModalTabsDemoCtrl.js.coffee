App.controller 'ModalTabsDemoCtrl', ['$scope','swaFn', ($scope, swaFn) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into ModalTabsDemoCtrl")

  $scope.tabs = [
    title: "Dynamic Title 1"
    content: "Dynamic content 1"
    disabled: false
  ,
    title: "Dynamic Title 2"
    content: "Dynamic content 2"
    disabled: false
  ]
  $scope.alertMe = ->
    setTimeout ->
       alert "You've selected the alert tab!"


  $scope.navType = "pills"


]