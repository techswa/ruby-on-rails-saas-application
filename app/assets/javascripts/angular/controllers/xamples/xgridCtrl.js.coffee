App.controller 'xgridCtrl', ['$scope','swaFn','ItemModel', ($scope, swaFn, ItemModel) ->

  $scope.myData = [{name: "Moroni", age: 50},
    {name: "Teancum", age: 43},
    {name: "Jacob", age: 27},
    {name: "Nephi", age: 29},
    {name: "Enos", age: 34}]

  $scope.aData = ItemModel.getItems()

  swaFn.logObject(true, $scope.aData, "aData")

  $scope.testGrid = { data: 'aData' }

]