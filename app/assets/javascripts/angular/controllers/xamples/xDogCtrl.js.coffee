App.controller 'xDogCtrl', ['$scope', '$rootScope', '$q', 'dialog', 'item', 'swaFn', 'SellerModel', 'qryStates', 'qryLicenses','qryGenders','qryEyeColors', ($scope, $rootScope, $q, dialog, item, swaFn, SellerModel, qryStates, qryLicenses, qryGenders, qryEyeColors) ->

   debug = false

   #Items are stored in ItemModel Service
 #  $scope.Items = ItemModel.getItems()
   swaFn.logString(debug,"Entering ItemsCtrl")
   $scope.ItemModel = ItemModel

  # $scope.sharedDataItem = SharedData

   displayObject = (object, label) ->
     label = label || "Object value"
     swaFn.logObject(debug, object, label)


   displayObject(ItemModel.getItems(),'Current items')
   swaFn.logString(debug, "Sum total of selected items = " + ItemModel.getItemsTotalWeightByMetalId(2).toString())


   # Dialog definition
   dialogOptions =
     controller: "ItemDialogCtrl"
     templateUrl: "dialogs/itemDialog.html"

   #New item Dialog
   $scope.newRow = () ->
     swaFn.logString(debug, "Add new item")
     #Get new item with next id from ItemModel for for editing
     newItem = ItemModel.newItem()
     swaFn.logObject(debug, newItem, "Item to edit")

     #Display dialog
     $dialog.dialog(angular.extend(dialogOptions,
       resolve:
         item: ->
           angular.copy newItem #, itemToEdit  #{id:'2',item:'Software',description:'Google sucks'}
       #angular.copy itemToEdit
     )).open().then (Item) ->

       #Did user save or cancel edit
       if Item == 'false'
        swaFn.logObject(debug, Item, "User canceled new record")

       else
         swaFn.logString(debug,"User wants to create new record")
         swaFn.logObject(debug, Item, "Returned value from dialog")
         #Call create in Model
         result = ItemModel.createItem(Item)


         #Dialog function
   $scope.editRow = (item) ->
       swaFn.logString(debug, 'Entered edit row with: ' + item.id)

       $dialog.dialog(angular.extend(dialogOptions,
         resolve:
           item: ->
             angular.copy item
       )).open().then (item) ->
         swaFn.logObject(debug, item, "returned result")

         #Did user save or cancel edit
         if item == 'false'
           swaFn.logString(debug, "User canceled update")
           swaFn.logObject(debug, item, "Returned value from dialog")
         else
           swaFn.logString(debug, "User wants to save changes.")
           swaFn.logObject(debug, item, "Returned value from dialog")
           #Call create in Model
           result = ItemModel.updateItem(item)


    #Replaced in Model with deleteItem()
#   #Delete slected row                            mn
   $scope.deleteRow = (row) ->
     #console.log "Selected record for deletion " + JSON.stringify(row, null, 4)
     #console.log "Record id is: " + JSON.stringify(row.id, null, 4)
     ItemModel.deleteItem(row.id)
     #Call deleteion method in Model
     #ItemModel.deleteItem(row.id)


   # Define grid for display - Pull direct from the Model

   $scope.ItemsGrid = {data: 'ItemModel.getItems()',
   enableRowSelections: false,
   multiSelect: false,
   keepLastSelected: false,
   enableSorting: false,
   selectedItems: [],
   afterSelectionChange: ->
   #  console.log 'afterSelectionChanged:fired'
     if $scope.ItemsGrid.selectedItems[0] is undefined
       console.log 'Selection is empty'
    # else console.log 'Selected item is: ' + $scope.ItemsGrid.selectedItems[0] #$scope.editRow($scope.ItemsGrid.selectedItems[0])

   columnDefs:[{
     field: 'product',
     displayName: 'Item',
     #beforeSelectionChange: NoSelection(),
     cellFilter: '',
     cellTemplate: '',
     enableCellEdit: false,
     editableCellTemplate: '',
     width: 300
   },{
     field: 'gender',
     displayName: 'Gender',
     cellFilter: '',
     cellTemplate: '<div class="cell-align-center">{{row.getProperty(col.field)}}</div>',
     enableCellEdit: false,
     editableCellTemplate: '',
     width: 65
   },{
     field: 'metal_name',
     displayName: 'Metal',
     cellFilter: '',
     cellTemplate: '<div class="cell-align-right">{{row.getProperty(col.field)}}</div>',
     enableCellEdit: false,
     editableCellTemplate: '',
     width: 65
   },{
     field: 'weightSummary',
     displayName: 'Weight',
     cellFilter: '',
     cellTemplate: '<div class="cell-align-right">{{row.getProperty(col.field)}}</div>',
     enableCellEdit: false,
     editableCellTemplate: '',
     width: 95
   },{
     field: 'sizeSummary',
     displayName: 'Size',
     cellFilter: '',
     cellTemplate: '<div class="cell-align-right">{{row.getProperty(col.field)}}</div>',
     enableCellEdit: false,
     editableCellTemplate: '',
     width: 65
   },{
     field: 'payment',
     displayName: 'Payment',
     cellFilter: 'formatDollar',
     cellTemplate: '<div class="cell-align-right">{{row.getProperty(col.field) | formatDollar}}</div>',
     enableCellEdit: false,
     editableCellTemplate: '',
     width: 95
   },{
     field: 'description',
     displayName: 'Description',
     cellFilter: '',
     cellTemplate: '',
     enableCellEdit: false,
     editableCellTemplate: ''
   }
   ,{
     field: '',
     displayName: '',
     cellFilter: '',
     cellTemplate: '<span class="icon-edit"></span><button class="btn btn-small btn-link" ng-click="editRow(row.entity)">Edit</button><span class="icon-remove"></span><button class="btn btn-small btn-link" ng-click="deleteRow(row.entity)">Delete</button>',
     enableCellEdit: false,
     editableCellTemplate: ''
   }
   ]};

   #Display initial totals
  #RecalculatePaymentTotal()

] #Keep this - end of app

