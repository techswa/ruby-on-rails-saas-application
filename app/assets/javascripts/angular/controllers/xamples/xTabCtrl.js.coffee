App.controller 'xTabCtrl', ['$scope',($scope)->
  #Test controller for tab.html
  #Used to prototype tabs with ui angular

  $scope.tabs = [
   {title: "Gold", template: "partials/quoteGoldPartial.html", disabled: false},
   {title: "Silver", template: "partials/quoteSilverPartial.html", disabled: false},
   {title: "Platinum", template: "partials/quotePlatinumPartial.html", disabled: false}
   ]

  $scope.alertMe = ->
    setTimeout ->
      alert "You've selected the alert tab!"


  $scope.navType = "pills"

]

