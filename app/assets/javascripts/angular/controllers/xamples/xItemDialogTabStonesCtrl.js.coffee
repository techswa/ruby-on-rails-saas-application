App.controller 'xSellerDialogTabStonesCtrl', ['$scope', '$rootScope', '$q', 'swaFn', 'ItemModel', ($scope, $rootScope, $q, swaFn, SellerModel) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into ItemDialogTabStonesCtrl")

  #==========================================
  #  Load external routines / libraries
  #==========================================
  ArrayItemIndex = (array, selection) =>
    swaFn.ArrayItemIndex(array, selection)

  #==========================================
  #  Define arrays and structures needed
  #==========================================
  #Create JSON to hold display data
  $scope.model = {}
  #Create JSON to hold errors
  $scope.error = {}



  #==========================================
  #load current record data from the model
  #==========================================
  getModelRecord= () ->
    $scope.model.state_id = $scope.States[ArrayItemIndex($scope.States, SellerModel.getFieldValueTemp('state_id'))]
    $scope.model.license_id = $scope.Licenses[ArrayItemIndex($scope.Licenses, SellerModel.getFieldValueTemp('license_id'))]
    $scope.model.number = SellerModel.getFieldValueTemp('number')
    $scope.model.month = $scope.Months[ArrayItemIndex($scope.Months, SellerModel.getFieldValueTemp('month'))]
    $scope.model.day = $scope.Days[ArrayItemIndex($scope.Days, SellerModel.getFieldValueTemp('day'))]
    $scope.model.year = SellerModel.getFieldValueTemp('year')

  #========================================================
  #  Save field changes to the model temp & display fields
  #========================================================
  $scope.setModelRecord=(selectCtrl)->
    #setFieldDisplay: (field, string)
    if selectCtrl == 'state'
      SellerModel.setFieldValueTemp('state_id', $scope.model.state_id.id)
      SellerModel.setFieldDisplayTemp('state_id', $scope.model.state_id.abbreviation)

    if selectCtrl == 'license'
      SellerModel.setFieldValueTemp('license_id', $scope.model.license_id.id)
      SellerModel.setFieldDisplayTemp('license_id', $scope.model.license_id.abbreviation)

    if selectCtrl == 'number'
      SellerModel.setFieldValueTemp('number', $scope.model.number)
      SellerModel.setFieldDisplayTemp('number', $scope.model.number)

    if selectCtrl == 'month'
      SellerModel.setFieldValueTemp('month', $scope.model.month.id)
      SellerModel.setFieldDisplayTemp('month', $scope.model.month.id)

    if selectCtrl == 'day'
      SellerModel.setFieldValueTemp('day', $scope.model.day.id)
      SellerModel.setFieldDisplayTemp('day', $scope.model.day.id)

    if selectCtrl == 'year'
      SellerModel.setFieldValueTemp('year', $scope.model.year)
      SellerModel.setFieldDisplayTemp('year', $scope.model.year)

#  #==========================================
#  # Load curent error data from the model
#  #==========================================
#  #Error flag used by ng-class in control
#  getModelErrors= () ->
#    $scope.error.state_id = SellerModel.getFieldError('state_id')
#    $scope.error.license_id = SellerModel.getFieldError('license_id')
#    $scope.error.number = SellerModel.getFieldError('number')
#    $scope.error.month = SellerModel.getFieldError('month')
#    $scope.error.day = SellerModel.getFieldError('day')
#    $scope.error.year = SellerModel.getFieldError('year')


  #=============================================
  #  Listen for any messages
  #=============================================
  #Listen for quote changes
  $scope.$on "refreshTempData()",  ->
    #$scope.refreshData()

  #=============================================
  #  Send any messages
  #=============================================
  #Not in use



#  #=============================================
#  #  Bootstrap controller code
#  #=============================================
#  bootstrapController = () ->
#
#    $q.all([
#      qryStates.Query(),
#      qryLicenses.Query(),
##       SellerModel.getStates()
##       SellerModel.getLicenses()
#
#
#    ]).then (result) ->
#      $scope.States = result[0]
#      $scope.Licenses = result[1]
#
#      MonthsCreate()
#      DaysCreate()
#
#      #Load edit data into form controls
#      getModelRecord()
#
#    #  $scope.States = SellerModel.getStates()
#    #  $scope.Licenses = SellerModel.getLicenses()
#
#  #Bootstrap controller
#  bootstrapController()


]