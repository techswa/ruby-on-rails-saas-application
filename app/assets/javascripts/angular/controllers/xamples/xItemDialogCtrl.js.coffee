App.controller 'xItemDialogCtrl', ['$scope', '$rootScope', '$q', '$dialog', '$filter', 'swaFn', 'MetalModel', 'QuoteModel', 'ItemModel', 'qryGenders', 'qryMetals', 'qryProducts', 'qryProductCategories', 'qryStoneClarities', 'qryStoneColors', 'qryStoneCuts', 'qryStoneSizes', 'qryStoneTypes', 'qryUnitWeights', 'qryUnitSizes', ($scope, $rootScope, $q, $dialog, $filter, swaFn, MetalModel, QuoteModel, ItemModel, qryGenders, qryMetals, qryProducts, qryProductCategories, qryStoneClarities, qryStoneColors, qryStoneCuts, qryStoneSizes, qryStoneTypes, qryUnitWeights, qryUnitSizes) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into ItemDialogCtrl")

  #swaFn.logObject(debug, item, "Entering into ItemDialogCtrl Edit")
  #==========================================
  #  Load external routines / libraries
  #==========================================
  getTabActive = ->
    #ItemModel.getTabActive()
    alert ""

  #Set active tab
  $scope.setTabActive = (tab) ->
    ItemModel.setTabActive(tab)
  #==============================================
  #  Determine operation to perform New or EDIT
  #==============================================

#  #Edit existing item
#  if item.op == 'EDIT'
#    #$scope.item = item  #single item
#    $scope.dialogTitle = "Edit existing item"
#    # Reset the buffer to null
#    ItemModel.clearBuffer()
#    # Load the selected row into the buffer for editing
#    ItemModel.loadBuffer(item.row)
#
#  else
#    #Add new item
#   # $scope.item = ItemModel.getNewItem()
#    $scope.dialogTitle = "Enter new item"
#    swaFn.logString(debug, "NEW Item called in ItemDialog")
#    # Reset the buffer to null
#    ItemModel.clearBuffer()
#    # Create clear buffer to use


  #==========================================
  #  Configure tab controller
  #==========================================
  $scope.tabs = [
    {id: 0, title: "General", template: "partials/itemGeneralPartial.html", disabled: false, checkErrors: "checkErrors(0)"},
    {id: 1, title: "Stones", template: "partials/itemStonesPartial.html", disabled: true, checkErrors: "checkErrors(1)"},
    {id: 2, title: "Details", template: "partials/itemDetailsPartial.html", disabled: true, checkErrors: "checkErrors(2)"}
    ]

  #=============================================
  #  Broadcast changes
  #=============================================
  BroadcastCheckTabErrors = ->
    $rootScope.$broadcast('CheckTabErrors')


  #==========================================
  # Check errors test
  #==========================================
#  $scope.checkErrors = (tabId) ->
#
#    #swaFn.alertObject(debug, $scope.tabs, "tab")
#   # swaFn.alertString(debug, tabid, "Selected")
#
#    alert "Active tab " + getActiveTab()
#    #Set active tab
#    #ItemModel.setTabActive(getActiveTab())
#
#    # Call check errors in model
#    #ItemModel.checkErrors($scope.tabs)
#
#  getActiveTab = ->
#    for k,v of $scope.tabs
#      if v.active is true
#        # Skip if current tab
#        # Bug retruns both current and next tab
#        if ItemModel.getActiveTab() isnt v.id
#          # Isnt current tab
#          return v.id

  #==========================================
  #  Process dialog exit
  #==========================================

  #Processes next step
  $scope.next = ->
    #Get current tab for error check
    currTab = getTabActive()
    alert currTab
    #Send message to tab controller to check for errors
    BroadcastCheckTabErrors()



#  $scope.save = ->
#    # Copy temp values into value field
#    ItemModel.saveBuffer()
#    dialog.close true
#
#
#  $scope.cancel = ->
#    swaFn.logString(debug, "Dialog close button clicked")
#    dialog.close false



    # All queries are now operformed at the tab level
  # Load foreign_keys with promises
  LoadForeignKeys = () ->
#    console.log "LoadForeignKeys"
    $q.all([
      qryGenders.Query(),
      qryProducts.Query(),
      qryProductCategories.Query(),
      qryMetals.Query(),
      qryStoneClarities.Query(),
      qryStoneColors.Query(),
      qryStoneCuts.Query(),
      qryStoneSizes.Query(),
      qryStoneTypes.Query(),
      qryUnitWeights.Query(),
      qryUnitSizes.Query()

    ]).then (result) ->
      $scope.Genders = result[0]
      #      console.log "result-Genders: " + JSON.stringify(result[0], null, 4)
      $scope.Products = result[1]
      $scope.ProductCategories = result[2]
      $scope.Metals = result[3]
      $scope.StoneClarities = result[4]
      $scope.StoneColors = result[5]
      $scope.StoneCuts = result[6]
      $scope.StoneSizes = result[7]
      $scope.StoneTypes = result[8]
      $scope.UnitWeights = result[9]
      $scope.UnitSizes = result[10]

      swaFn.logString(debug, $scope.Products)
      #Configure option defaults after foreign keys have been loaded
      #if new record (id=0) don't set defaults
      #alert 'ItemDialogCtrl - Setting object defaults : ' + String($scope.item.id)

      #Set dialogdefaults based upon operation
      #New records id is set to null for indentification
      if $scope.item.id isnt null
        #console.log "setting defaults 'EDIT' "
        SetObjectDefaults('EDIT')
      else
        #console.log "setting defaults 'NEW' "
        SetObjectDefaults('NEW')

      ret = 0

  #Load foreign keys
  #Waits for promses to be fulfilled
  LoadForeignKeys()


  #Calculate value of metals
  $scope.calcItemValue1 = (item) ->
    swaFn.logObject(debug, item ,"calc item value")

    #reset value to zero
    value = 0
    #Get the details for the selected metal

    metalDetails = $scope.getMetalDetails(item.metal_id)
    swaFn.logObject(debug, metalDetails,"Metal details")

    metalBase = metalDetails.base
    metalPurity = metalDetails.purity
    metalWeight = item.weight
    #get factor for value
    weightFactor = getWeightFactor(item.unitWeight_id)
    swaFn.logString(debug, "base metal " + metalBase)
    #If metal is gold
    if metalBase == 'Au'
      value = $scope.calcGoldValue(metalPurity, metalWeight, weightFactor)
      #alert "item value: " + value
    #if metal is silver
    if metalBase == 'Ag'
      value = $scope.calcSilverValue(metalPurity, metalWeight, weightFactor)
    #if metal is platinum
    if metalBase == 'Pt'
      value = $scope.calcPlatinumValue(metalPurity, metalWeight, weightFactor)

    swaFn.logString(debug, "Calc cvalue is : " + value)
    return value

  #Determine what unit is being us and return factor for value
  # as in how many units per troy ounce
  getWeightFactor = (unitWeight_id) ->
    for unitWeight in $scope.UnitWeights
      if unitWeight.id == unitWeight_id
        return unitWeight.factor

  #Load details regarding a metal
  $scope.getMetalDetails = (metal_id) ->
    for metal in $scope.Metals
      if metal.id == metal_id
        return metal


#  $scope.calcGoldValue = (purity, weight, factor) ->
#    value = QuoteModel.getPayout('Au') * purity * weight * factor / 1000
#    return value
#
#  $scope.calcSilverValue = (purity, weight, factor) ->
#    value = QuoteModel.getPayout('Ag') * purity * weight * factor / 1000
#    return value
#
#  $scope.calcPlatinumValue = (purity, weight, factor) ->
#    value = QuoteModel.getPayout('Pt') * purity * weight * factor / 1000
#    return value


  # Source data dor product select filterd by Category
  $scope.filteredProducts = (input) ->
    console.log "filtered: " + input
    return




  #Set Option Default in ng-option control
  #Sets edit form defaults to match actual record
  OptionByForeignKey = (optionCtrl, foreign_key) ->
    #Find record in array and return index
    idx = 0
    for option in optionCtrl
      if option.id == foreign_key
        return idx
      idx++


  #Set select item defaults
  # EDIT or NEW is passed in as op
  SetObjectDefaults = (op) ->

#    if op == "NEW"
#      console.log "Setting new defaults"
#      #Set so that there aren't any errors
#      #Weight
#      $scope.weight = 0
#      #UnitWeight
#      $scope.unitWeight = $scope.UnitWeights[2]
#      #$scope.unitWeight = 1
#
#    #Set controls to the appropraite selection
#    if op == 'EDIT'
#      $scope.gender = $scope.Genders[OptionByForeignKey($scope.Genders, $scope.item.gender_id)]
#
#      #Product Category
#  #    #$scope.productCategory = $scope.ProductCategories[(parseInt($scope.item.productCategory_id)-1)]
#      $scope.productCategory = $scope.item.productCategory_id #$scope.ProductCategories[OptionByForeignKey($scope.ProductCategories, $scope.item.productCategory_id)]
#      #Product
#      #$scope.product = $scope.Products[(parseInt($scope.item.product_id)-1)]
#      $scope.product = $scope.Products[OptionByForeignKey($scope.Products, $scope.item.product_id)]
#      #Metal
#      #$scope.metal = $scope.Metals[(parseInt($scope.item.metal_id)-1)]
#      $scope.metal = $scope.Metals[OptionByForeignKey($scope.Metals, $scope.item.metal_id)]
#      #console.log 'Looking for record #2'
#      #SetOptionDefault($scope.Metals, 2)
#      #Weight
#      $scope.weight = $scope.item.weight
#      #UnitWeight
#      #$scope.unitWeight = $scope.UnitWeights[(parseInt($scope.item.unitWeight_id)-1)]
#      $scope.unitWeight = $scope.UnitWeights[OptionByForeignKey($scope.UnitWeights, $scope.item.unitWeight_id)]
#      #Payment
#      $scope.payment = $scope.item.payment
#      #Size
#      $scope.size = $scope.item.size
#      #UnitSize
#      #$scope.unitSize = $scope.UnitSizes[(parseInt($scope.item.unitSize_id)-1)]
#      $scope.unitSize = $scope.UnitSizes[OptionByForeignKey($scope.UnitSizes, $scope.item.unitSize_id)]
#      #Description
#      $scope.description = $scope.item.description
#      #Engraving
#      $scope.engraving = $scope.item.engraving
#      #Hallmark
#      $scope.hallmark = $scope.item.hallmark
#      #Comments
#      $scope.comments = $scope.item.comments

  #Create fields for display when user makes a change
  #weightSummary
  WeightSummary = () ->
    $scope.item.weightSummary = $scope.item.weight + ' ' + $scope.unitWeight.abbreviation

  # sizeSummary
  SizeSummary = () ->
    $scope.item.sizeSummary = $scope.item.size + ' ' + $scope.unitSize.abbreviation


#   # Update shared object with any edits
#  $scope.UpdateItemObject1 = (selectCtrl) ->
#    alert 'Control passed into UpdateItemObject(): ' + selectCtrl
#    alert 'Control id : ' + $scope.metal.id
#    #Gender
#    if selectCtrl == 'gender'
#      alert 'Control id : ' + $scope.gender.id
#      $scope.item.gender_id = $scope.gender.id
#      $scope.item.gender = $scope.gender.abbreviation
#      swaFn.logObject(debug, $scope.item, "item object")
#    #ProductCategory
#    if selectCtrl == 'productCategory'
#      $scope.item.productCategory_id  = $scope.productCategory.id
##    #Product
#    if selectCtrl == 'product'
#      $scope.item.product = $scope.product.name
#      $scope.item.product_id  = $scope.product.id
##
#    #Metal
#    if selectCtrl == 'metal'
#      alert JSON.stringify($scope.metal, null, 4)
#      $scope.item.metal_id = $scope.metal.id
#      $scope.item.metal_base = $scope.metal.base
#      $scope.item.metal_name = $scope.metal.abbreviation
#      $scope.item.metal_purity = $scope.metal.purity
#
#      #Calculate value of item
#      $scope.payment= $scope.calcItemValue($scope.item)
#      #Update item for grid display
#      $scope.item.payment = $scope.payment
#
##    #Weight
#    if selectCtrl == 'weight'
#      $scope.item.weight = $scope.weight
#      #Create weight summary field
#      WeightSummary()
#      #Recalculate value of item
#      $scope.payment = $scope.calcItemValue($scope.item)
#      #Update item for grid display
#      $scope.item.payment = $scope.payment
#
#
#      #UnitWeight
#    if selectCtrl == 'unitWeight'
#      $scope.item.unitWeight_id = $scope.unitWeight.id
#      WeightSummary()
#      #Calculate value of item
#      $scope.payment = $scope.calcItemValue($scope.item)
#      #Update item for grid display
#      $scope.item.payment = $scope.payment
#
##    #Size
#    if selectCtrl == 'size'
#      $scope.item.size = $scope.size
#      #sizeSummary display update
#      SizeSummary()
##    #UnitSize
#    if selectCtrl == 'unitSize'
#      $scope.item.unitSize_id = $scope.unitSize.id
#      #sizeSummary display update
#      SizeSummary()
##    #Description
#    if selectCtrl == 'description'
#      $scope.item.description = $scope.description
##    #Engraving
#    if selectCtrl == 'engraving'
#      $scope.item.engraving = $scope.engraving
##    #Hallmark
#    if selectCtrl == 'hallmark'
#       $scope.item.hallmark = $scope.hallmark
##    #Comments
#    if selectCtrl == 'comments'
#      $scope.item.comments = $scope.comments
#    #console.log $scope.item
#


# Original code below
#  $scope.save = ->
#    swaFn.logObject(debug, $scope.item, "Dialog result being passed back to caller:")
#    dialog.close $scope.item
#
#
#  $scope.close = ->
#    dialog.close 'false'







]