App.controller 'xItemDialogTabDetailsCtrl', ['$scope', '$rootScope', '$q', 'swaFn', 'ItemModel', ($scope, $rootScope, $q, swaFn, ItemModel) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into ItemDialogTabDetailsCtrl")

  #==========================================
  #  Load external routines / libraries
  #==========================================
  ArrayItemIndex = (array, selection) =>
    swaFn.ArrayItemIndex(array, selection)

  #load record buffer value
  getBufferValue = (Key) ->
    return ItemModel.getBufferValue(Key)

  setbufferValue = (Key, Value) ->
    ItemModel.setBufferValue(Key, Value)

  #==========================================
  #  Define arrays and structures needed
  #==========================================
  #Create JSON to hold display data
  $scope.buffer = {}

  #Ceated to hold select control returned data
  $scope.select = {}

  #Create JSON to hold errors
  $scope.error = {}

  #==========================================
  #  Load Buffer record to edit
  #==========================================
  #Display data to edit
  loadBufferData = () ->
    # Link to Model buffer
    $scope.buffer = ItemModel.getBuffer()

  #========================================================
  #  Process any click events / Update dependant fields
  #  Called from ng-change in html
  #========================================================

  $scope.updateBuffer=(selectCtrl)->
    #setFieldDisplay: (field, string)
    if selectCtrl == 'test123'
      return
      #SellerModel.setFieldValueTemp('state_id', $scope.model.state_id.id)
      #SellerModel.setFieldDisplayTemp('state_id', $scope.model.state_id.abbreviation)



  #==========================================
  # Load curent error data from the model
  #==========================================
  #Error flag used by ng-class in control
  getModelErrors= () ->
    $scope.error.state_id = SellerModel.getFieldError('state_id')
    $scope.error.license_id = SellerModel.getFieldError('license_id')
    $scope.error.number = SellerModel.getFieldError('number')
    $scope.error.month = SellerModel.getFieldError('month')
    $scope.error.day = SellerModel.getFieldError('day')
    $scope.error.year = SellerModel.getFieldError('year')


  #=============================================
  #  Bootstrap controller code
  #=============================================
  bootstrapController = () ->

    # Load buffer data
    loadBufferData()



  #Bootstrap controller
  bootstrapController()


]