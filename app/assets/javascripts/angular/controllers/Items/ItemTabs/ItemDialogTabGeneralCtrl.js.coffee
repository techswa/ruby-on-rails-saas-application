App.controller 'ItemDialogTabGeneralCtrl', ['$scope', '$rootScope', '$q', 'swaFn', 'ItemModel', 'MetalModel', 'QuoteModel', 'qryGenders', 'qryMetals', 'qryProducts', 'qryProductCategories', 'qryUnitWeights', 'qryUnitSizes', ($scope, $rootScope, $q, swaFn, ItemModel, MetalModel, QuoteModel,  qryGenders, qryMetals, qryProducts, qryProductCategories, qryUnitWeights, qryUnitSizes) ->

  #==========================================
  #  Controller overview
  #
  #  This controller manages the general section
  #  of the Item details form.
  #  It can only edit the data in the form elements
  #  that corrispond to this section.
  #  It loads its buffer values based upon the already selected item
  #  Any edits are saved when the user selected the Next button
  #  the next is only shown after all selections have been made
  #==========================================

  #$scope.isMyError = _itemGeneral.selectGender.$invalid
  #_itemGeneral.selectGender.$invalid
  #selectGender.$invalid=true
  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into ItemDialogTabGeneralCtrl")

  #==========================================
  #  Load external routines / libraries
  #==========================================
  ArrayItemIndex = (array, selection) =>
    swaFn.ArrayItemIndex(array, selection)

  #load record buffer value
  getBufferValue = (Key) ->
    return ItemModel.getBufferValue(Key)

  #Set error to true on field
  setError = (field) ->
    ItemModel.setError(field)


  #==========================================
  # Listen for messages
  #==========================================
#  $scope.$on "CheckTabErrors", ->
#    #for k,v of data
#      swaFn.alertString(debug, "Checking for errors inside of tab")



  #==========================================
  #  Define arrays and structures needed
  #==========================================
  #Create JSON to hold display data
  $scope.buffer = {}

  #Ceated to hold select control returned data
  $scope.select = {}

  #Create JSON to hold errors
  $scope.error = {}

  #==========================================
  #  Load Buffer record to edit
  #==========================================
  #Display data to edit
  loadBufferData = () ->
    # Link to Model buffer
    $scope.buffer = ItemModel.getBuffer()

  #==========================================
  # Initialize select controls
  #==========================================
  initSelectControls = () ->
    # Only process for existing records
    if $scope.buffer.id is null || $scope.buffer.id is undefined
        return
    else
      # Used for select fields
      $scope.select.gender =  $scope.Genders[ArrayItemIndex($scope.Genders, getBufferValue('gender_id') )]
      $scope.select.productCategory = $scope.ProductCategories[ArrayItemIndex($scope.ProductCategories, getBufferValue('productCategory_id') )].id
      $scope.select.product = $scope.Products[ArrayItemIndex($scope.Products, getBufferValue('product_id') )].id
      $scope.select.metal = $scope.Metals[ArrayItemIndex($scope.Metals, getBufferValue('metal_id') )]
      $scope.select.unitWeight = $scope.UnitWeights[ArrayItemIndex($scope.UnitWeights, getBufferValue('unitWeight_id') )]
      $scope.select.unitSize = $scope.UnitSizes[ArrayItemIndex($scope.UnitSizes, getBufferValue('unitSize_id') )]


 #=============================================
 #  Calculate common weight factor for metals
 #=============================================
  # Returns
  getWeightFactor = (unitWeight_id) ->
    for unitWeight in $scope.UnitWeights
      if unitWeight.id == unitWeight_id
        return unitWeight.factor

  #=============================================
  #  Return metal record details
  #=============================================
  $scope.getMetalDetails = (metal_id) ->
    for metal in $scope.Metals
      if metal.id == metal_id
        return metal

  #=======================================================
  #  Metal value calcs based on metal, weight & payout
  #=======================================================
  $scope.calcGoldValue = (purity, weight, factor) ->
    value = QuoteModel.getPayout('Au') * purity * weight * factor / 1000
    return value

  $scope.calcSilverValue = (purity, weight, factor) ->
    value = QuoteModel.getPayout('Ag') * purity * weight * factor / 1000
    return value

  $scope.calcPlatinumValue = (purity, weight, factor) ->
    value = QuoteModel.getPayout('Pt') * purity * weight * factor / 1000
    return value

  #=======================================================
  #  Calc value of item to be purchased
  #=======================================================
  $scope.calcItemValue = (item) ->
    swaFn.logObject(debug, item ,"calc item value")

    #reset value to zero
    value = 0
    #Get the details for the selected metal

    metalDetails = $scope.getMetalDetails(item.metal_id)
    swaFn.logObject(debug, metalDetails,"Metal details")

    metalBase = metalDetails.base
    metalPurity = metalDetails.purity
    metalWeight = item.weight

    #get factor for value
    weightFactor = getWeightFactor(item.unitWeight_id)
    swaFn.logString(debug, "base metal " + metalBase)

    #If metal is gold
    if metalBase == 'Au'
      value = $scope.calcGoldValue(metalPurity, metalWeight, weightFactor)

    #if metal is silver
    if metalBase == 'Ag'
      value = $scope.calcSilverValue(metalPurity, metalWeight, weightFactor)

    #if metal is platinum
    if metalBase == 'Pt'
      value = $scope.calcPlatinumValue(metalPurity, metalWeight, weightFactor)

    swaFn.logString(debug, "Calc cvalue is : " + value)
    return value

  #==========================================
  # Dependant field calculations
  #==========================================
  WeightSummary = () ->
    $scope.buffer.weightSummary = $scope.buffer.weight + ' ' + $scope.select.unitWeight.abbreviation

  # sizeSummary
  SizeSummary = () ->
    $scope.buffer.sizeSummary = $scope.buffer.size + ' ' + $scope.select.unitSize.abbreviation

  #==========================================
  # Edit existing item
  #==========================================
  editItem = ->
    #Load blank record
    $scope.buffer = ItemModel.getEditItem()


  #==========================================
  # Create item
  #==========================================
  createItem = ->
    #Load blank record
    $scope.buffer = ItemModel.getEditItem()

  #==========================================
  # Load current record from buffer into form
  #==========================================
  loadBufferRecord= () ->
    # Only load if existing record
#    if $scope.buffer.id is not null
#      $scope.buffer.state_id = $scope.States[ArrayItemIndex($scope.States, ItemModel.getFieldValueTemp('state_id'))]
#      $scope.buffer.license_id = $scope.Licenses[ArrayItemIndex($scope.Licenses, ItemModel.getFieldValueTemp('license_id'))]
#      $scope.buffer.number = ItemModel.getFieldValueTemp('number')
#      $scope.buffer.month = $scope.Months[ArrayItemIndex($scope.Months, ItemModel.getFieldValueTemp('month'))]
#      $scope.buffer.day = $scope.Days[ArrayItemIndex($scope.Days, ItemModel.getFieldValueTemp('day'))]
#      $scope.buffer.year = ItemModel.getFieldValueTemp('year')


  #========================================================
  #  Process any click events / Update dependant fields
  #  Called from ng-change in html
  #========================================================

  $scope.UpdateBuffer = (selectCtrl) ->

    #Gender
    if selectCtrl == 'gender'

      $scope.buffer.gender_id = $scope.select.gender.id
      $scope.buffer.gender =  $scope.select.gender.abbreviation

    #ProductCategory
    if selectCtrl == 'productCategory'
      $scope.buffer.productCategory_id = $scope.select.productCategory.id

    #    #Product
    if selectCtrl == 'product'
      $scope.buffer.product_id = $scope.select.product.id
      $scope.buffer.product = $scope.select.product.name

    #Metal
    if selectCtrl == 'metal'
      $scope.buffer.metal_id = $scope.select.metal.id
      $scope.buffer.metal_base = $scope.select.metal.base
      $scope.buffer.metal_name = $scope.select.metal.abbreviation
      $scope.buffer.metal_purity = $scope.select.metal.purity

    #    #Weight
    if selectCtrl == 'weight'
      WeightSummary()

    #UnitWeight
    if selectCtrl == 'unitWeight_select'
      $scope.buffer.unitWeight_id = $scope.select.unitWeight.id
      WeightSummary()

    #    #Size
    if selectCtrl == 'size'
      #sizeSummary display update
      SizeSummary()

    #    #UnitSize
    if selectCtrl == 'unitSize_select'
      $scope.buffer.unitSize_id = $scope.select.unitSize.id
      SizeSummary()

#  #==========================================
#  # Set Error on all required controls
#  #==========================================
#  #Error flag used by ng-class in control
  setErrorFields= () ->
    fields = ItemModel.getErrorsAll()
    #swaFn.logObject(debug, fields, "Error Structure")

    # Find a specific field and display its details
    for k,v of fields
      if v.field is 'description'
        swaFn.logString(debug, "Details: #{v.error_msg}")

#    $scope.error.state_id = SellerModel.getFieldError('state_id')
#    $scope.error.license_id = SellerModel.getFieldError('license_id')
#    $scope.error.number = SellerModel.getFieldError('number')
#    $scope.error.month = SellerModel.getFieldError('month')
#    $scope.error.day = SellerModel.getFieldError('day')
#    $scope.error.year = SellerModel.getFieldError('year')


  #=============================================
  #  Bootstrap controller code
  #=============================================
  # All queries are now operformed at the tab level
  # Load foreign_keys with promises
  bootstrapController = () ->

      $scope.Genders = gon.genders
      $scope.Products = gon.products
      $scope.ProductCategories = gon.product_categories
      $scope.Metals = gon.metals
      $scope.UnitWeights = gon.unit_weights
      $scope.UnitSizes = gon.unit_sizes

# Working code - Disabled to test Gon
#    $q.all([
#      qryGenders.Query(),
#      qryProducts.Query(),
#      qryProductCategories.Query(),
#      qryMetals.Query(),
#      qryUnitWeights.Query(),
#      qryUnitSizes.Query()
#
#    ]).then (result) ->
#      $scope.Genders = result[0]
#      $scope.Products = result[1]
#      $scope.ProductCategories = result[2]
#      $scope.Metals = result[3]
#      $scope.UnitWeights = result[4]
#      $scope.UnitSizes = result[5]

      # Call display
      loadBufferData()
      # Initialize seelct controls
      initSelectControls()
      #Set all errors for this page
      setErrorFields()

  #Execute bootstrap controller
  bootstrapController()


]