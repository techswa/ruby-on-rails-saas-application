App.controller 'ItemDialogTabStonesCtrl', ['$scope', '$rootScope', '$q', 'swaFn', 'ItemModel', 'qryStoneSizes', 'qryStoneColors', 'qryStoneTypes',($scope, $rootScope, $q, swaFn, ItemModel, qryStoneSizes, qryStoneColors, qryStoneTypes) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into ItemDialogTabStonesCtrl")

  #==========================================
  #  Load external routines / libraries
  #==========================================
  ArrayItemIndex = (array, selection) =>
    swaFn.ArrayItemIndex(array, selection)

  #==========================================
  #  Define arrays and structures needed
  #==========================================
  #Create JSON to hold display data
  $scope.buffer = {}

  #Ceated to hold select control returned data
  $scope.select = {}

  #Create JSON to hold errors
  $scope.error = {}

  #==========================================
  # Add / Get / Set stone
  #==========================================
  #Default stone edit
  $scope.editDisplay = false

  #Holds stone edit row top position for use in style
  topRow = ""

  $scope.editStone = (idx) ->
    # Check if already in edit mode
    if $scope.editDisplay == true
      #dont act on click event
    else
      #Disable wizard buttons
      $scope.$parent.StepNavigation.priorButtonDisabled = true
      $scope.$parent.StepNavigation.nextButtonDisabled = true
      #Disable modal cancel button
      $scope.$parent.StepNavigation.formCancelDisabled = true
      #Set the row to edit & postion div

      #Set base height
      dialogOffset = 79
      #Set row height
      rowHeight = 35
      #Set border height
      borderHeight = 1
#      #Calculate input row position
      idx_offset = idx + 0
      #Calculate total height of row area
      rowBorderHeight = rowHeight+borderHeight
      #Calculate total height needed
      totalHeight = idx_offset * rowBorderHeight
      #Add dialog margin offset
      pixels = dialogOffset + totalHeight
      #Construct and set style value
      topRow = top:"#{pixels}px"

#       $scope.topOffset = ((idx +1)* (rowHeight+borderHeight))+ 79

      $scope.editDisplay = true

  $scope.getRowTop = () ->
     topRow
     #top:"89px"

  $scope.cancelStone = () ->
    $scope.editDisplay = false
    #Reset wizard buttons  /Both to be displayed
    $scope.$parent.StepNavigation.priorButtonDisabled = false
    $scope.$parent.StepNavigation.nextButtonDisabled = false
    #Display form cancel button
    $scope.$parent.StepNavigation.formCancelDisabled = false

  $scope.addStone = () ->
    swaFn.logString(debug,"addStone() called")
    stone_details ={qty:1, size:"L", color:"Green", type:"Costume" }
    ItemModel.addStone(stone_details)

  $scope.getStone = (stone_id) ->
    swaFn.logString(debug,"getStone() called")
    ItemModel.getStone(stone_id)

  $scope.stone = {qty:1, size:1, color:2, Type:2}

#  #==========================================
#  #load current record data from the model
#  #==========================================
#  getModelRecord= () ->
#    $scope.model.state_id = $scope.States[ArrayItemIndex($scope.States, SellerModel.getFieldValueTemp('state_id'))]
#    $scope.model.license_id = $scope.Licenses[ArrayItemIndex($scope.Licenses, SellerModel.getFieldValueTemp('license_id'))]
#    $scope.model.number = SellerModel.getFieldValueTemp('number')
#    $scope.model.month = $scope.Months[ArrayItemIndex($scope.Months, SellerModel.getFieldValueTemp('month'))]
#    $scope.model.day = $scope.Days[ArrayItemIndex($scope.Days, SellerModel.getFieldValueTemp('day'))]
#    $scope.model.year = SellerModel.getFieldValueTemp('year')

#  #========================================================
#  #  Save field changes to the model temp & display fields
#  #========================================================
#  $scope.setModelRecord=(selectCtrl)->
#    #setFieldDisplay: (field, string)
#    if selectCtrl == 'state'
#      SellerModel.setFieldValueTemp('state_id', $scope.model.state_id.id)
#      SellerModel.setFieldDisplayTemp('state_id', $scope.model.state_id.abbreviation)
#
#    if selectCtrl == 'license'
#      SellerModel.setFieldValueTemp('license_id', $scope.model.license_id.id)
#      SellerModel.setFieldDisplayTemp('license_id', $scope.model.license_id.abbreviation)
#
#    if selectCtrl == 'number'
#      SellerModel.setFieldValueTemp('number', $scope.model.number)
#      SellerModel.setFieldDisplayTemp('number', $scope.model.number)
#
#    if selectCtrl == 'month'
#      SellerModel.setFieldValueTemp('month', $scope.model.month.id)
#      SellerModel.setFieldDisplayTemp('month', $scope.model.month.id)
#
#    if selectCtrl == 'day'
#      SellerModel.setFieldValueTemp('day', $scope.model.day.id)
#      SellerModel.setFieldDisplayTemp('day', $scope.model.day.id)
#
#    if selectCtrl == 'year'
#      SellerModel.setFieldValueTemp('year', $scope.model.year)
#      SellerModel.setFieldDisplayTemp('year', $scope.model.year)

#  #==========================================
#  # Load curent error data from the model
#  #==========================================
#  #Error flag used by ng-class in control
#  getModelErrors= () ->
#    $scope.error.state_id = SellerModel.getFieldError('state_id')
#    $scope.error.license_id = SellerModel.getFieldError('license_id')
#    $scope.error.number = SellerModel.getFieldError('number')
#    $scope.error.month = SellerModel.getFieldError('month')
#    $scope.error.day = SellerModel.getFieldError('day')
#    $scope.error.year = SellerModel.getFieldError('year')


  #==========================================
  #  Load Buffer record to edit
  #==========================================
  #Display data to edit
  loadBufferData = () ->
    # Link to Model buffer
    $scope.buffer = ItemModel.getBuffer()
    swaFn.logObject(debug, $scope.buffer, "Buffer loaded")

  #=============================================
  #  Bootstrap controller code
  #=============================================
  bootstrapController = () ->

    $q.all([
      qryStoneSizes.Query(),
      qryStoneColors.Query(),
      qryStoneTypes.Query(),

    ]).then (result) ->
      $scope.StoneSizes = result[0]
      $scope.StoneColors = result[1]
      $scope.StoneTypes = result[2]

     # alert $scope.StoneSizes[0].abbreviation
     # alert $scope.StoneSizes[1].abbreviation
    # Load buffer data
    loadBufferData()



  #Bootstrap controller
  bootstrapController()


]