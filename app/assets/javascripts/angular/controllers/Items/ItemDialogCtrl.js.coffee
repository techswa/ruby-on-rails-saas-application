App.controller 'ItemDialogCtrl',  ['$scope', '$rootScope', '$q','$modal', '$modalInstance', 'item', '$filter', 'swaFn', 'MetalModel', "QuoteModel", 'ItemModel', 'qryGenders', 'qryMetals', 'qryProducts', 'qryProductCategories', 'qryStoneClarities', 'qryStoneColors', 'qryStoneCuts', 'qryStoneSizes', 'qryStoneTypes', 'qryUnitWeights', 'qryUnitSizes', ($scope, $rootScope, $q, $modal, $modalInstance, item, $filter, swaFn, MetalModel, QuoteModel, ItemModel, qryGenders, qryMetals, qryProducts, qryProductCategories, qryStoneClarities, qryStoneColors, qryStoneCuts, qryStoneSizes, qryStoneTypes, qryUnitWeights, qryUnitSizes) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into ItemDialogCtrl")

  #==============================================
  #  Determine operation to perform New or EDIT
  #==============================================

  #Edit existing item
  if item.op == 'EDIT'
    #$scope.item = item  #single item
    $scope.dialogTitle = "Edit existing item"
    # Reset the buffer to null
    ItemModel.clearBuffer()
    #    alert 'clear buffer'
    # Load the selected row into the buffer for editing
    ItemModel.loadBuffer(item.row)
#    alert 'Load buffer'

  else
    #Add new item
    # $scope.item = ItemModel.getNewItem()
    $scope.dialogTitle = "Enter new item"
    swaFn.logString(debug, "NEW Item called in ItemDialog")
    # Reset the buffer to null
    ItemModel.clearBuffer()
  # Create clear buffer to use

  #==========================================
  #  Configure step controller
  # id: integer handle for record
  # title: Title of form section
  # number: Glyph number to display in front of title
  # editState: active/disabled/completed - selected section title is highlighted
  # isDirty: true, false, null - If untouched = null, if error checked = true, else false
  # showForm: true /false ng-show div content for section
  # disabled: treu / false - section title disabled
  #==========================================
  $scope.steps = [
    {id:0,  title: "General", number: 1, template: "partials/_itemGeneral.html", editState:"active", hasError: null, showForm:true, disabled:false},
    {id:1,  title: "Stones", number: 2, template: "partials/_itemStones.html", editState:"inactive", hasError: null, showForm:false, disabled:true},
    {id:2,  title: "Details", number: 3,  template: "partials/_itemDetails.html", editState:"inactive", hasError: null, showForm:false, disabled:true},
    ]

  #==========================================
  #  Configure navigation controller
  # Set initial state
  #==========================================
  $scope.StepNavigation = {
    currentStep:0,
    priorButtonDisabled:true, # Form wizard prior button
    nextButtonDisabled:false, # Form wizrd next button
    formSaveDisabled:false, #Manages the save button
    formCancelDisabled:false} #Used to disable dialog cancel button during edits

  #Set default
  #$scope.priorDisabled = true

  #==========================================
  #  Manage step selections
  #==========================================
  #Clear current selection & display section
  hideFormSections = ->
    #loop through and clear all
    for step in $scope.steps
      step.editState = "inactive"
      step.showForm = false


  $scope.getPriorStep = () ->
    # if !Disabled
    if $scope.StepNavigation.priorButtonDisabled == false
      #check current for for errors - if false continue
      if !checkStepErrors ($scope.StepNavigation.currentStep)
        #Get the prior page
        $scope.setStepNavigation(-1)
      else
        alert 'Errors on form'

  $scope.getNextStep = () ->
    # if !Disabled
    if $scope.StepNavigation.nextButtonDisabled == false
      if !checkStepErrors ($scope.StepNavigation.currentStep)
        #Get the prior page
        #Get the next page
        $scope.setStepNavigation(1)
      else
        alert 'Errors on form'


  $scope.setStepNavigation = (offset) ->
    #Uses $scope.StepNavigation = {CurrentStep:0, priorButtonDisabled: true, nextButtonDisabled: false}
    StartStep = 0
    EndStep = $scope.steps.length - 1

    # if -1 is passed in
    if offset == -1 #Get prior available tab
      #If the current tab is the first one then
      if $scope.StepNavigation.currentStep isnt 0
        #Get new current step
        $scope.StepNavigation.currentStep = $scope.StepNavigation.currentStep - 1

        #if at first step then hide the prior button
        if $scope.StepNavigation.currentStep == 0
          $scope.StepNavigation.priorButtonDisabled = true
        else
          $scope.StepNavigation.priorButtonDisabled = false
        #Always display next button
        $scope.StepNavigation.nextButtonDisabled = false

    else # Get next available tab
      if $scope.StepNavigation.currentStep isnt EndStep
        #Get new current step
        $scope.StepNavigation.currentStep = $scope.StepNavigation.currentStep + 1

        #if at the end hide the next button
        if $scope.StepNavigation.currentStep == EndStep
          $scope.StepNavigation.nextButtonDisabled = true
        else
          $scope.StepNavigation.nextButtonDisabled = false

        #Always display prior button
        $scope.StepNavigation.priorButtonDisabled = false

    #Select next Step
    $scope.selectedStep($scope.StepNavigation.currentStep)

  #

  #Called by $scope.StepNavigation
  $scope.selectedStep = (current_step) ->
    #Hide all forms
    hideFormSections()

    #set new selection to active
    $scope.steps[current_step].editState = "active"
    $scope.steps[current_step].showForm = true

    #set prior selections to completed
    end = current_step
    #alert 'end: ' + end

    if current_step > 0
      #Set all to inactive
      for step in $scope.steps
        $scope.steps.editState = "inactive"

      section = 0
      #alert current_step
      while section < current_step
        $scope.steps[section].editState = "completed"
        section++

  #==========================================
  #Check step for errors
  #==========================================
  checkStepErrors = (step) ->
    #No errors then set step isDirty = false
    #Has errors then set step to isDirty = true

    #Set false for testing
    $scope.steps[step].hasError = false


  #==========================================
  #  Process dialog exit
  #==========================================

  $scope.save = ->
    #if !disabled
    if $scope.StepNavigation.formIncomplete == false
      # Copy temp values into value field
      ItemModel.saveBuffer()
      $modalInstance.close true


  $scope.cancel = ->
    #if !disabled
    if $scope.StepNavigation.formCancelDisabled == false
      swaFn.logString(debug, "Dialog close button clicked")
      $modalInstance.close false


#  #==============================
#  #Calculate value of metals
#  #==============================
#
#  $scope.calcItemValue1 = (item) ->
#    swaFn.logObject(debug, item ,"calc item value")
#
#    #reset value to zero
#    value = 0
#    #Get the details for the selected metal
#
#    metalDetails = $scope.getMetalDetails(item.metal_id)
#    swaFn.logObject(debug, metalDetails,"Metal details")
#
#    metalBase = metalDetails.base
#    metalPurity = metalDetails.purity
#    metalWeight = item.weight
#    #get factor for value
#    weightFactor = getWeightFactor(item.unitWeight_id)
#    swaFn.logString(debug, "base metal " + metalBase)
#    #If metal is gold
#    if metalBase == 'Au'
#      value = $scope.calcGoldValue(metalPurity, metalWeight, weightFactor)
#    #alert "item value: " + value
#    #if metal is silver
#    if metalBase == 'Ag'
#      value = $scope.calcSilverValue(metalPurity, metalWeight, weightFactor)
#    #if metal is platinum
#    if metalBase == 'Pt'
#      value = $scope.calcPlatinumValue(metalPurity, metalWeight, weightFactor)
#
#    swaFn.logString(debug, "Calc cvalue is : " + value)
#    return value
#
#  #Determine what unit is being us and return factor for value
#  # as in how many units per troy ounce
#  getWeightFactor = (unitWeight_id) ->
#    for unitWeight in $scope.UnitWeights
#      if unitWeight.id == unitWeight_id
#        return unitWeight.factor
#
#  #Load details regarding a metal
#  $scope.getMetalDetails = (metal_id) ->
#    for metal in $scope.Metals
#      if metal.id == metal_id
#        return metal

]