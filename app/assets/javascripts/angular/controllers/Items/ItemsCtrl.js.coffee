App.controller 'ItemsCtrl', ['$scope','$rootScope','$q','$modal', 'swaFn', 'SharedData','MetalModel','ItemModel','qryUserCurrent',($scope, $rootScope, $q, $modal, swaFn, SharedData, MetalModel, ItemModel, qryUserCurrent) ->

   #==========================================
   #  Controller responsibilities
   #==========================================
   # This controller has three tasks.
   # 1. Define Item dialog
   # 2. Display the Item's Edit dialog
   # 3. Display formatted grid of items

   #==========================================
   # Debug flags
   #==========================================
   debug = true
   swaFn.logString(debug, "Entering ItemsCtrl")


   $scope.ItemModel = ItemModel

   #Use test items fixture
   ItemModel.useTestItems()

   # Create new item
   $scope.newRow=()->
     # null used in place of row number
     swaFn.logString(debug, "ItemCtrl.$scope.createRow")
     callItemDialog('NEW', null)

   # Edit current row
   $scope.editRow=(row)->
     swaFn.logString(debug, "Edit requested on row #{row}")
     callItemDialog('EDIT', row)


  #called from main page
   callItemDialog = (op, row) ->
     #passed parameters
     data = {op, row}
     modalInstance = $modal.open(
       backdrop: 'static',
       keyboard: true,
       templateUrl: 'dialogs/itemDialog-test.html',
       controller: 'ItemDialogCtrl',
#       templateUrl: 'partials/tabTest.html',
#       controller: 'TabTestCtrl',
       windowClass: 'modal itemDialog'
       resolve:
         item: ->
           data
     )
     modalInstance.result.then ((result) ->
       swaFn.logString(debug, "dialog return area")
       swaFn.logString(debug, "Dialog has returned: #{result}")
       # Update and Display Seller
       if result == false
         swaFn.logString(debug, "Changes have been discarded.")
       else
         swaFn.logString(debug, "Changes have been saved.")
       #displayRecord()

     ), ->
       #$log.info "Modal dismissed at: " + new Date()







#   #==========================================
#   #  Define Dialog
#   #==========================================
#   dialogOptions =
#     controller: "ItemDialogTabCtrl"
#     templateUrl: "dialogs/itemDialog.html"
#
#   #==========================================
#   #  Create dialog and display
#   #==========================================
#   # Create Seller Dialog function
#   # operation and row
#   callItemDialog-old = (op, row)->
#     alert "Dialog called"
#     #Package data for use
#     data = {op, row}
#
#     $dialog.dialog(angular.extend(dialogOptions,
#        resolve:
#          item: ->
#           angular.copy data
#       )).open().then (result) ->
#        swaFn.logString(debug, "Dialog return area")
#        swaFn.logString(debug, "Dialog has returned: #{result}")
#        # Update and Display Seller
#        if result == false
#          swaFn.logString(debug, "Changes have been discarded.")
#        else
#          swaFn.logString(debug, "Changes have been saved.")
#          # Refresh seller data display
#          #displayRecord()


       #Replaced in Model with deleteItem()
#   #Delete slected row                            mn
   $scope.deleteRow = (row) ->
     #console.log "Selected record for deletion " + JSON.stringify(row, null, 4)
     #console.log "Record id is: " + JSON.stringify(row.id, null, 4)
     ItemModel.deleteItem(row.id)
     #Call deleteion method in Model
     #ItemModel.deleteItem(row.id)


   #swaFn.alertObject(debug, ItemModel.getItems(),"ItemData")
   # Define grid for display - Pull direct from the Model
   # was using  data: 'ItemModel.getItems()'
   $scope.ItemsGrid = {data: 'ItemModel.getItems()',
   enableRowSelections: true,
   multiSelect: false,
   keepLastSelected: false,
   enableSorting: false,
   plugins: [new ngGridFlexibleHeightPlugin()],
   selectedItems: [],
   afterSelectionChange: ->
     #If a row has been selected
     if $scope.ItemsGrid.selectedItems[0] isnt undefined
       swaFn.logObject(debug, $scope.ItemsGrid.selectedItems[0], "Selected row is: ")
       rec_id = $scope.ItemsGrid.selectedItems[0].id
       #Edit existing item
       $scope.editRow(rec_id)

   columnDefs:[{
     field: 'product',
     displayName: 'Item',
     #beforeSelectionChange: NoSelection(),
     cellFilter: '',
     cellTemplate: '',
     enableCellEdit: false,
     editableCellTemplate: '',
     width: 200
   },{
     field: 'gender',
     displayName: 'M/F',
     cellFilter: '',
     cellTemplate: '<div class="cell-align-center">{{row.getProperty(col.field)}}</div>',
     enableCellEdit: false,
     editableCellTemplate: '',
     width: 40
   },{
     field: 'metal_name',
     displayName: 'Metal',
     cellFilter: '',
     cellTemplate: '<div class="cell-align-right">{{row.getProperty(col.field)}}</div>',
     enableCellEdit: false,
     editableCellTemplate: '',
     width: 65
   },{
     field: 'weightSummary',
     displayName: 'Weight',
     cellFilter: '',
     cellTemplate: '<div class="cell-align-right">{{row.getProperty(col.field)}}</div>',
     enableCellEdit: false,
     editableCellTemplate: '',
     width: 95
   },{
     field: 'sizeSummary',
     displayName: 'Size',
     cellFilter: '',
     cellTemplate: '<div class="cell-align-right">{{row.getProperty(col.field)}}</div>',
     enableCellEdit: false,
     editableCellTemplate: '',
     width: 65
   },{
     field: 'payment',
     displayName: 'Payout',
     cellFilter: 'formatDollar',
     cellTemplate: '<div class="cell-align-right">{{row.getProperty(col.field) | formatDollar}}</div>',
     enableCellEdit: false,
     editableCellTemplate: '',
     width: 70
   },{
     field: 'description',
     displayName: 'Description',
     cellFilter: '',
     cellTemplate: '',
     enableCellEdit: false,
     editableCellTemplate: ''
   }
#   ,{
#     field: '',
#     displayName: '',
#     cellFilter: '',
##     cellTemplate: '<span class="icon-edit"></span><button class="btn btn-small btn-link" ng-click="editRow(row.entity)">Edit</button><span class="icon-remove"></span><button class="btn btn-small btn-link" ng-click="deleteRow(row.entity)">Delete</button>',
#     cellTemplate: '<span class="icon-edit"></span><button class="btn btn-small btn-link" ng-click="editRow(row.entity.id)">Edit</button><span class="icon-remove"></span><button class="btn btn-small btn-link" ng-click="deleteRow(row.entity)">Delete</button>',
#     enableCellEdit: false,
#     editableCellTemplate: ''
#   }
   ]};

   #Display initial totals
  #RecalculatePaymentTotal()

] #Keep this - end of app

