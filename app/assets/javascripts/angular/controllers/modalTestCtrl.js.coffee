App.controller 'ModalTestCtrl', ['$scope', '$rootScope', '$modal', '$log','swaFn', ($scope, $rootScope, $modal, $log, swaFn ) ->

  # Works with newer BS UI 0.10.0
  # $dialog has been removed from library

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into ModalTestCtrl")

  $scope.items = ["item1", "item2", "item3"]

  openModalDialog = () ->
    alert 'Inside open modal'
    modalInstance = $modal.open(
      backdrop: 'static',
      keyboard: true,
      templateUrl: 'dialogs/modalTest.html',
      controller: 'ModalInstanceCtrl',
      windowClass: 'modal modal-test'
      resolve:
        items: ->
          $scope.items
    )
    modalInstance.result.then ((selectedItem) ->
      $scope.selected = selectedItem
      return
    ), ->
      $log.info "Modal dismissed at: " + new Date()
      return
    return

  $scope.modalOpen = () ->
    # Display dialog
    swaFn.logString(debug, "modalOpen() called")
    openModalDialog()

  return
]
