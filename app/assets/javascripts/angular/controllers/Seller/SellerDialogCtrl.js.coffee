App.controller 'SellerDialogCtrl', ['$scope', '$q', '$modal', '$detection', '$modalInstance', 'items', 'swaFn', 'SellerModel', ($scope, $q, $modal, $detection, $modalInstance, items, swaFn, SellerModel) ->

  #==========================================
  # General Operation
  # -----------------
  # SellerModel is the Service that contains
  # the data that is collected and edited
  #
  # $scope.buffer is used as a temporary record
  # thus allowing for a complete rollback
  #
  # When a record is saved the SellerModel data
  # is overwritten by $scope.buffer
  #==========================================

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into SellerDialogCtrl")

  #==========================================
  #  Constants
  #==========================================
  EDIT_STATE = true
  ACTIVE_STATE = "active"
  INACTIVE_STATE = "inactive"
  COMPLETED_STATE = "completed"

  CTRL_STATE = 'state'
  CTRL_LICENSE = 'license'
  CTRL_MONTH = 'month'
  CTRL_DAY = 'day'
  CTRL_STATE_ID = 'state_id'
  CTRL_LICENSE_ID = 'license_id'
  CTRL_NUMBER = 'number'
  CTRL_MONTH = 'month'
  CTRL_DAY = 'day'
  CTRL_YEAR = 'year'
  CTRL_NAME_FIRST = 'name_first'
  CTRL_NAME_LAST = 'name_last'
  CTRL_ADDRESS_1 = 'address_1'
  CTRL_ADDRESS_2 = 'address_2'
  CTRL_CITY = 'city'
  CTRL_ASTATE_ID = 'astate_id'
  CTRL_ZIPCODE = 'zipcode'
  CTRL_HEIGHT_FEET = 'height_feet'
  CTRL_HEIGHT_INCHES = 'height_inches'
  CTRL_GENDER_ID = 'gender_id'
  CTRL_EYECOLOR_ID = 'eyecolor_id'

  #==============================================
  # Get broswer type
  #==============================================
  $scope.isiOS = false
  $scope.isOther = false
  if $detection.isiOS()
    $scope.browser_type = "iOS"# if $detection.isiOS()
    $scope.isiOS = true# if $detection.isiOS()
  else
    $scope.isOther = true
  $scope.browser_type = "Safari Desktop" if $detection.isSafariDesktop()
  #swaFn.logString(debug, navigator.userAgent)

  #==============================================
  # Determine operation to perform New or EDIT
  #==============================================

  #Edit existing item
  if items.op == EDIT_STATE
    #alert item.op
    #$scope.item = item  #single item
    $scope.dialogTitle = "Edit Seller"
    # Reset the buffer to null
    SellerModel.clearBuffer()
    # Load the selected row into the buffer for editing
    swaFn.logString(debug, "SellerModel.loadBuffer() - SellerdDialogCtrl")
    SellerModel.loadBuffer()
    # The tab controller is now loaded
  else
    #Add new item
    # $scope.item = ItemModel.getNewItem()
    $scope.dialogTitle = "Create New Seller"

  #==========================================
  #  Load external routines / libraries
  #==========================================
  ArrayItemIndex = (array, selection) =>
    swaFn.ArrayItemIndex(array, selection)

  #manage record buffer
  getBufferValue = (Key) ->
    return SellerModel.getBufferValue(Key)

  #==========================================
  #  Process dialog exit
  #==========================================
  $scope.submitForm = (isValid) ->
    #alert 'Entering submit form'
  # check to make sure the form is completely valid
    $scope.save() if isValid


  $scope.save = ->
    # Copy temp values into value field
    #SellerModel.commitRecordTansaction()
    SellerModel.saveBuffer()
    swaFn.logObject(true, SellerModel.getSellers(), "Seller object changes saved")
    $modalInstance.close true


  $scope.close = ->
    swaFn.logString(debug, "Dialog close button clicked")
    $modalInstance.close false
    #$modalInstance.hide


  #==========================================
  #  Define arrays and structures needed
  #==========================================
  #Create JSON to hold display data
  $scope.buffer = {}

  #Ceated to hold select control returned data
  $scope.select = {}

  #Create JSON to hold errors
  $scope.error = {}

  #Create array of months for select control population
  $scope.Months = []

  MonthsCreate = () ->
    for month in [1..12] by 1
      $scope.Months.push ({id: + String(month)})


  #Create array of days for select control population
  $scope.Days = []

  DaysCreate = () ->
    for day in [1..31] by 1
      $scope.Days.push ({id: + String(day)})

  #Create array of feet for select control population
  $scope.HeightFeetOptions = []

  HeightFeetOptionsCreate = () ->
    for foot in [4..7] by 1
      $scope.HeightFeetOptions.push ({id: + String(foot)})

  #Create array of inches for select control population
  $scope.HeightInchOptions = []

  HeightInchOptionsCreate = () ->
    value = ''
    for inch in [0..11] by 1
      $scope.HeightInchOptions.push ({id: + String(inch)})

  #alert(JSON.stringify($scope.HeightInchOptions))
  #alert($scope.HeightInchOptions)
  #==========================================
  #  Load Buffer record to edit
  #==========================================
  #Display data to edit
  loadBufferData = () ->
    # Link to Model buffer
    $scope.buffer = SellerModel.getBuffer()
    #alert 'SellerDialgCtrl.loadBufferData() ' + $scope.buffer.zipcode
  #==========================================
  # Initialize select controls
  #==========================================
  initSelectControls = () ->
    $scope.select.state_id = $scope.States[ArrayItemIndex($scope.States, getBufferValue(CTRL_STATE_ID))]
    $scope.select.license_id = $scope.Licenses[ArrayItemIndex($scope.Licenses, getBufferValue(CTRL_LICENSE_ID))]
    $scope.select.month = $scope.Months[ArrayItemIndex($scope.Months, getBufferValue(CTRL_MONTH))]
    $scope.select.day = $scope.Days[ArrayItemIndex($scope.Days, getBufferValue(CTRL_DAY))]
    $scope.select.year = getBufferValue(CTRL_YEAR)
    $scope.select.astate_id = $scope.States[ArrayItemIndex($scope.States, getBufferValue(CTRL_ASTATE_ID))]
    $scope.select.height_feet = $scope.HeightFeetOptions[ArrayItemIndex($scope.HeightFeetOptions, getBufferValue(CTRL_HEIGHT_FEET))]
    $scope.select.height_inches = $scope.HeightInchOptions[ArrayItemIndex($scope.HeightInchOptions, getBufferValue(CTRL_HEIGHT_INCHES))]
    $scope.select.gender_id = $scope.Genders[ArrayItemIndex($scope.Genders, getBufferValue(CTRL_GENDER_ID))]
    $scope.select.eyecolor_id = $scope.Eyecolors[ArrayItemIndex($scope.Eyecolors, getBufferValue(CTRL_EYECOLOR_ID))]


  #====================================================================
  #  Update Buffer Process any click events / Update dependant fields
  #  Called from ng-change in html
  #====================================================================
  $scope.updateBuffer = (selectCtrl) ->

    if selectCtrl == CTRL_STATE
      $scope.buffer.state_id = $scope.select.state_id.id
      $scope.buffer.state = $scope.select.state_id.abbreviation

    if selectCtrl == CTRL_LICENSE
      $scope.buffer.license_id = $scope.select.license_id.id
      $scope.buffer.license = $scope.select.license_id.abbreviation

    if selectCtrl == CTRL_MONTH
      $scope.buffer.month = $scope.select.month.id

    if selectCtrl == CTRL_DAY
      $scope.buffer.day = $scope.select.day.id

    if selectCtrl == CTRL_ASTATE_ID
      $scope.buffer.astate_id = $scope.select.astate_id.id
      $scope.buffer.astate = $scope.select.astate_id.abbreviation

    if selectCtrl == CTRL_HEIGHT_FEET
      $scope.buffer.height_feet = $scope.select.height_feet.id

    if selectCtrl == CTRL_HEIGHT_INCHES
      $scope.buffer.height_inches = $scope.select.height_inches.id

    if selectCtrl == CTRL_GENDER_ID
      $scope.buffer.gender_id = $scope.select.gender_id.id
      $scope.buffer.gender = $scope.select.gender_id.abbreviation

    if selectCtrl == CTRL_EYECOLOR_ID
      $scope.buffer.eyecolor_id = $scope.select.eyecolor_id.id
      $scope.buffer.eyecolor = $scope.select.eyecolor_id.abbreviation


  #==========================================
  # Load current error data from the model
  #==========================================
  #Error flag used by ng-class in control
  getModelErrors= () ->
    $scope.error.state_id = SellerModel.getFieldError(CTRL_STATE_ID)
    $scope.error.license_id = SellerModel.getFieldError(CTRL_LICENSE_ID)
    $scope.error.number = SellerModel.getFieldError(CTRL_NUMBER)
    $scope.error.month = SellerModel.getFieldError(CTRL_MONTH)
    $scope.error.day = SellerModel.getFieldError(CTRL_DAY)
    $scope.error.year = SellerModel.getFieldError(CTRL_YEAR)
    $scope.error.name_first = SellerModel.getFieldError(CTRL_NAME_FIRST)
    $scope.error.name_last = SellerModel.getFieldError(CTRL_NAME_LAST)
    $scope.error.address_1 = SellerModel.getFieldError(CTRL_ADDRESS_1)
    $scope.error.address_2 = SellerModel.getFieldError(CTRL_ADDRESS_2)
    $scope.error.city = SellerModel.getFieldError(CTRL_CITY)
    $scope.error.astate_id = SellerModel.getFieldError(CTRL_ASTATE_ID)
    $scope.error.zipcode = SellerModel.getFieldError(CTRL_ZIPCODE)


  #=============================================
  #  Bootstrap controller code
  #=============================================
  bootstrapController = () ->

    $scope.States = gon.states
    $scope.Licenses = gon.licenses
    $scope.Genders = gon.genders
    $scope.Eyecolors = gon.eyecolors
    # Code below works - Disabled for Gon test
    #    $q.all([
    #      qryStates.Query(),
    #      qryLicenses.Query(),
    #
    #    ]).then (result) ->
    #      $scope.States = result[0]
    #      $scope.Licenses = result[1]

    MonthsCreate()
    DaysCreate()
    HeightFeetOptionsCreate()
    HeightInchOptionsCreate()

    # Load buffer to edit
    loadBufferData()
    #Initialize Select control values
    initSelectControls()

  #Bootstrap controller
  bootstrapController()

]