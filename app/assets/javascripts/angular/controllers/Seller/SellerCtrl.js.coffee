App.controller 'SellerCtrl', ['$scope','$q', '$modal', '$log', '$resource', 'swaFn', 'SellerModel', 'qryEyeColors','qryGenders','qryLicenses', 'qrySeller', 'qryStates', ($scope, $q, $modal, $log, $resource, swaFn, SellerModel, qryEyeColors, qryGenders, qryLicenses, qrySeller, qryStates) ->

  #==========================================
  #  Controller responsibilities
  #==========================================
  # This controller has two tasks.
  # 1. Define Seller's dialog
  # 2. Display the Seller's Edit dialog
  # 3. Display formatted Seller's record

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering SellerCtrl")

  #==========================================
  #  Constants
  #==========================================
  EDIT_STATE = 0
  ACTIVE_STATE = 1
  INACTIVE_STATE = 2
  COMPLETED_STATE = 3

  #==========================================
  #  Load external routines / libraries
  #==========================================
  ArrayItemIndex = (array, selection) =>
    swaFn.ArrayItemIndex(array, selection)

  getDisplayValue = (field) =>
    SellerModel.getFieldValue(field)

  #==============================================
  # Calls dialog
  #==============================================
  # Create Seller Dialog function
  $scope.sellerDialogShow = () ->
    #alert 'Clicked show seller dialog.'
  # Display dialog
    swaFn.logString(debug, "createSellerDialog(EDIT_STATE,0)")
    createSellerDialog(EDIT_STATE,0)


  $scope.toggleModal = () ->
    #alert 'Selected toggle modal'
#    $scope.modalShown = false
    $scope.modalShown = !$scope.modalShown

  #==============================================
  # Define dialog and display
  #==============================================

  $scope.test = 'efegegeeg'

  #==========================================
  #  Define dialog and display
  #==========================================
  # Create Seller Dialog function

  createSellerDialog = (op, row) ->
    #Package data for use
    data = {op, row}

    modalInstance = $modal.open(
      backdrop: 'static',
      keyboard: true,
      templateUrl: 'dialogs/sellerDialog.html',
      controller: 'SellerDialogCtrl',
      windowClass: 'modal sellerDialog'
      resolve:
        items: ->
          data
    )
    modalInstance.result.then ((result) ->
      swaFn.logString(debug, "dialog return area")
      swaFn.logString(debug, "Dialog has returned: #{result}")
      # Update and Display Seller
      if result == false
        swaFn.logString(debug, "Changes have been discarded.")
      else
        swaFn.logString(debug, "Changes have been saved.")
        displayRecord()

    ), ->
      $log.info "Modal dismissed at: " + new Date()

      modalInstance.result.then ((selectedItem) ->
        $scope.selected = selectedItem
      ), ->
        $log.info "modalInstance.result " + "Modal dismissed at: " + new Date()
        #$log.info "Modal dismissed at: " + new Date()
        # Refresh seller data display
        displayRecord()

# Replaced code that used depricated $dialog
#        $dialog.dialog(angular.extend(dialogOptions,
#          resolve:
#            item: ->
#              angular.copy data
#         )).open().then (result) ->
#          swaFn.logString(debug, "dialog return area")
#          swaFn.logString(debug, "Dialog has returned: #{result}")
#          # Update and Display Seller
#          if result == false
#            swaFn.logString(debug, "Changes have been discarded.")
#          else
#            swaFn.logString(debug, "Changes have been saved.")
#            displayRecord()
#            # Refresh seller data display
#         ## displayRecord()

  #==========================================
  #  Create and display Seller's record
  #==========================================
  displayRecord1 = () ->
    alert 'displayRecord() called'
    #SellerModel.setSellerLine1("Hello Line 1")
    $scope.SellerLine1 = createSellerLine1()
    $scope.SellerLine2 = createSellerLine2()
    $scope.SellerLine3 = createSellerLine3()
    $scope.SellerLine4 = createSellerLine4()
    $scope.SellerLine5 = createSellerLine5()

    $scope.sellerId = $scope.SellerLine1 + $scope.SellerLine2 + $scope.SellerLine3 + $scope.SellerLine4 + $scope.SellerLine5


  createSellerLine1 = () ->
    #alert 'createSellerLine1'
    birthdata = getDisplayValue('month') + '/' + getDisplayValue('day') + '/' + getDisplayValue('year')
    return  getDisplayValue('state') + ' ' + getDisplayValue('license') + ' ' + getDisplayValue('number') + ' ' + birthdata + '\n'

  createSellerLine2 = () ->
    return getDisplayValue('name_first') + ' ' + getDisplayValue('name_last')+ '\n'

  createSellerLine3 = () ->
    return getDisplayValue('address_1') + ' ' + getDisplayValue('address_2')+ '\n'

  createSellerLine4 = () ->
    return getDisplayValue('city') + ', ' + getDisplayValue('astate') + ' ' + getDisplayValue('zipcode')+ '\n'

  createSellerLine5 = () ->
    return getDisplayValue('height_feet') + "' " + getDisplayValue('height_inches') + "' " + getDisplayValue('gender') + ' ' + getDisplayValue('eyecolor')


  displayRecord = () ->
    licenseData = getDisplayValue('state') + ' ' + getDisplayValue('license') + ' ' + getDisplayValue('number')
    birthData = getDisplayValue('month') + '/' + getDisplayValue('day') + '/' + getDisplayValue('year')
    nameData = getDisplayValue('name_first') + ' ' + getDisplayValue('name_last')
    addressData = getDisplayValue('address_1') + ' ' + getDisplayValue('address_2')
    cityData = getDisplayValue('city') + ', ' + getDisplayValue('astate') + ' ' + getDisplayValue('zipcode')
    bioData = getDisplayValue('height_feet') + "' " + getDisplayValue('height_inches') + "' " + getDisplayValue('gender') + ' ' + getDisplayValue('eyecolor')
    $scope.modalShown = false
    $scope.sellerId = licenseData + ' ' + birthData + '\n' + nameData + '\n' + addressData + '\n' + cityData + '\n' + bioData

  # Code below used for testing only
  SellerModel.loadFixtureRecord()
  displayRecord()

]