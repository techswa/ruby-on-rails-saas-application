
#Format integer into currency
formatDollar = (input) ->
  # Convert pennies to dollars
  output = input / 100
  #Will this need a comma separator
  if output > 999
    comma = true

  # Add decimal point, convert to string
  temp = output.toFixed(2).toString()

  # Add comma thousands separator
  if comma
    position = 1
    #ret = [temp.slice(0, position), ",", temp(position)].join('')
    ret = temp.substr(0, position) + "," + temp.substr(position)
  else
    ret = temp

  input = ret