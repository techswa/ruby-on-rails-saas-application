#
# Attaches input mask onto input element
#
angular.module("ui.mask", []).value("uiMaskConfig",
  maskDefinitions:
    9: '/\d/'
    A: '/[a-zA-Z]/'
    "*": '/[a-zA-Z0-9]/'
).directive "uiMask", ["uiMaskConfig", (maskConfig) ->
  console.log "maskDefinitions: " +  JSON.stringify(maskConfig, null, 4)
  priority: 100
  require: "ngModel"
  restrict: "A"
  compile: uiMaskCompilingFunction = ->

#    options = maskConfig
#    uiMaskLinkingFunction = (scope, iElement, iAttrs, controller) ->
#
#      # Minimum required length of the value to be considered valid
#
#      # Vars for initializing/uninitializing
#
#      # Vars used exclusively in eventHandler()
#      initialize = (maskAttr) ->
#        return uninitialize()  unless angular.isDefined(maskAttr)
#        processRawMask maskAttr
#        return uninitialize()  unless maskProcessed
#        initializeElement()
#        bindEventListeners()
#        true
#      initPlaceholder = (placeholderAttr) ->
#        return  unless angular.isDefined(placeholderAttr)
#        maskPlaceholder = placeholderAttr
#        # If the mask is processed, then we need to update the value
#        eventHandler()  if maskProcessed
#      formatter = (fromModelValue) ->
#        return fromModelValue  unless maskProcessed
#        value = unmaskValue(fromModelValue or "")
#        isValid = validateValue(value)
#        controller.$setValidity "mask", isValid
#        (if isValid and value.length then maskValue(value) else `undefined`)
#      parser = (fromViewValue) ->
#        return fromViewValue  unless maskProcessed
#        value = unmaskValue(fromViewValue or "")
#        isValid = validateValue(value)
#
#        # We have to set viewValue manually as the reformatting of the input
#        # value performed by eventHandler() doesn't happen until after
#        # this parser is called, which causes what the user sees in the input
#        # to be out-of-sync with what the controller's $viewValue is set to.
#        controller.$viewValue = (if value.length then maskValue(value) else "")
#        controller.$setValidity "mask", isValid
#        controller.$setValidity "required", false  if value is "" and controller.$error.required isnt `undefined`
#        (if isValid then value else `undefined`)
#
#      # we can't use angular.copy nor angular.extend, they lack the power to do a deep merge
#      uninitialize = ->
#        maskProcessed = false
#        unbindEventListeners()
#        if angular.isDefined(originalPlaceholder)
#          iElement.attr "placeholder", originalPlaceholder
#        else
#          iElement.removeAttr "placeholder"
#        if angular.isDefined(originalMaxlength)
#          iElement.attr "maxlength", originalMaxlength
#        else
#          iElement.removeAttr "maxlength"
#        iElement.val controller.$modelValue
#        controller.$viewValue = controller.$modelValue
#        false
#      initializeElement = ->
#        value = oldValueUnmasked = unmaskValue(controller.$modelValue or "")
#        valueMasked = oldValue = maskValue(value)
#        isValid = validateValue(value)
#        viewValue = (if isValid and value.length then valueMasked else "")
#        # Double maxlength to allow pasting new val at end of mask
#        iElement.attr "maxlength", maskCaretMap[maskCaretMap.length - 1] * 2  if iAttrs.maxlength
#        iElement.attr "placeholder", maskPlaceholder
#        iElement.val viewValue
#        controller.$viewValue = viewValue
#
#      # Not using $setViewValue so we don't clobber the model value and dirty the form
#      # without any kind of user interaction.
#      bindEventListeners = ->
#        return  if eventsBound
#        iElement.bind "blur", blurHandler
#        iElement.bind "mousedown mouseup", mouseDownUpHandler
#        iElement.bind "input keyup click", eventHandler
#        eventsBound = true
#      unbindEventListeners = ->
#        return  unless eventsBound
#        iElement.unbind "blur", blurHandler
#        iElement.unbind "mousedown", mouseDownUpHandler
#        iElement.unbind "mouseup", mouseDownUpHandler
#        iElement.unbind "input", eventHandler
#        iElement.unbind "keyup", eventHandler
#        iElement.unbind "click", eventHandler
#        eventsBound = false
#      validateValue = (value) ->
#        # Zero-length value validity is ngRequired's determination
#        (if value.length then value.length >= minRequiredLength else true)
#      unmaskValue = (value) ->
#        valueUnmasked = ""
#        maskPatternsCopy = maskPatterns.slice()
#
#        # Preprocess by stripping mask components from value
#        value = value.toString()
#        angular.forEach maskComponents, (component) ->
#          value = value.replace(component, "")
#
#        angular.forEach value.split(""), (chr) ->
#          if maskPatternsCopy.length and maskPatternsCopy[0].test(chr)
#            valueUnmasked += chr
#            maskPatternsCopy.shift()
#
#        valueUnmasked
#      maskValue = (unmaskedValue) ->
#        valueMasked = ""
#        maskCaretMapCopy = maskCaretMap.slice()
#        angular.forEach maskPlaceholder.split(""), (chr, i) ->
#          if unmaskedValue.length and i is maskCaretMapCopy[0]
#            valueMasked += unmaskedValue.charAt(0) or "_"
#            unmaskedValue = unmaskedValue.substr(1)
#            maskCaretMapCopy.shift()
#          else
#            valueMasked += chr
#
#        valueMasked
#      getPlaceholderChar = (i) ->
#        placeholder = iAttrs.placeholder
#        if typeof placeholder isnt "undefined" and placeholder[i]
#          placeholder[i]
#        else
#          "_"
#
#      # Generate array of mask components that will be stripped from a masked value
#      # before processing to prevent mask components from being added to the unmasked value.
#      # E.g., a mask pattern of '+7 9999' won't have the 7 bleed into the unmasked value.
#      # If a maskable char is followed by a mask char and has a mask
#      # char behind it, we'll split it into it's own component so if
#      # a user is aggressively deleting in the input and a char ahead
#      # of the maskable char gets deleted, we'll still be able to strip
#      # it in the unmaskValue() preprocessing.
#      getMaskComponents = ->
#        maskPlaceholder.replace(/[_]+/g, "_").replace(/([^_]+)([a-zA-Z0-9])([^_])/g, "$1$2_$3").split "_"
#
#      processRawMask = (mask) ->
#        characterCount = 0
#        maskCaretMap = []
#        maskPatterns = []
#        maskPlaceholder = ""
#        if typeof mask is "string"
#          minRequiredLength = 0
#          isOptional = false
#          splitMask = mask.split("")
#          angular.forEach splitMask, (chr, i) ->
#            if linkOptions.maskDefinitions[chr]
#              maskCaretMap.push characterCount
#              maskPlaceholder += getPlaceholderChar(i)
#              maskPatterns.push linkOptions.maskDefinitions[chr]
#              characterCount++
#              minRequiredLength++  unless isOptional
#            else if chr is "?"
#              isOptional = true
#            else
#              maskPlaceholder += chr
#              characterCount++
#
#
#        # Caret position immediately following last position is valid.
#        maskCaretMap.push maskCaretMap.slice().pop() + 1
#        maskComponents = getMaskComponents()
#        maskProcessed = (if maskCaretMap.length > 1 then true else false)
#      blurHandler = ->
#        oldCaretPosition = 0
#        oldSelectionLength = 0
#        if not isValid or value.length is 0
#          valueMasked = ""
#          iElement.val ""
#          scope.$apply ->
#            controller.$setViewValue ""
#
#      mouseDownUpHandler = (e) ->
#        if e.type is "mousedown"
#          iElement.bind "mouseout", mouseoutHandler
#        else
#          iElement.unbind "mouseout", mouseoutHandler
#      mouseoutHandler = ->
#        oldSelectionLength = getSelectionLength(this)
#        iElement.unbind "mouseout", mouseoutHandler
#      eventHandler = (e) ->
#        e = e or {}
#
#        # Allows more efficient minification
#        eventWhich = e.which
#        eventType = e.type
#
#        # Prevent shift and ctrl from mucking with old values
#        return  if eventWhich is 16 or eventWhich is 91
#        val = iElement.val()
#        valOld = oldValue
#        valMasked = undefined
#        valUnmasked = unmaskValue(val)
#        valUnmaskedOld = oldValueUnmasked
#        valAltered = false
#        caretPos = getCaretPosition(this) or 0
#        caretPosOld = oldCaretPosition or 0
#        caretPosDelta = caretPos - caretPosOld
#        caretPosMin = maskCaretMap[0]
#        caretPosMax = maskCaretMap[valUnmasked.length] or maskCaretMap.slice().shift()
#        selectionLenOld = oldSelectionLength or 0
#        isSelected = getSelectionLength(this) > 0
#        wasSelected = selectionLenOld > 0
#
#        # Case: Typing a character to overwrite a selection
#        isAddition = (val.length > valOld.length) or (selectionLenOld and val.length > valOld.length - selectionLenOld)
#
#        # Case: Delete and backspace behave identically on a selection
#        isDeletion = (val.length < valOld.length) or (selectionLenOld and val.length is valOld.length - selectionLenOld)
#        isSelection = (eventWhich >= 37 and eventWhich <= 40) and e.shiftKey # Arrow key codes
#        isKeyLeftArrow = eventWhich is 37
#
#        # Necessary due to "input" event not providing a key code
#        isKeyBackspace = eventWhich is 8 or (eventType isnt "keyup" and isDeletion and (caretPosDelta is -1))
#        isKeyDelete = eventWhich is 46 or (eventType isnt "keyup" and isDeletion and (caretPosDelta is 0) and not wasSelected)
#
#        # Handles cases where caret is moved and placed in front of invalid maskCaretMap position. Logic below
#        # ensures that, on click or leftward caret placement, caret is moved leftward until directly right of
#        # non-mask character. Also applied to click since users are (arguably) more likely to backspace
#        # a character when clicking within a filled input.
#        caretBumpBack = (isKeyLeftArrow or isKeyBackspace or eventType is "click") and caretPos > caretPosMin
#        oldSelectionLength = getSelectionLength(this)
#
#        # These events don't require any action
#        return  if isSelection or (isSelected and (eventType is "click" or eventType is "keyup"))
#
#        # Value Handling
#        # ==============
#
#        # User attempted to delete but raw value was unaffected--correct this grievous offense
#        if (eventType is "input") and isDeletion and not wasSelected and valUnmasked is valUnmaskedOld
#          caretPos--  while isKeyBackspace and caretPos > caretPosMin and not isValidCaretPosition(caretPos)
#          caretPos++  while isKeyDelete and caretPos < caretPosMax and maskCaretMap.indexOf(caretPos) is -1
#          charIndex = maskCaretMap.indexOf(caretPos)
#
#          # Strip out non-mask character that user would have deleted if mask hadn't been in the way.
#          valUnmasked = valUnmasked.substring(0, charIndex) + valUnmasked.substring(charIndex + 1)
#          valAltered = true
#
#        # Update values
#        valMasked = maskValue(valUnmasked)
#        oldValue = valMasked
#        oldValueUnmasked = valUnmasked
#        iElement.val valMasked
#        if valAltered
#
#          # We've altered the raw value after it's been $digest'ed, we need to $apply the new value.
#          scope.$apply ->
#            controller.$setViewValue valUnmasked
#
#
#        # Caret Repositioning
#        # ===================
#
#        # Ensure that typing always places caret ahead of typed character in cases where the first char of
#        # the input is a mask char and the caret is placed at the 0 position.
#        caretPos = caretPosMin + 1  if isAddition and (caretPos <= caretPosMin)
#        caretPos--  if caretBumpBack
#
#        # Make sure caret is within min and max position limits
#        caretPos = (if caretPos > caretPosMax then caretPosMax else (if caretPos < caretPosMin then caretPosMin else caretPos))
#
#        # Scoot the caret back or forth until it's in a non-mask position and within min/max position limits
#        caretPos += (if caretBumpBack then -1 else 1)  while not isValidCaretPosition(caretPos) and caretPos > caretPosMin and caretPos < caretPosMax
#        caretPos++  if (caretBumpBack and caretPos < caretPosMax) or (isAddition and not isValidCaretPosition(caretPosOld))
#        oldCaretPosition = caretPos
#        setCaretPosition this, caretPos
#      isValidCaretPosition = (pos) ->
#        maskCaretMap.indexOf(pos) > -1
#      getCaretPosition = (input) ->
#        if input.selectionStart isnt `undefined`
#          return input.selectionStart
#        else if document.selection
#
#          # Curse you IE
#          input.focus()
#          selection = document.selection.createRange()
#          selection.moveStart "character", -input.value.length
#          return selection.text.length
#        0
#      setCaretPosition = (input, pos) ->
#        return  if input.offsetWidth is 0 or input.offsetHeight is 0 # Input's hidden
#        if input.setSelectionRange
#          input.focus()
#          input.setSelectionRange pos, pos
#        else if input.createTextRange
#
#          # Curse you IE
#          range = input.createTextRange()
#          range.collapse true
#          range.moveEnd "character", pos
#          range.moveStart "character", pos
#          range.select()
#      getSelectionLength = (input) ->
#        return (input.selectionEnd - input.selectionStart)  if input.selectionStart isnt `undefined`
#        return (document.selection.createRange().text.length)  if document.selection
#        0
#      maskProcessed = false
#      eventsBound = false
#      maskCaretMap = undefined
#      maskPatterns = undefined
#      maskPlaceholder = undefined
#      maskComponents = undefined
#      minRequiredLength = undefined
#      value = undefined
#      valueMasked = undefined
#      isValid = undefined
#      originalPlaceholder = iAttrs.placeholder
#      originalMaxlength = iAttrs.maxlength
#      oldValue = undefined
#      oldValueUnmasked = undefined
#      oldCaretPosition = undefined
#      oldSelectionLength = undefined
#      linkOptions = {}
#      if iAttrs.uiOptions
#        linkOptions = scope.$eval("[" + iAttrs.uiOptions + "]")
#        if angular.isObject(linkOptions[0])
#          linkOptions = ((original, current) ->
#            for i of original
#              if Object::hasOwnProperty.call(original, i)
#                unless current[i]
#                  current[i] = angular.copy(original[i])
#                else
#                  angular.extend current[i], original[i]
#            current
#          )(options, linkOptions[0])
#      else
#        linkOptions = options
#      iAttrs.$observe "uiMask", initialize
#      iAttrs.$observe "placeholder", initPlaceholder
#      controller.$formatters.push formatter
#      controller.$parsers.push parser
#      iElement.bind "mousedown mouseup", mouseDownUpHandler
#
#      # https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/indexOf
#      unless Array::indexOf
#        Array::indexOf = (searchElement) -> #, fromIndex
#          "use strict"
#          throw new TypeError()  if this is null
#          t = Object(this)
#          len = t.length >>> 0
#          return -1  if len is 0
#          n = 0
#          if arguments_.length > 1
#            n = Number(arguments_[1])
#            if n isnt n # shortcut for verifying if it's NaN
#              n = 0
#            else n = (n > 0 or -1) * Math.floor(Math.abs(n))  if n isnt 0 and n isnt Infinity and n isnt -Infinity
#          return -1  if n >= len
#          k = (if n >= 0 then n else Math.max(len - Math.abs(n), 0))
#          while k < len
#            return k  if k of t and t[k] is searchElement
#            k++
#          -1
]