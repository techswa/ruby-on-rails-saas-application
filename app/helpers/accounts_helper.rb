module AccountsHelper

  def link_new_account
    link_to (t 'account.new_account'), new_account_path, class: 'btn btn-info btn-small'
  end

  def link_show_account(account)
    link_to t('account.show_account'), account_path(account), class: 'btn btn-info btn-sm'
  end

  def link_accounts
    link_to t('account.link_account_index'), accounts_path, class: 'btn btn-info btn-small'
  end

  def link_edit_account(account)
    link_to t('account.edit'), edit_account_path(account), class: 'btn btn-info btn-sm'
  end


  def contact_display(contact_name_first, contact_name_last)
    contact_combined = contact_name_first.to_s << " " << contact_name_last.to_s
  end

  def address_display(address_office_1,
                      address_office_2,
                      address_office_city,
                      address_office_state,
                      address_office_zipcode)
    address_combined = address_office_1.to_s << "<br />"
    #Supress the second line if empty
    if !address_office_2.to_s == ""
      address_combined << address_office_2.to_s << "<br />"
    end
    address_combined << address_office_city.to_s << ", " <<  address_office_state.to_s
    address_combined << " " << address_office_zipcode.to_s
    address_combined.html_safe
  end

  def mail_display(address_mail_1,
                   address_mail_2,
                   address_mail_city,
                   address_mail_state,
                   address_mail_zipcode)
    mail_combined = address_mail_1.to_s << "<br />"
    #Supress the second line if empty
    if !address_mail_2.to_s == ""
      mail_combined << address_mail_2.to_s << "<br />"
    end
    mail_combined << address_mail_city.to_s << ", " <<  address_mail_state.to_s
    mail_combined << " " << address_mail_zipcode.to_s
    mail_combined.html_safe
  end

  def phone_office_display(phone_number, extension)
    #construct phone display string
    if !extension.to_s
      # no extension
      phone_office_display = number_to_phone(phone_number, :area_code => true)
    else
      phone_office_display = number_to_phone(phone_number, :area_code => true, :extension => extension)
    end
    #
  end

  def phone_cell_display(phone_number)
    phone_cell_display = number_to_phone(phone_number, :area_code => true)
  end

end
