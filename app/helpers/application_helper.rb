module ApplicationHelper




  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end

  #Convert spaces from string and format as css Id tag
  def add_id string
    string.replace(string.gsub(' ', '-'))

    "id=#{string.downcase}"
  end

  #Replace spaces with hyphens and lower case
  #used to ad css id based upon I18n string
  def id_from_I18n(section_id)
    section_id.replace(section_id.gsub(' ', '-')).downcase
  end

end
