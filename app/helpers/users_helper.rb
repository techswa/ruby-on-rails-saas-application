module UsersHelper


  def link_add_users(account, current_user)
    #current_user.add_role :admin
    puts current_user.inspect
    puts 'Check user role for accounting'
    puts current_user.has_role? :accounting

    if current_user.has_role? :accounting
      puts 'Inside of has_role :accounting'
      puts account.inspect
      #link_to t('user.link_new_user'), new_account_user_path(account), class: 'btn btn-info btn-sm'
      link_new_account_user(account)
      #link_to 'Add manager'
      #link_to 'Add buyer'
      #link_new_account_user(account)
    end

    #if current_user.has_role? :owner
    #  link_to 'Add manager'
    #  link_to 'Add buyer'
    #end
    #if current_user.has_role? :manager
    #  link_to 'Add buyer'
    #end
  end

  def link_user_count(account, user_count)
    if user_count == 0
      link_new_account_user(account)
    else
      link_account_users(account)
    end
  end

  def link_new_account_user(account)
    link_to t('user.link_new_user'), new_account_user_path(account), class: 'btn btn-info btn-sm'
  end

  def link_account_users(account)
    link_to t('user.link_user_count'), account_users_path(account), class: 'btn btn-info btn-sm'
  end

  def link_account_user(account, user)
    #Display specific user
    link_to t('user.link_view_user'), account_user_path(account, user), class: 'btn btn-info btn-sm'
  end

  def link_edit_account_user(account, user)
    #Edit specific user
    link_to t('user.link_edit_user'), edit_account_user_path(account, user), class: 'btn btn-info btn-sm'
  end
  
  
  
  def display_role(user)
    _role = nil

    if user.has_role? :primary then _role = 'Primary' end

    if user.has_role? :manager then _role = 'Manager' end

    if user.has_role? :buyer then _role = 'Buyer' end

    if user.has_role? :unassigned then _role = 'Unassigned' end

    if user.roles.count == 0 then _role = 'Unassigned' end

   # role = user.roles
    return _role
  end



  def user_details current_user
    html = ''.html_safe
    html.safe_concat "<div>Current user: #{current_user.full_name}</div>"
    html.safe_concat "<div>Current user id: #{current_user.id}</div>"
    html.safe_concat '<div>User has the following roles:</div>'


   if current_user.has_role? :sysop
     html.safe_concat  '<div>:sysop</div>'
   elsif current_user.has_role? :support
     html.safe_concat  '<div>:support</div>'
   elsif current_user.has_role? :accounting
     html.safe_concat '<div>:accounting</div>'
   elsif current_user.has_role? :primary
     html.safe_concat '<div>:owner</div>'
   elsif current_user.has_role? :manager
     html.safe_concat '<div>:manager</div>'
   elsif current_user.has_role? :buyer
     html.safe_concat '<div>:buyer</div>'
   elsif current_user.has_role? :leo
     html.safe_concat '<div>:leo</div>'
   end
    html

end
  #  <div>Has role sysop: <%= current_user.has_role? :sysop%></div>
  #<div>Has role support: <%= current_user.has_role? :support%></div>
  #<div>Has role accounting: <%= current_user.has_role? :accounting%></div>
  #  <div>Has role owner: <%= current_user.has_role? :owner%></div>
  #<div>Has role manager: <%= current_user.has_role? :manager%></div>
  #<div>Has role buyer: <%= current_user.has_role? :buyer%></div>
  #  <div>Has role leo: <%= current_user.has_role? :leo%></div>



end