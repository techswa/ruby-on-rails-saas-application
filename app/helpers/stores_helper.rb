module StoresHelper

  def role_options
    if current_user.has_role? :primary

    end
  end

  def link_store_count(account, store_count)
    if store_count == 0
      link_new_account_store(account)
    else
      link_account_stores(account)
    end
  end

  def link_new_account_store(account)
    link_to t('account.add_store'), new_account_store_path(account), class: 'btn btn-info btn-sm'
  end

  def link_account_stores(account)
    link_to t('account.list_stores'), account_stores_path(account), class: 'btn btn-info btn-sm'
  end

  def link_account_store(account, store)
    #Display specific store
    link_to t('store.view_store'), account_store_path(account, store), class: 'btn btn-info btn-sm'
  end

  def link_edit_account_store(account, store)
    #Edit specific store
    link_to t('store.edit_store'), edit_account_store_path(account, store), class: 'btn btn-info btn-sm'
  end


  #def test
  ##<div><%= link_edit_account_store %> </div>
  #end
end
