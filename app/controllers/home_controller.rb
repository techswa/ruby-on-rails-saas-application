class HomeController < ApplicationController

  before_filter :authenticate_user!

  def index
    gon.user = current_user.slug
    gon.metals = Metal.all
    gon.states = State.all
    gon.licenses = License.all
    gon.genders = Gender.all
    gon.eyecolors = Eyecolor.all


    gon.products = Product.all
    gon.product_categories = Productcategory.all
    gon.unit_weights = Unitweight.all
    gon.unit_sizes = Unitsize.all



  end

  def welcome

  end
end
