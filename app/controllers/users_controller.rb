class UsersController < ApplicationController

  #Devise
  #before_filter :authenticate_user!

  #instantiate role filter class object in private method below
  before_filter :load_role_service

  before_filter :load_password_service, on: [:send_temp_password]

  # @account.users
  before_filter :get_account_users

  # Only allow staff roles
  before_filter :staff_only_actions?, on: [:destroy]

  #before_filter :account_priority, except: :index
  # Verify user is allowed to edit target user
  # Keeps user from hand entering user id in url

  def index
    # Only return record that belong to the same account unless system staff
    @users = filter_list_by_role(current_user)
    respond_to do |format|
      format.html
     # format.json { render json: @user }
    end
  end

  def show
    @user = @account.users.find_by_slug(params[:id])
    if @user.nil?
      redirect_to account_users_path, notice: 'No user found.'
    else
      respond_to do |format|
          format.html
          # format.json { render json: @user }
      end
    end
  end

  def new
    @user = @account.users.build
    # Only allow user to assign role that are beneath their role
    @roles = @role_service.build_allowable_roles
    # Set default role to the lowest usable
    @selected_role_id = @role_service.get_role_id_from_symbol(:buyer)

    respond_to do |format|
      format.html # new.html.erb
     # format.json { render json: @user }
    end
  end

  def create
    puts 'Entering User#create...'
    if @role_service.power_role_selected? # Check for forged role
      puts 'params: ' + params.to_s
      @user = @account.users.build(params[:user])
      puts '@user: ' + @user.inspect
      @user.password = 'password'
      @user.password_confirmation = 'password'

      respond_to do |format|
        if @user.save
          @role_service.set_user_role(@user) # Add user role
          format.html { redirect_to account_users_path, notice: "#{@user.full_name} was successfully created." }
         # format.json { render json: @user, status: :created, location: @user }

        else # Record was not saved - redisplay the form
          @roles = @role_service.build_allowable_roles
          @selected_role_id = params[:user][:role_id]
          format.html { render action: 'new' }
        end
      end
   else
     redirect_to account_user_path, :alert => 'Unable to update user, incorrect role selected.'
    end
  end

  def edit
    @user = @account.users.find_by_slug(params[:id])
    @roles = @role_service.build_allowable_roles
    #set the current role as list default
    @selected_role_id = @role_service.get_current_role_index(@user)
  end

  def update

    if @role_service.power_role_selected? # Check for forged role
      @user = @account.users.find_by_slug(params[:id])
      _params = @role_service.get_parameters

      if @user.update_attributes(_params)
        puts 'User#update:add_user_role'
        @role_service.set_user_role(@user) # Add user role
        redirect_to account_users_path, :notice => "#{@user.full_name} has been updated."
      else
        redirect_to account_user_path, :alert => 'Unable to update user.'
      end
    else
      redirect_to account_user_path, :alert => 'Unable to update user, incorrect role selected.'
    end
  end

  def destroy
    puts 'Users#destroy called'
    puts params.inspect
    #redirect_to account_users_path, notice: 'Authorized to delete records.'
    #user = User.find(params[:id])
    #unless user == current_user
    #  user.destroy
    #  redirect_to users_path, :notice => 'User deleted.'
    #else
    #  redirect_to users_path, :notice => 'Can\'t delete yourself.'
    #end
  end

  def send_temp_password
    puts params.to_s
    #if @role_service.power_role_selected?
    #@password_service.get_temp_password(params[:id])
    #User.find_by_slug(params[:id]).reset_password!('password', 'password')
    _user= User.find_by_slug(params[:id])
    puts _user.inspect
    _user.send_reset_password_instructions
    puts 'Send temp password '
    redirect_to account_users_path, :alert => "Password reset instructions have been sent to #{_user.full_name} at #{_user.email}."
    #else # Hacking attempt?
    #  redirect_to root_path, :alert => 'System error'
    #end
  end



private

  def load_role_service
    @role_service = Services::RoleService.new(current_user, params)
  end

  def load_password_service
    @password_service = Services::PasswordService.new(current_user, params)
  end

  # Code to test slug
  # Changed to get_current_account - moved to application controller
  #def get_current_account
  #  @account = Account.find_by_slug(_account_id)
  #end

  def get_account_users
    puts 'Entering Users#get_account_users ...'
    # returns the users account or redirect to root if hacked
    _account_id = params[:account_id]
    # actual, non slugged id column value
    _account_record = Account.find_by_slug(params[:account_id])

    if current_user.account_id == _account_record.id
    # Test if current_user is editing own acccount
      @account = Account.find_by_slug(_account_id)
    elsif @role_service.system_role? (current_user) #Admin, accounting or support
      @account = Account.find_by_slug(_account_id)
    else
      # #Attempted hack? User trying to edit account other than thier own.
      redirect_to root_path, :notice => 'There seems to be a problem with the account.'
      return
    end
  end

  #def account_priority
  #  # Verify user is allowed to edit target user
  #  # Keeps user from hand entering user id in url
  #  # Verifies current_user role is higher level than target of edit
  #  puts 'Entering account_priority()...'
  #  puts 'get id for current user'
  #  _current_user_role_id = @role_service.get_current_role_index(current_user)
  #  puts _current_user_role_id
  #  puts 'get target record'
  #  @user = @account.users.find_by_slug(params[:id])
  #  puts 'get target id from ' + @user.inspect
  #  _target_user_role_id = @role_service.get_current_role_index(@user)
  #  puts 'c user ' + _current_user_role_id.to_s
  #  puts 't user ' + _target_user_role_id.to_s
  #  if _current_user_role_id == _target_user_role_id #Someone is cheating
  #    puts 'first test'
  #    redirect_to root_path
  #    return
  #  elsif _current_user_role_id > _target_user_role_id
  #    puts 'second test'
  #    redirect_to root_path
  #    return
  #  end
  #  puts 'Exiting account_priority()'
  #end

  #def set_user_role(user)
  #  puts 'Entered User#add_user_role with ' + user.inspect
  #  @role_service.clear_all_roles(user)
  #
  #  #Just get one role
  #  _new_role_name = @role_service.get_new_role_name
  #  puts 'role to add ' + _new_role_name
  #  user.add_role _new_role_name.to_sym
  #end

  def filter_list_by_role(current_user)
    # Only display records for roles that are managed
    _current_user = current_user
    if _current_user.has_role? :admin
      @account.users.admin_roles_managed

    elsif _current_user.has_role? :accounting
      #@users = @account.users.accounting_roles_managed
      @account.users.accounting_roles_managed

    elsif _current_user.has_role? :support
      @account.users.support_roles_managed

    elsif _current_user.has_role? :primary
      @account.users.primary_roles_managed

    elsif _current_user.has_role? :manager
      @account.users.manager_roles_managed

    else
      @account.users.no_roles_managed
    end
  end

  # Used to restrict controller actions to staff only
  def staff_only_actions?
    if (current_user.has_role? :accounting) || (current_user.has_role? :support)
      puts 'Users#staff_action_only = true'
      true
    else
      puts 'Users#staff_action_only = false'
      #redirect_to account_users_path, notice: 'Unauthorized to delete records.'
      #return
      #false
    end
  end

end