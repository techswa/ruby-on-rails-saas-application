class UnitweightsController < ApplicationController
  # GET /unitweights
  # GET /unitweights.json
  def index
    @unitweights = Unitweight.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @unitweights }
    end
  end

  # GET /unitweights/1
  # GET /unitweights/1.json
  def show
    @unitweight = Unitweight.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @unitweight }
    end
  end

  # GET /unitweights/new
  # GET /unitweights/new.json
  def new
    @unitweight = Unitweight.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @unitweight }
    end
  end

  # GET /unitweights/1/edit
  def edit
    @unitweight = Unitweight.find(params[:id])
  end

  # POST /unitweights
  # POST /unitweights.json
  def create
    @unitweight = Unitweight.new(params[:unitweight])

    respond_to do |format|
      if @unitweight.save
        format.html { redirect_to @unitweight, notice: 'Unitweight was successfully created.' }
        format.json { render json: @unitweight, status: :created, location: @unitweight }
      else
        format.html { render action: "new" }
        format.json { render json: @unitweight.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /unitweights/1
  # PUT /unitweights/1.json
  def update
    @unitweight = Unitweight.find(params[:id])

    respond_to do |format|
      if @unitweight.update_attributes(params[:unitweight])
        format.html { redirect_to @unitweight, notice: 'Unitweight was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @unitweight.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /unitweights/1
  # DELETE /unitweights/1.json
  def destroy
    @unitweight = Unitweight.find(params[:id])
    @unitweight.destroy

    respond_to do |format|
      format.html { redirect_to unitweights_url }
      format.json { head :no_content }
    end
  end
end
