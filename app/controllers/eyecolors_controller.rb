class EyecolorsController < ApplicationController
  # GET /eyecolors
  # GET /eyecolors.json
  def index
    @eyecolors = Eyecolor.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @eyecolors }
    end
  end

  # GET /eyecolors/1
  # GET /eyecolors/1.json
  def show
    @eyecolor = Eyecolor.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @eyecolor }
    end
  end

  # GET /eyecolors/new
  # GET /eyecolors/new.json
  def new
    @eyecolor = Eyecolor.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @eyecolor }
    end
  end

  # GET /eyecolors/1/edit
  def edit
    @eyecolor = Eyecolor.find(params[:id])
  end

  # POST /eyecolors
  # POST /eyecolors.json
  def create
    @eyecolor = Eyecolor.new(params[:eyecolor])

    respond_to do |format|
      if @eyecolor.save
        format.html { redirect_to @eyecolor, notice: 'Eyecolor was successfully created.' }
        format.json { render json: @eyecolor, status: :created, location: @eyecolor }
      else
        format.html { render action: "new" }
        format.json { render json: @eyecolor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /eyecolors/1
  # PUT /eyecolors/1.json
  def update
    @eyecolor = Eyecolor.find(params[:id])
    respond_to do |format|
      params[:eyecolor].delete(:email_confirmation) #This isn't stored
      if @eyecolor.update_attributes(params[:eyecolor])
        format.html { redirect_to @eyecolor, notice: 'Eyecolor was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @eyecolor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /eyecolors/1
  # DELETE /eyecolors/1.json
  def destroy
    @eyecolor = Eyecolor.find(params[:id])
    @eyecolor.destroy

    respond_to do |format|
      format.html { redirect_to eyecolors_url }
      format.json { head :no_content }
    end
  end
end
