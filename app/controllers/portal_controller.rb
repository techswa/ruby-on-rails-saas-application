class PortalController < ApplicationController
  #used by accounting and support

  def index
    @accounts = AccountDecorator.decorate_collection(Account.all)
  end

  def new
  end

  def show
  end
end
