class StaffController < ApplicationController

  #Used by clients to manage their staff
  #Uses Account.id

  def newClerk
  end

  def newManager
  end

  def createClerk
  end

  def createManager
  end

  def indexClerks
  end

  def indexManagers
  end

  def editClerk
  end

  def destroy
    #Doesn't delete user, but sends them to the archive

  end
end
