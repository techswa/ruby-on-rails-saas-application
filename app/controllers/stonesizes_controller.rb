class StonesizesController < ApplicationController
  # GET /stonesizes
  # GET /stonesizes.json
  def index
    @stonesizes = Stonesize.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stonesizes }
    end
  end

  # GET /stonesizes/1
  # GET /stonesizes/1.json
  def show
    @stonesize = Stonesize.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stonesize }
    end
  end

  # GET /stonesizes/new
  # GET /stonesizes/new.json
  def new
    @stonesize = Stonesize.new
    #set defaults
    @stonesize.language_id = 1
    @stonesize.sort = 1

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stonesize }
    end
  end

  # GET /stonesizes/1/edit
  def edit
    @stonesize = Stonesize.find(params[:id])
  end

  # POST /stonesizes
  # POST /stonesizes.json
  def create
    @stonesize = Stonesize.new(params[:stonesize])

    respond_to do |format|
      if @stonesize.save
        format.html { redirect_to @stonesize, notice: 'Stonesize was successfully created.' }
        format.json { render json: @stonesize, status: :created, location: @stonesize }
      else
        format.html { render action: "new" }
        format.json { render json: @stonesize.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /stonesizes/1
  # PUT /stonesizes/1.json
  def update
    @stonesize = Stonesize.find(params[:id])

    respond_to do |format|
      if @stonesize.update_attributes(params[:stonesize])
        format.html { redirect_to @stonesize, notice: 'Stonesize was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stonesize.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stonesizes/1
  # DELETE /stonesizes/1.json
  def destroy
    @stonesize = Stonesize.find(params[:id])
    @stonesize.destroy

    respond_to do |format|
      format.html { redirect_to stonesizes_url }
      format.json { head :no_content }
    end
  end
end
