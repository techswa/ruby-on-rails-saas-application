class StonetypesController < ApplicationController
  # GET /stonetypes
  # GET /stonetypes.json
  def index
    @stonetypes = Stonetype.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stonetypes }
    end
  end

  # GET /stonetypes/1
  # GET /stonetypes/1.json
  def show
    @stonetype = Stonetype.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stonetype }
    end
  end

  # GET /stonetypes/new
  # GET /stonetypes/new.json
  def new
    @stonetype = Stonetype.new
    #Set defaults
    @stonetype.language_id = 1
    @stonetype.sort = 1

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stonetype }
    end
  end

  # GET /stonetypes/1/edit
  def edit
    @stonetype = Stonetype.find(params[:id])
  end

  # POST /stonetypes
  # POST /stonetypes.json
  def create
    @stonetype = Stonetype.new(params[:stonetype])

    respond_to do |format|
      if @stonetype.save
        format.html { redirect_to @stonetype, notice: 'Stonetype was successfully created.' }
        format.json { render json: @stonetype, status: :created, location: @stonetype }
      else
        format.html { render action: "new" }
        format.json { render json: @stonetype.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /stonetypes/1
  # PUT /stonetypes/1.json
  def update
    @stonetype = Stonetype.find(params[:id])

    respond_to do |format|
      if @stonetype.update_attributes(params[:stonetype])
        format.html { redirect_to @stonetype, notice: 'Stonetype was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stonetype.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stonetypes/1
  # DELETE /stonetypes/1.json
  def destroy
    @stonetype = Stonetype.find(params[:id])
    @stonetype.destroy

    respond_to do |format|
      format.html { redirect_to stonetypes_url }
      format.json { head :no_content }
    end
  end
end
