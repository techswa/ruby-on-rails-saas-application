class PaymethodsController < ApplicationController
  # GET /paymethods
  # GET /paymethods.json
  def index
    @paymethods = Paymethod.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @paymethods }
    end
  end

  # GET /paymethods/1
  # GET /paymethods/1.json
  def show
    @paymethod = Paymethod.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @paymethod }
    end
  end

  # GET /paymethods/new
  # GET /paymethods/new.json
  def new
    @paymethod = Paymethod.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @paymethod }
    end
  end

  # GET /paymethods/1/edit
  def edit
    @paymethod = Paymethod.find(params[:id])
  end

  # POST /paymethods
  # POST /paymethods.json
  def create
    @paymethod = Paymethod.new(params[:paymethod])

    respond_to do |format|
      if @paymethod.save
        format.html { redirect_to @paymethod, notice: 'Paymethod was successfully created.' }
        format.json { render json: @paymethod, status: :created, location: @paymethod }
      else
        format.html { render action: "new" }
        format.json { render json: @paymethod.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /paymethods/1
  # PUT /paymethods/1.json
  def update
    @paymethod = Paymethod.find(params[:id])

    respond_to do |format|
      if @paymethod.update_attributes(params[:paymethod])
        format.html { redirect_to @paymethod, notice: 'Paymethod was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @paymethod.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /paymethods/1
  # DELETE /paymethods/1.json
  def destroy
    @paymethod = Paymethod.find(params[:id])
    @paymethod.destroy

    respond_to do |format|
      format.html { redirect_to paymethods_url }
      format.json { head :no_content }
    end
  end
end
