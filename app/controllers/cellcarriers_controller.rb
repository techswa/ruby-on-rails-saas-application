class CellcarriersController < ApplicationController
  # GET /cellcarriers
  # GET /cellcarriers.json
  def index
    @cellcarriers = Cellcarrier.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @cellcarriers }
    end
  end

  # GET /cellcarriers/1
  # GET /cellcarriers/1.json
  def show
    @cellcarrier = Cellcarrier.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @cellcarrier }
    end
  end

  # GET /cellcarriers/new
  # GET /cellcarriers/new.json
  def new
    @cellcarrier = Cellcarrier.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @cellcarrier }
    end
  end

  # GET /cellcarriers/1/edit
  def edit
    @cellcarrier = Cellcarrier.find(params[:id])
  end

  # POST /cellcarriers
  # POST /cellcarriers.json
  def create
    @cellcarrier = Cellcarrier.new(params[:cellcarrier])

    respond_to do |format|
      if @cellcarrier.save
        format.html { redirect_to @cellcarrier, notice: 'Cellcarrier was successfully created.' }
        format.json { render json: @cellcarrier, status: :created, location: @cellcarrier }
      else
        format.html { render action: "new" }
        format.json { render json: @cellcarrier.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /cellcarriers/1
  # PUT /cellcarriers/1.json
  def update
    @cellcarrier = Cellcarrier.find(params[:id])

    respond_to do |format|
      if @cellcarrier.update_attributes(params[:cellcarrier])
        format.html { redirect_to @cellcarrier, notice: 'Cellcarrier was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @cellcarrier.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cellcarriers/1
  # DELETE /cellcarriers/1.json
  def destroy
    @cellcarrier = Cellcarrier.find(params[:id])
    @cellcarrier.destroy

    respond_to do |format|
      format.html { redirect_to cellcarriers_url }
      format.json { head :no_content }
    end
  end
end
