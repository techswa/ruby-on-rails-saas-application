class AuthorizesController < ApplicationController


 # before_filter :verify_account, :only => :create
 # before_filter :account_details

  #before_filter :load_authorize_service
  #before_filter :account_authorized?

  # GET /authorizes
  # GET /authorizes.json
  def index
    @authorizes = Authorize.all

    #auth = Services::Authorize.new(current_user, app_store_id)
    #
    #if auth.flash_messages
    #  puts 'flash count'
    #  flash[:notice] = auth.flash_messages[0]
    #  flash[:notice] = auth.flash_messages[1]
    #  puts auth.flash_messages
    #end
    #r = Services::Roles.new('test role')



    #auth.puts_account_details
    #auth.account_authorized?


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @authorizes }
    end
  end

  # GET /authorizes/1
  # GET /authorizes/1.json
  def show
    @authorize = Authorize.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @authorize }
    end
  end

  # GET /authorizes/new
  # GET /authorizes/new.json
  def new
    @authorize = Authorize.new
    @authorize.user_id = current_user.id
    @license = '1'
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @authorize }
    end
  end

  # GET /authorizes/1/edit
  def edit
    @authorize = Authorize.find(params[:id])
  end

  # POST /authorizes
  # POST /authorizes.json
  def create
    @authorize = Authorize.new(params[:authorize])

    respond_to do |format|
      if @authorize.save
        format.html { redirect_to @authorize, notice: 'Authorize was successfully created.' }
        format.json { render json: @authorize, status: :created, location: @authorize }
      else
        format.html { render action: "new" }
        format.json { render json: @authorize.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /authorizes/1
  # PUT /authorizes/1.json
  def update
    @authorize = Authorize.find(params[:id])

    respond_to do |format|
      if @authorize.update_attributes(params[:authorize])
        format.html { redirect_to @authorize, notice: 'Authorize was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @authorize.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /authorizes/1
  # DELETE /authorizes/1.json
  def destroy
    @authorize = Authorize.find(params[:id])
    @authorize.destroy

    respond_to do |format|
      format.html { redirect_to authorizes_url }
      format.json { head :no_content }
    end
  end

private


  def load_authorize_service(service = AuthorizeService.new)
    @authorize_service ||= service
  end

  ##Is the account paid through today?
  #def account_authorized?
  #  #@account_status = Authorize.where("store_id = ?", @store.id)
  #  @expiration_date = @authorize_details[0].expiration_date
  #  puts @expiration_date
  #  puts Date.today
  #  @days_left = (@expiration_date - Date.today).to_i
  #  puts @days_left
  #
  #  if @days_left > 60
  #    true
  #  elsif @days_left < 60 && @days_left > 15
  #    true
  #    puts "License expiring soon, you have #{@days_left} days left."
  #  elsif @days_left < 15 && @days_left > 7
  #    true
  #    puts "Notice: License expiring soon, you have #{@days_left} days left."
  #    flash[:warning] = "License expiring soon, you have #{@days_left} days left."
  #
  #  elsif @days_left < 8
  #    true
  #    puts "Alert: License expiring soon, you have #{@days_left} days left."
  #    flash[:notice] = "License expiring soon, you only have #{@days_left} days left."
  #  else @days_left < 0
  #    false
  #    puts 'Your annual license has expired. Please call to renew.'
  #    #Display alert message
  #
  #  end
  #
  #
  #end

  #def account_details
  #  @user_id = current_user.id
  #  @store = Store.find(app_store_id)
  #  @authorize_details = Authorize.where("store_id = ?", @store.id).active
  #  #puts @store.inspect
  #  #puts @authorize_details.inspect
  #  #puts "current_user.id #{@user_id}"
  #  #puts "store.id #{@store.id}"
  #  #puts "store license #{@store.license}"
  #end


  def verify_account

    false
  end
end
