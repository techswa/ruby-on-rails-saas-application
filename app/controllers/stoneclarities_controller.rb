class StoneclaritiesController < ApplicationController
  # GET /stoneclarities
  # GET /stoneclarities.json
  def index
    @stoneclarities = Stoneclarity.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stoneclarities }
    end
  end

  # GET /stoneclarities/1
  # GET /stoneclarities/1.json
  def show
    @stoneclarity = Stoneclarity.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stoneclarity }
    end
  end

  # GET /stoneclarities/new
  # GET /stoneclarities/new.json
  def new
    @stoneclarity = Stoneclarity.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stoneclarity }
    end
  end

  # GET /stoneclarities/1/edit
  def edit
    @stoneclarity = Stoneclarity.find(params[:id])
  end

  # POST /stoneclarities
  # POST /stoneclarities.json
  def create
    @stoneclarity = Stoneclarity.new(params[:stoneclarity])

    respond_to do |format|
      if @stoneclarity.save
        format.html { redirect_to @stoneclarity, notice: 'Stoneclarity was successfully created.' }
        format.json { render json: @stoneclarity, status: :created, location: @stoneclarity }
      else
        format.html { render action: "new" }
        format.json { render json: @stoneclarity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /stoneclarities/1
  # PUT /stoneclarities/1.json
  def update
    @stoneclarity = Stoneclarity.find(params[:id])

    respond_to do |format|
      if @stoneclarity.update_attributes(params[:stoneclarity])
        format.html { redirect_to @stoneclarity, notice: 'Stoneclarity was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stoneclarity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stoneclarities/1
  # DELETE /stoneclarities/1.json
  def destroy
    @stoneclarity = Stoneclarity.find(params[:id])
    @stoneclarity.destroy

    respond_to do |format|
      format.html { redirect_to stoneclarities_url }
      format.json { head :no_content }
    end
  end
end
