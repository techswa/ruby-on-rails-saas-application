class ApplicationController < ActionController::Base
  protect_from_forgery

  #Makes the store id available to all controllers
  #before_filter :app_store_id



  rescue_from CanCan::AccessDenied do |exception|
    redirect_to :back, :alert => exception.message
  end

private
  ##Currently logged in store
  #def app_store_id
  #  1
  #end



end

