class StonecolorsController < ApplicationController
  # GET /stonecolors
  # GET /stonecolors.json
  def index
    @stonecolors = Stonecolor.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stonecolors }
    end
  end

  # GET /stonecolors/1
  # GET /stonecolors/1.json
  def show
    @stonecolor = Stonecolor.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stonecolor }
    end
  end

  # GET /stonecolors/new
  # GET /stonecolors/new.json
  def new
    @stonecolor = Stonecolor.new

    #Set defaults
    @stonecolor.language_id = 1

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stonecolor }
    end
  end

  # GET /stonecolors/1/edit
  def edit
    @stonecolor = Stonecolor.find(params[:id])
  end

  # POST /stonecolors
  # POST /stonecolors.json
  def create
    @stonecolor = Stonecolor.new(params[:stonecolor])

    respond_to do |format|
      if @stonecolor.save
        format.html { redirect_to @stonecolor, notice: 'Stonecolor was successfully created.' }
        format.json { render json: @stonecolor, status: :created, location: @stonecolor }
      else
        format.html { render action: "new" }
        format.json { render json: @stonecolor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /stonecolors/1
  # PUT /stonecolors/1.json
  def update
    @stonecolor = Stonecolor.find(params[:id])

    respond_to do |format|
      if @stonecolor.update_attributes(params[:stonecolor])
        format.html { redirect_to @stonecolor, notice: 'Stonecolor was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stonecolor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stonecolors/1
  # DELETE /stonecolors/1.json
  def destroy
    @stonecolor = Stonecolor.find(params[:id])
    @stonecolor.destroy

    respond_to do |format|
      format.html { redirect_to stonecolors_url }
      format.json { head :no_content }
    end
  end
end
