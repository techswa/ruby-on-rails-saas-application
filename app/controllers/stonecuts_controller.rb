class StonecutsController < ApplicationController
  # GET /stonecuts
  # GET /stonecuts.json
  def index
    @stonecuts = Stonecut.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stonecuts }
    end
  end

  # GET /stonecuts/1
  # GET /stonecuts/1.json
  def show
    @stonecut = Stonecut.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stonecut }
    end
  end

  # GET /stonecuts/new
  # GET /stonecuts/new.json
  def new
    @stonecut = Stonecut.new
    #set defaults
    @stonecut.language_id = 1
    @stonecut.sort = 1
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stonecut }
    end
  end

  # GET /stonecuts/1/edit
  def edit
    @stonecut = Stonecut.find(params[:id])
  end

  # POST /stonecuts
  # POST /stonecuts.json
  def create
    @stonecut = Stonecut.new(params[:stonecut])

    respond_to do |format|
      if @stonecut.save
        format.html { redirect_to @stonecut, notice: 'Stonecut was successfully created.' }
        format.json { render json: @stonecut, status: :created, location: @stonecut }
      else
        format.html { render action: "new" }
        format.json { render json: @stonecut.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /stonecuts/1
  # PUT /stonecuts/1.json
  def update
    @stonecut = Stonecut.find(params[:id])

    respond_to do |format|
      if @stonecut.update_attributes(params[:stonecut])
        format.html { redirect_to @stonecut, notice: 'Stonecut was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stonecut.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stonecuts/1
  # DELETE /stonecuts/1.json
  def destroy
    @stonecut = Stonecut.find(params[:id])
    @stonecut.destroy

    respond_to do |format|
      format.html { redirect_to stonecuts_url }
      format.json { head :no_content }
    end
  end
end
