class ManagersController < ApplicationController

  #Used by Owners to manage Managers buyers

  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
  end

  def update
  end

  def destroy
  end
end
