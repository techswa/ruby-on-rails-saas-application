class BuyersController < ApplicationController

  #Used by Managers to manage buyers

  before_filter :scope_by_account

  def index
    @buyers = User.where("account_id=?", @account)

  end

  def show
  end

  def new
  end

  def edit
  end

  def create
  end

  def update
  end

  def destroy
  end


private

  def scope_by_account
    @account = current_user.account_id
    # Temp testing
    @account = 3
  end

end
