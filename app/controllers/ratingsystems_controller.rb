class RatingsystemsController < ApplicationController
  # GET /ratingsystems
  # GET /ratingsystems.json
  def index
    @ratingsystems = Ratingsystem.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ratingsystems }
    end
  end

  # GET /ratingsystems/1
  # GET /ratingsystems/1.json
  def show
    @ratingsystem = Ratingsystem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ratingsystem }
    end
  end

  # GET /ratingsystems/new
  # GET /ratingsystems/new.json
  def new
    @ratingsystem = Ratingsystem.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ratingsystem }
    end
  end

  # GET /ratingsystems/1/edit
  def edit
    @ratingsystem = Ratingsystem.find(params[:id])
  end

  # POST /ratingsystems
  # POST /ratingsystems.json
  def create
    @ratingsystem = Ratingsystem.new(params[:ratingsystem])

    respond_to do |format|
      if @ratingsystem.save
        format.html { redirect_to @ratingsystem, notice: 'Ratingsystem was successfully created.' }
        format.json { render json: @ratingsystem, status: :created, location: @ratingsystem }
      else
        format.html { render action: "new" }
        format.json { render json: @ratingsystem.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /ratingsystems/1
  # PUT /ratingsystems/1.json
  def update
    @ratingsystem = Ratingsystem.find(params[:id])

    respond_to do |format|
      if @ratingsystem.update_attributes(params[:ratingsystem])
        format.html { redirect_to @ratingsystem, notice: 'Ratingsystem was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ratingsystem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ratingsystems/1
  # DELETE /ratingsystems/1.json
  def destroy
    @ratingsystem = Ratingsystem.find(params[:id])
    @ratingsystem.destroy

    respond_to do |format|
      format.html { redirect_to ratingsystems_url }
      format.json { head :no_content }
    end
  end
end
