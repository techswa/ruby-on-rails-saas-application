class UnitsizesController < ApplicationController
  # GET /unitsizes
  # GET /unitsizes.json
  def index
    @unitsizes = Unitsize.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @unitsizes }
    end
  end

  # GET /unitsizes/1
  # GET /unitsizes/1.json
  def show
    @unitsize = Unitsize.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @unitsize }
    end
  end

  # GET /unitsizes/new
  # GET /unitsizes/new.json
  def new
    @unitsize = Unitsize.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @unitsize }
    end
  end

  # GET /unitsizes/1/edit
  def edit
    @unitsize = Unitsize.find(params[:id])
  end

  # POST /unitsizes
  # POST /unitsizes.json
  def create
    @unitsize = Unitsize.new(params[:unitsize])

    respond_to do |format|
      if @unitsize.save
        format.html { redirect_to @unitsize, notice: 'Unitsize was successfully created.' }
        format.json { render json: @unitsize, status: :created, location: @unitsize }
      else
        format.html { render action: "new" }
        format.json { render json: @unitsize.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /unitsizes/1
  # PUT /unitsizes/1.json
  def update
    @unitsize = Unitsize.find(params[:id])

    respond_to do |format|
      if @unitsize.update_attributes(params[:unitsize])
        format.html { redirect_to @unitsize, notice: 'Unitsize was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @unitsize.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /unitsizes/1
  # DELETE /unitsizes/1.json
  def destroy
    @unitsize = Unitsize.find(params[:id])
    @unitsize.destroy

    respond_to do |format|
      format.html { redirect_to unitsizes_url }
      format.json { head :no_content }
    end
  end
end
