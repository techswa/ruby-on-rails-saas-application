class StoresController < ApplicationController



  #Unauthorized index request? Redirects to index if the case.
  #before_filter :verify_index_key?, :only => [:index]

 # before_filter :verify_store_key?, :except => [:new, :create]
  before_filter :get_account

  # GET /storest
  # GET /stores.json
  def index



    #@stores = Store.where('account_id = ?', params[:account_id])  #where(:account_id => 1)
    @stores = @account.stores
    #@account_number = @account.number
    #@account_name = @account.name
    #@account_city = @account.city

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stores }
    end
  end


  # GET /stores/1
  # GET /stores/1.json
  def show
    @store = @account.stores.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @store }
    end
  end

  # GET /stores/new
  # GET /stores/new.json
  def new
    #@account = Account.find(params[:account_id])
    @store = @account.stores.build

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @store }
    end
  end


  # POST /stores
  # POST /stores.json
  def create
    @store = @account.stores.build(params[:store])
    @store.key = generate_store_key

    #Created in create action so it cant be tampered with

    respond_to do |format|
      if @store.save
        format.html { redirect_to @store, notice: 'Store was successfully created.' }
        format.json { render json: @store, status: :created, location: @store }
      else
        format.html { render action: 'new' }
        format.json { render json: @store.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /stores/1/edit
  def edit
    @store = @account.stores.find(params[:id])
    #@account = @store.account_id

  end


  # PUT /stores/1
  # PUT /stores/1.json
  def update
    @store = @account.stores.find(params[:id])

    respond_to do |format|
      if @store.update_attributes(params[:store])
        format.html { redirect_to account_stores_url(@store), notice: 'Store was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @store.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stores/1
  # DELETE /stores/1.json
  def destroy
    @store = @account.stores.find(params[:id])
    @store.destroy

    respond_to do |format|
      format.html { redirect_to stores_url }
      format.json { head :no_content }
    end
  end

  def generate_store_key
    SecureRandom.uuid
  end

  def set_session_account_key(account_id)
    #Store record key in session for security
    @record = Store.find(account_id)
    session[:store_key] = @record.key
  end

  def get_session_store_key
    session[:store_key]
  end

  def store_session_key_match?(record_id)
    #Does the account key submitted match the requested record's key
    stored_key = get_session_store_key
    puts 'Verify key: ' + params.to_s
    #Does user have key to use record?
    match = Store.where("id = ? AND key = ?", record_id, stored_key).first
    if match.nil?
      #Key does not match the record
      flash[:alert] = "Unauthorized store key."
      #redirect_to accounts_path
      false
    else
      flash[:notice] = "Store key is authorized."
      true
    end
  end

private

  def get_account
    @account = Account.find(params[:account_id])
  end

end
