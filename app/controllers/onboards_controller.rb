class OnboardsController < ApplicationController
  def new
    @onboard = OnboardForm.new
  end

  def create
    @onboard = OnboardForm.new(params[:onboard])
    respond_to do |format|
      #if @onboard.submit(params[:onboard])
      if @onboard.save
        puts 'redirect data: ' + @onboard.inspect
        puts '@onboard params: ' + @onboard.params.to_s
         format.html { redirect_to @onboard,  notice: 'Record created' }
      else
        format.html { render action: 'new', notice: 'Record creation failed.' }
      end
    end

  end

  def show

  end

  def edit
  end
end
