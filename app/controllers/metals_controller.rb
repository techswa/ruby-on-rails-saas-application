class MetalsController < ApplicationController
 # before_filter :get_user, :only => [:index, :new, :edit]
 # before_filter :accessible_roles, :only => [:show, :edit, :show, :update, :create]
 # load_and_authorize_resource :only => [:show, :new, :destroy, :edit, :update]

  # GET /metals
  # GET /metals.json
  def index
    puts 'Metals#index ' + params.to_s
    puts 'Session ' + session.inspect
    @metals = Metal.sorted.all #Metal.accessible_by(current_ability, :index).all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @metals}
    end
  end

  # GET /metals/1
  # GET /metals/1.json
  def show
    @metal = Metal.find(params[:id])
    #@metal = Metal.accessible_by(current_ability, :show).find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @metal }
    end
  end

  # GET /metals/new
  # GET /metals/new.json
  def new
    @metal = Metal.new
    #Set defaults
    @metal.sort = 0
    @metal.language_id = 1

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @metal }
    end
  end

  # GET /metals/1/edit
  def edit
    @metal = Metal.find(params[:id])
  end

  # POST /metals
  # POST /metals.json
  def create
    @metal = Metal.new(params[:metal])

    respond_to do |format|
      if @metal.save
        format.html { redirect_to @metal, notice: 'Metal was successfully created.' }
        format.json { render json: @metal, status: :created, location: @metal }
      else
        format.html { render action: "new" }
        format.json { render json: @metal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /metals/1
  # PUT /metals/1.json
  def update
    @metal = Metal.find(params[:id])

    respond_to do |format|
      if @metal.update_attributes(params[:metal])
        format.html { redirect_to @metal, notice: 'Metal was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @metal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /metals/1
  # DELETE /metals/1.json
  def destroy
    @metal = Metal.find(params[:id])
    @metal.destroy

    respond_to do |format|
      format.html { redirect_to metals_url }
      format.json { head :no_content }
    end
  end

  #Used for security
  def accessible_roles
    @accessible_roles = Role.accessible_by(current_ability, :read)
  end
  def get_user
    @current_user = current_user
  end
end
