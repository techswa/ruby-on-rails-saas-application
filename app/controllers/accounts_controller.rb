#include ActionView::Helpers::NumberHelper

# Manages Accounts information for client

class AccountsController < ApplicationController

  #Allow cancan to manage access
  #load_and_authorize_resource

  rescue_from ActiveRecord::RecordNotFound do
    render :not_found
  end

  before_filter :staff_only_actions # All actions

  #Unauthorized record request? Redirects to index if the case.
  #before_filter :verify_key?, :except => [:index, :new, :create]

  def index
    @accounts = Account.all
  end

  def new
    @account = Account.new
  end

  def create
    @account = Account.new(params[:account])

    respond_to do |format|
      if @account.save
        redirect_to @account, notice: t('message.record_saved')
      else
        #flash[:error] = @account.address_office_state_id
        flash[:error] =  @account.errors.full_messages.to_sentence
        render :new
      end
    end
  end

  def show
    @account = Account.find_by_slug(params[:id])
    @store_count = 1 #Store.where("account_id = ?", @account.id).count
    @user_count = 2 #User.where("account_id = ?", @account.id).count
  end

  def edit
    @account = Account.find_by_slug(params[:id])
  end

  def update
    @account = Account.find_by_slug(params[:id])

    if @account.update_attributes(params[:account])
      redirect_to @account, notice: t('message.successfully_updated')
    else
        render :edit
    end
  end



private
  def generate_account_key
    SecureRandom.uuid
  end

  def set_session_account_key(account_id)
    #Store record key in session for security
    @record = Account.find(account_id)
    session[:account_key] = @record.key
  end

  def get_session_account_key
    session[:account_key]
  end

  def account_session_key_match?(record_id)
    #Does the account key submitted match the requested record's key
    stored_key = get_session_account_key
    puts 'Verify key: ' + params.to_s
    #Does user have key to use record?
    key = Account.where("id = ? AND key = ?", record_id, stored_key).first
    if key.nil?
      #Key does not match the record
      flash[:alert] = "Unauthorized key use."
      #redirect_to accounts_path
      false
    else
      flash[:notice] = "Key is authorized."
      true
    end
  end

  # Used to restrict controller actions to staff only
  def staff_only_actions
    if (current_user.has_role? :accounting) || (current_user.has_role? :support)
      true
    else
      redirect_to root_path, notice: 'No matching account was found.'
    end
  end

end
