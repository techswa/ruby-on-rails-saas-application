class CommentsController < ApplicationController

  before_filter :load_commentable

  # GET /comments
  # GET /comments.json
  def index

    #Retrieve for singluar model
    #@commentable = Account.find(params[:account_id])
    @comments = @commentable.comments

    respond_to do |format|
      format.html #index.html.erb
      format.json { render json: @comments }
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    @comment = Comment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comment }
    end
  end

  # GET /comments/new
  # GET /comments/new.json
  def new
    @comment = @commentable.comments.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @comment }
    end
  end

  # GET /comments/1/edit
  def edit
    @comment = @commentable#.comments
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = @commentable.comments.new(params[:comment])

    respond_to do |format|
      if @comment.save
        format.html { redirect_to [@commentable, :comments], notice: 'Comment was successfully created.' }
        #format.json { render json: @comment, status: :created, location: @comment }
      else
        format.html { render action: "new" }
        #format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /comments/1
  # PUT /comments/1.json
  def update
    @comment = Comment.find(params[:id])

    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to comments_url }
      format.json { head :no_content }
    end
  end

private

  def load_commentable
    #resource, id = request.path.split('/')[1,2]
    #@commentable = resource.singularize.classify.constantize.find(id) # Account.find(1)
    def load_commentable
      if params[:account_id]
        @commentable = Account.find(params[:account_id])
      elsif params[:store_id]
        @commentable = Store.find(params[:store_id])
      end
    end
  end
end
