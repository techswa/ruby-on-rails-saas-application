class BasemetalsController < ApplicationController
  # GET /basemetals
  # GET /basemetals.json
  def index
    @basemetals = Basemetal.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @basemetals }
    end
  end

  # GET /basemetals/1
  # GET /basemetals/1.json
  def show
    @basemetal = Basemetal.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @basemetal }
    end
  end

  # GET /basemetals/new
  # GET /basemetals/new.json
  def new
    @basemetal = Basemetal.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @basemetal }
    end
  end

  # GET /basemetals/1/edit
  def edit
    @basemetal = Basemetal.find(params[:id])
  end

  # POST /basemetals
  # POST /basemetals.json
  def create
    @basemetal = Basemetal.new(params[:basemetal])

    respond_to do |format|
      if @basemetal.save
        format.html { redirect_to @basemetal, notice: 'Basemetal was successfully created.' }
        format.json { render json: @basemetal, status: :created, location: @basemetal }
      else
        format.html { render action: "new" }
        format.json { render json: @basemetal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /basemetals/1
  # PUT /basemetals/1.json
  def update
    @params = params.to_yaml
    @basemetal = Basemetal.find(params[:id])

    respond_to do |format|
      if @basemetal.update_attributes(params[:basemetal])
        format.html { redirect_to @basemetal, notice: 'Basemetal was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @basemetal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /basemetals/1
  # DELETE /basemetals/1.json
  def destroy
    @basemetal = Basemetal.find(params[:id])
    @basemetal.destroy

    respond_to do |format|
      format.html { redirect_to basemetals_url }
      format.json { head :no_content }
    end
  end
end
