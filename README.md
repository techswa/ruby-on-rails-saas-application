#Fine Metal Buyer#
This was a multi-tenant, multi-user SasS application.  It was developed as a retail store POS platform for reporting precious metal purchases to law enforcement as required by a recently passed law at the time.

##Project brief:##
Texas had recently passed a new law requiring all individuals and companies that purchased precious metals from consumers to register their locations and report all purchases to both the state and local law enforcement.  The law was passed after the Pawnshop association, the traditional conduit for people to sell their gold jewelry lobbied the state.  The state "Pawn Shop" laws require pawn shops to report all purchases to local and state agencies, then hold the inventory for at least 30 days before liquidating it.  The pop-up "We Buy Gold" enterprises were not held to these same laws, thus were able to purchase from consumers and sell the gold to metal refineries on the same day.  This greatly reduced the capital necessary to purchase metals and removed the uncertainty of the spot market price of the disposition of the purchases, thus allowing for a higher purchase price from the consumer.  This created a very lopped-sided market for the pawn shops, not unlike the taxi vs. Uber issue.

##Project details:##
This project was created in C# and Silverlight, but that platform was abandoned due to Microsoft removing support for Silverlight towards the end of the project.

This was a multi-tenant, multi-user SasS application.  It consisted of 35 tables hosted on a PostgreSQL server.

This application is a port/refactor/rewrite of the original C# code base, but since this was my first time to use Rails and was working alone, I had nothing but Railscasts and Stackover Flow for support.  Thus the project turned into a very slow process with many structural changes as time went by.  I found that very little of my .Net knowledge seemed to be particularly helpful when learning and working with Rails.  This was also the first time that I adopted testing practices "RSpec".  I also naively tried to implement Angular JS as it was being first released by Google.  

The project was finally abandoned due to costs and the lack enforcement of the new law by the agencies tasked with enforcing it. 

##Project environment:##
* Ruby 1.9.x
* Rails 3.2.x
* Angular 1.x
* Bootstrap 2.x SASS
* Devise
* CanCan
* Rolify
* Factory Girl
* Rspec
* Cucumber
* Capibara
* Capistrano