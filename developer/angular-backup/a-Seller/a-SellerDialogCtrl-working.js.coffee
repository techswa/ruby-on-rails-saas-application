App.controller 'SellerDialogCtrl', ['$scope', '$q', '$modal', '$modalInstance', 'items', 'swaFn', 'SellerModel', ($scope, $q, $modal, $modalInstance, items, swaFn, SellerModel) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into SellerDialogCtrl")

  #==============================================
  # Determine operation to perform New or EDIT
  #==============================================

  #Edit existing item
  if items.op == 'EDIT'
    #alert item.op
    #$scope.item = item  #single item
    $scope.dialogTitle = "Edit Seller"
    # Reset the buffer to null
    SellerModel.clearBuffer()
    # Load the selected row into the buffer for editing
    swaFn.logString(debug, "SellerModel.loadBuffer() - SellerdDialogCtrl")
    SellerModel.loadBuffer()
    # The tab controller is now loaded
  else
    #Add new item
    # $scope.item = ItemModel.getNewItem()
    $scope.dialogTitle = "Creat New Seller"

  #==========================================
  #  Configure tab controller
  #==========================================
  $scope.tabs = [
    {title: "Identification", template: "partials/sellerIdPartial.html", active:true, disabled: false}
    {title: "Address", template: "partials/sellerContactPartial.html", active:false, disabled: false},
    {title: "Physical", template: "partials/sellerPhysicalPartial.html", active:false, disabled: false}
    ]


  #==========================================
  #  Process dialog exit
  #==========================================

  $scope.save = ->
    # Copy temp values into value field
    #SellerModel.commitRecordTansaction()

    SellerModel.saveBuffer()
    swaFn.logObject(true, SellerModel.getSellers(), "Seller object changes saved")
    $modalInstance.close true


  $scope.close = ->
    swaFn.logString(debug, "Dialog close button clicked")
    $modalInstance.close false


]