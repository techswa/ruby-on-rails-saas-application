App.controller 'SellerDialogTabPhysicalCtrl', ['$scope', '$rootScope', '$q', 'swaFn', 'SellerModel', 'qryGenders','qryEyeColors', ($scope, $rootScope, $q, swaFn, SellerModel, qryGenders, qryEyeColors) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into SellerDialogPhysicalTabCtrl")

  #==========================================
  #  Load external routines / libraries
  #==========================================
  ArrayItemIndex = (array, selection) =>
    swaFn.ArrayItemIndex(array, selection)

  # Manage record buffer
  getBufferValue = (Key) ->
    return SellerModel.getBufferValue(Key)

  #==========================================
  #  Constants
  #==========================================
  CTRL_HEIGHT_FEET = 'height_feet'
  CTRL_HEIGHT_INCHES = 'height_inches'
  CTRL_GENDER_ID = 'gender_id'
  CTRL_EYECOLOR_ID = 'eyecolor_id'

  #==========================================
  #  Define arrays and structures needed
  #==========================================
  #Create JSON to hold display data
  $scope.buffer = {}

  #Ceated to hold select control returned data
  $scope.select = {}

  #Create JSON to hold errors
  $scope.error = {}

  #Create array of feet for select control population
  $scope.HeightFeetOptions = []

  HeightFeetOptionsCreate = () ->
    for foot in [4..7] by 1
      $scope.HeightFeetOptions.push ({id: + String(foot)})


  #Create array of inches for select control population
  $scope.HeightInchOptions = []

  HeightInchOptionsCreate = () ->
    for inch in [0..11] by 1
      $scope.HeightInchOptions.push ({id: + String(inch)})

  #==========================================
  #  Get Buffer record to edit
  #==========================================
  #Display data to edit
  getBufferData = () ->
    # Link to Model buffer
    $scope.buffer = SellerModel.getBuffer()

  #==========================================
  # Initialize select controls
  #==========================================
  initSelectControls = () ->
    $scope.select.height_feet = $scope.HeightFeetOptions[ArrayItemIndex($scope.HeightFeetOptions, getBufferValue(CTRL_HEIGHT_FEET))]
    $scope.select.height_inches = $scope.HeightInchOptions[ArrayItemIndex($scope.HeightInchOptions, getBufferValue(CTRL_HEIGHT_INCHES))]
    $scope.select.gender_id = $scope.Genders[ArrayItemIndex($scope.Genders, getBufferValue(CTRL_GENDER_ID))]
    $scope.select.eyecolor_id = $scope.Eyecolors[ArrayItemIndex($scope.Eyecolors, getBufferValue(CTRL_EYECOLOR_ID))]


  #====================================================================
  #  Update Buffer Process any click events / Update dependant fields
  #  Called from ng-change in html
  #====================================================================
  $scope.updateBuffer = (selectCtrl) ->


    if selectCtrl == CTRL_HEIGHT_FEET
      $scope.buffer.height_feet = $scope.select.height_feet.id

    if selectCtrl == CTRL_HEIGHT_INCHES
      $scope.buffer.height_inches = $scope.select.height_inches.id

    if selectCtrl == CTRL_GENDER_ID
      $scope.buffer.gender_id = $scope.select.gender_id.id
      $scope.buffer.gender = $scope.select.gender_id.abbreviation

    if selectCtrl == CTRL_EYECOLOR_ID
      $scope.buffer.eyecolor_id = $scope.select.eyecolor_id.id
      $scope.buffer.eyecolor = $scope.select.eyecolor_id.abbreviation

  #=============================================
  #  Bootstrap controller code
  #=============================================
  bootstrapController = () ->

      $scope.Genders = gon.genders
      $scope.Eyecolors = gon.eyecolors

# Works but disabled for Gon test
    #Load REST data visa promises
#    $q.all([
#      qryGenders.Query(),
#      qryEyeColors.Query()
#
#
#    ]).then (result) ->
#      $scope.Genders = result[0]
#      $scope.Eyecolors = result[1]

      HeightFeetOptionsCreate()
      HeightInchOptionsCreate()

      # Load buffer data
      getBufferData()
      # Initialize select controls
      initSelectControls()

  #Bootstrap controller
  bootstrapController()

]
