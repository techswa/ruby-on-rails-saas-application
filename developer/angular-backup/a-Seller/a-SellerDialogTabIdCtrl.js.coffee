App.controller 'SellerDialogTabIdCtrl', ['$scope', '$rootScope', '$q', 'swaFn', 'SellerModel', 'qryStates', 'qryLicenses', ($scope, $rootScope, $q, swaFn, SellerModel, qryStates, qryLicenses) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into SellerDialogIdTabCtrl")

  #==========================================
  #  Load external routines / libraries
  #==========================================
  ArrayItemIndex = (array, selection) =>
    swaFn.ArrayItemIndex(array, selection)

  #manage record buffer
  getBufferValue = (Key) ->
    return SellerModel.getBufferValue(Key)

  #==========================================
  #  Constants
  #==========================================
  EDIT_STATE = 0
  ACTIVE_STATE = 1
  INACTIVE_STATE = 2
  COMPLETED_STATE = 3

  CTRL_STATE = 'state'
  CTRL_LICENSE = 'license'
  CTRL_MONTH = 'month'
  CTRL_DAY = 'day'
  STATE_ID = 'state_id'
  LICENSE_ID = 'license_id'
  NUMBER = 'number'
  MONTH = 'month'
  DAY = 'day'
  YEAR = 'year'


  #==========================================
  #  Define arrays and structures needed
  #==========================================
  #Create JSON to hold display data
  $scope.buffer = {}

  #Ceated to hold select control returned data
  $scope.select = {}

  #Create JSON to hold errors
  $scope.error = {}

  #Create array of months for select control population
  $scope.Months = []

  MonthsCreate = () ->
    for month in [1..12] by 1
      $scope.Months.push ({id: + String(month)})


  #Create array of days for select control population
  $scope.Days = []

  DaysCreate = () ->
    for day in [1..31] by 1
      $scope.Days.push ({id: + String(day)})

  #==========================================
  #  Load Buffer record to edit
  #==========================================
  #Display data to edit
  loadBufferData = () ->
    # Link to Model buffer
    $scope.buffer = SellerModel.getBuffer()

  #==========================================
  # Initialize select controls
  #==========================================
  initSelectControls = () ->
    $scope.select.state_id = $scope.States[ArrayItemIndex($scope.States, getBufferValue(STATE_ID))]
    $scope.select.license_id = $scope.Licenses[ArrayItemIndex($scope.Licenses, getBufferValue(LICENSE_ID))]
    $scope.select.month = $scope.Months[ArrayItemIndex($scope.Months, getBufferValue(MONTH))]
    $scope.select.day = $scope.Days[ArrayItemIndex($scope.Days, getBufferValue(DAY))]
    $scope.select.year = getBufferValue(YEAR)

  #====================================================================
  #  Update Buffer Process any click events / Update dependant fields
  #  Called from ng-change in html
  #====================================================================
  $scope.updateBuffer = (selectCtrl) ->

    if selectCtrl == CTRL_STATE
      $scope.buffer.state_id = $scope.select.state_id.id
      $scope.buffer.state = $scope.select.state_id.abbreviation

    if selectCtrl == CTRL_LICENSE
      $scope.buffer.license_id = $scope.select.license_id.id
      $scope.buffer.license = $scope.select.license_id.abbreviation

    if selectCtrl == CTRL_MONTH
      $scope.buffer.month = $scope.select.month.id

    if selectCtrl == CTRL_DAY
      $scope.buffer.day = $scope.select.day.id

  #==========================================
  # Load current error data from the model
  #==========================================
  #Error flag used by ng-class in control
  getModelErrors= () ->
    $scope.error.state_id = SellerModel.getFieldError(STATE_ID)
    $scope.error.license_id = SellerModel.getFieldError(LICENSE_ID)
    $scope.error.number = SellerModel.getFieldError(NUMBER)
    $scope.error.month = SellerModel.getFieldError(MONTH)
    $scope.error.day = SellerModel.getFieldError(DAY)
    $scope.error.year = SellerModel.getFieldError(YEAR)


  #=============================================
  #  Bootstrap controller code
  #=============================================
  bootstrapController = () ->

      $scope.States = gon.states
      $scope.Licenses = gon.licenses

    # Code below works - Disabled for Gon test
#    $q.all([
#      qryStates.Query(),
#      qryLicenses.Query(),
#
#    ]).then (result) ->
#      $scope.States = result[0]
#      $scope.Licenses = result[1]

      MonthsCreate()
      DaysCreate()

      # Load buffer to edit
      loadBufferData()
      #Initialize Select control values
      initSelectControls()

  #Bootstrap controller
  bootstrapController()


]