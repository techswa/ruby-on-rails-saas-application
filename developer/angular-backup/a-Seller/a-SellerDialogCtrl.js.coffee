App.controller 'SellerDialogCtrl', ['$scope', '$q', '$modal', '$modalInstance', 'items', 'swaFn', 'SellerModel', ($scope, $q, $modal, $modalInstance, items, swaFn, SellerModel) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into SellerDialogCtrl")

  #==========================================
  #  Constants
  #==========================================
  EDIT_STATE = true
  ACTIVE_STATE = "active"
  INACTIVE_STATE = "inactive"
  COMPLETED_STATE = "completed"

  #==============================================
  # Determine operation to perform New or EDIT
  #==============================================

  #Edit existing item
  if items.op == EDIT_STATE
    #alert item.op
    #$scope.item = item  #single item
    $scope.dialogTitle = "Edit Seller"
    # Reset the buffer to null
    SellerModel.clearBuffer()
    # Load the selected row into the buffer for editing
    swaFn.logString(debug, "SellerModel.loadBuffer() - SellerdDialogCtrl")
    SellerModel.loadBuffer()
    # The tab controller is now loaded
  else
    #Add new item
    # $scope.item = ItemModel.getNewItem()
    $scope.dialogTitle = "Create New Seller"

  #==========================================
  # Configure step controller
  # id: integer handle for record
  # title: Title of form section
  # number: Glyph number to display in front of title
  # editState: active/disabled/completed - selected section title is highlighted
  # hasErrors: true, false
  # showForm: true /false ng-show div content for section
  # disabled: treu / false - section title disabled
  #==========================================
  $scope.steps = [
    {id:0,  title: "Identification", number: 1, template: "partials/_sellerId.html", editState:ACTIVE_STATE, hasErrors: false, showForm:true, disabled:false},
    {id:1,  title: "Contact", number: 2, template: "partials/_sellerContact.html", editState:INACTIVE_STATE, hasErrors: false, showForm:false, disabled:true},
    {id:2,  title: "Physical", number: 3,  template: "partials/_sellerPhysical.html", editState:INACTIVE_STATE, hasErrors: false, showForm:false, disabled:true},
    ]


  #==========================================
  #  Configure navigation controller
  # Set initial state
  #==========================================
  $scope.StepNavigation = {
    currentStep:0,
    priorButtonDisabled:true,
    nextButtonDisabled:false,
    formIncomplete:true}

  #Set default
  #$scope.priorDisabled = true

  #==========================================
  #  Manage step selections
  #==========================================
  #Clear current selection & display section
  hideFormSections = ->
    #loop through and clear all
    for step in $scope.steps
      step.editState = INACTIVE_STATE
      step.showForm = false


  $scope.getPriorStep = () ->
    #check current for for errors - if false continue
    if !checkStepErrors ($scope.StepNavigation.currentStep)
      #Get the prior page
      $scope.setStepNavigation(-1)
    else
      alert 'Errors on form'

  $scope.getNextStep = () ->
    if !checkStepErrors ($scope.StepNavigation.currentStep)
      #Get the prior page
      #Get the next page
      $scope.setStepNavigation(1)
    else
      alert 'Errors on form'


  $scope.setStepNavigation = (offset) ->
    #Uses $scope.StepNavigation = {CurrentStep:0, priorButtonDisabled: true, nextButtonDisabled: false}
    StartStep = 0
    EndStep = $scope.steps.length - 1

    # if -1 is passed in
    if offset == -1 #Get prior available tab
      #If the current tab is the first one then
      if $scope.StepNavigation.currentStep isnt 0
        #Get new current step
        $scope.StepNavigation.currentStep = $scope.StepNavigation.currentStep - 1

        #if at first step then hide the prior button
        if $scope.StepNavigation.currentStep == 0
          $scope.StepNavigation.priorButtonDisabled = true
        else
          $scope.StepNavigation.priorButtonDisabled = false
        #Always display next button
        $scope.StepNavigation.nextButtonDisabled = false

    else # Get next available tab
      if $scope.StepNavigation.currentStep isnt EndStep
        #Get new current step
        $scope.StepNavigation.currentStep = $scope.StepNavigation.currentStep + 1

        #if at the end hide the next button
        if $scope.StepNavigation.currentStep == EndStep
          $scope.StepNavigation.nextButtonDisabled = true
        else
          $scope.StepNavigation.nextButtonDisabled = false

        #Always display prior button
        $scope.StepNavigation.priorButtonDisabled = false

    #Select next Step
    $scope.selectedStep($scope.StepNavigation.currentStep)

  #

  #Called by $scope.StepNavigation
  $scope.selectedStep = (current_step) ->
    #Hide all forms
    hideFormSections()

    #set new selection to active
    $scope.steps[current_step].editState = ACTIVE_STATE
    $scope.steps[current_step].showForm = true

    #set prior selections to completed
    end = current_step
    #alert 'end: ' + end

    if current_step > 0
      #Set all to inactive
      for step in $scope.steps
        $scope.steps.editState = INACTIVE_STATE

      section = 0
      #alert current_step
      while section < current_step
        $scope.steps[section].editState = COMPLETED_STATE
        section++

  #==========================================
  #  Step 0 errors
  #==========================================
  checkStepErrors_0 = ->
    step = 0
    console.log "Checking for errors on panel 0"
    #$scope.steps[step].hasErrors = true
    return true

  #==========================================
  #Check step for errors
  #==========================================
  checkStepErrors = (step) ->
    #No errors then set step hasErrors = false
    #Has errors then set step to hasErrors = true
    today = "Monday"
    switch step
      when 0
        #checkStepErrors_0()
        #console.log "Here are your todos for the day..."
        $scope.steps[step].hasErrors = checkStepErrors_0()
      when 1
        console.log "Go watch football and relax!"
      when 2
        console.log "Go watch football and relax!"
      else
        console.log "Get to work you lazy bum!"

    #Set false for testing
    #$scope.steps[step].hasErrors = true


  #==========================================
  #  Process dialog exit
  #==========================================

  $scope.save = ->
    # Copy temp values into value field
    #SellerModel.commitRecordTansaction()
    SellerModel.saveBuffer()
    swaFn.logObject(true, SellerModel.getSellers(), "Seller object changes saved")
    $modalInstance.close true


  $scope.close = ->
    swaFn.logString(debug, "Dialog close button clicked")
    $modalInstance.close false
    #$modalInstance.hide


]