App.controller 'SellerDialogTabContactCtrl', ['$scope', '$rootScope', '$q', 'swaFn', 'SellerModel', 'qryStates', ($scope, $rootScope, $q, swaFn, SellerModel, qryStates) ->

  #==========================================
  #  Debug flags
  #==========================================
  debug = true
  swaFn.logString(debug, "Entering into SellerDialogTabAddressCtrl")

  #==========================================
  #  Load external routines / libraries
  #==========================================
  ArrayItemIndex = (array, selection) =>
    swaFn.ArrayItemIndex(array, selection)

  #manage record buffer
  getBufferValue = (Key) ->
    return SellerModel.getBufferValue(Key)

  #==========================================
  #  Constants
  #==========================================
  EDIT_STATE = 0
  ACTIVE_STATE = 1
  INACTIVE_STATE = 2
  COMPLETED_STATE = 3

  CTRL_NAME_FIRST = 'name_first'
  CTRL_NAME_LAST = 'name_last'
  CTRL_ADDRESS_1 = 'address_1'
  CTRL_ADDRESS_2 = 'address_2'
  CTRL_CITY = 'city'
  CTRL_ASTATE_ID = 'astate_id'
  CTRL_ZIP = 'zip'

  #==========================================
  #  Define arrays and structures needed
  #==========================================
  #Create JSON to hold display data
  $scope.buffer = {}

  #Ceated to hold select control returned data
  $scope.select = {}

  #Create JSON to hold errors
  $scope.error = {}

  #==========================================
  #  Load Buffer record to edit
  #==========================================
  #Display data to edit
  loadBufferData = () ->
    # Link to Model buffer
    $scope.buffer = SellerModel.getBuffer()

  #==========================================
  # Initialize select controls
  #==========================================
  initSelectControls = () ->
    $scope.select.astate_id = $scope.States[ArrayItemIndex($scope.States, getBufferValue(CTRL_ASTATE_ID))]

  #==========================================
  # Check errors test
  #==========================================
  $scope.checkErrors = ->
    alert "Leaving Tab"


  #====================================================================
  #  Update Buffer Process any click events / Update dependant fields
  #  Called from ng-change in html
  #====================================================================
  $scope.updateBuffer = (selectCtrl) ->

    if selectCtrl == CTRL_ASTATE_ID
      $scope.buffer.astate_id = $scope.select.astate_id.id
      $scope.buffer.astate = $scope.select.astate_id.abbreviation

  #==========================================
  # Load current error data from the model
  #==========================================
  #Error flag used by ng-class in control
  getModelErrors= () ->
    $scope.error.name_first = SellerModel.getFieldError(CTRL_NAME_FIRST)
    $scope.error.name_last = SellerModel.getFieldError(CTRL_NAME_LAST)
    $scope.error.address_1 = SellerModel.getFieldError(CTRL_ADDRESS_1)
    $scope.error.address_2 = SellerModel.getFieldError(CTRL_ADDRESS_2)
    $scope.error.city = SellerModel.getFieldError(CTRL_CITY)
    $scope.error.astate_id = SellerModel.getFieldError(CTRL_ASTATE_ID)
    $scope.error.zip = SellerModel.getFieldError(CTRL_ZIP)

  #=============================================
  #  Bootstrap controller code
  #=============================================
  bootstrapController = () ->

    $q.all([
      qryStates.Query(),

    ]).then (result) ->
      $scope.States = result[0]

      # Load buffer data
      loadBufferData()
      # Initialize select controls
      initSelectControls()

  #Bootstrap controller
  bootstrapController()


]